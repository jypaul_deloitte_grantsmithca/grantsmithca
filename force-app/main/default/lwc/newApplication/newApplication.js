import { LightningElement, wire } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import { CurrentPageReference } from 'lightning/navigation';

export default class NewApplication extends LightningElement {

    solicitationId;
    submissionId;
    currentPageRef;
    urlStateParameters;
    spinning = false;

    @wire(CurrentPageReference)
    getStatParameters(currentPageRef){
        if (currentPageRef){
            this.urlStateParameters = currentPageRef.state;
            this.setParametersFromUrl();
        }
    }

    setParametersFromUrl(){
        this.solicitationId = this.urlStateParameters.id || null;
    }
   
    handleSuccessRecord(event){
        this.showToast("Success", "The application has been submitted successfully.", "success");
        this.submissionId = event.detail.id;
        console.log('submission id: '+ this.submissionId);
        this.resetForm();
        this.setParametersFromUrl();
        this.spinning = false;
    }

    handleErrorRecord(event){
        this.showToast("Success", "An error occurred while submitting the application .", "error");
        this.spinning = false;
    }

    resetForm(){
        const inputFields = this.template.querySelectorAll(
            'lightning-input-field'
        );

        if(inputFields){
            inputFields.forEach(field => {
                if(field.fieldName != "Name"){
                    field.reset();
                }
            });
        }
    }

    showSpinner(event){
        this.spinning = true;
    }

    showToast(title, message, variant){
        const evnt = new ShowToastEvent({
            title: title,
            message: message,
            variant: variant
        });

        this.dispatchEvent(evnt);
    }

    // connectedCallback(){
    //     // Get Solicitation Id from url parameters
    //     const idParam = this.getUrlParamValue(window.location.href, 'id');
    //     if(idParam){
    //         this.solicitationId = idParam;
    //     }
    // }

    // getUrlParamValue(url, key){
    //     return new URL(url).searchParams.get(key);
    // }
}