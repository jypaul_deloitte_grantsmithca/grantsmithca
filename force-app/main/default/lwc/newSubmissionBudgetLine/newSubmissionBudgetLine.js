import { LightningElement, track, api } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import getSubmissionBudgetLines from '@salesforce/apex/GranteeSubmissions_Ctrl.getSubmissionBudgetLines';

export default class NewSubmissionBudgetLine extends LightningElement {
    spinning = false;

    /**
     * Parent Submission Record
     * @type GS_Submission__c object
     */
    @api submission = {};

    @track submissionLines = [];

    @track submissionListColumns = [
        {label: 'Name', fieldName: 'Name', type: 'text'},
        {label: 'Submission', fieldName: 'SubmissionName', type: 'text'},
        {label: 'Budget Category', fieldName: 'GS_Budget_Category__c', type: 'text'},
        {label: 'Requested Amount', fieldName: 'GS_Requested_Amount__c', type: 'number'},
    ];

    
    /***********************************/
    connectedCallback(){
        this.getSubmissionLines();
    }

    getSubmissionLines(){
        if(this.submission){
            getSubmissionBudgetLines({submissionId: this.submission.Id})
                .then((result) => {
                    this.submissionLines = result;
                    result.forEach(sub => {
                        sub.SubmissionName = sub.GS_Submission__r.Name;
                    })
                })
                .catch((error) =>{
                    this.showToast("Error", "Error getting submission budget lines Records .", "error");

                });
        }
    }

    handleSuccessRecord(event){
        this.showToast("Success", "The 'Submission Budget Line' has been created successfully.", "success");
        this.resetForm();
        this.spinning = false;
        
        // Refresh the table
        this.getSubmissionLines();
    }

    handleErrorRecord(event){
        this.showToast("Success", "An error occurred while creation the Submission Budget Line .", "error");
        this.spinning = false;
    }

    resetForm(){
        const inputFields = this.template.querySelectorAll(
            'lightning-input-field'
        );

        if(inputFields){
            inputFields.forEach(field => {
                if(field.fieldName != "GS_Submission__c"){
                    field.reset();
                }
            });
        }
    }

    showSpinner(event){
        this.spinning = true;
    }

    showToast(title, message, variant){
        const evnt = new ShowToastEvent({
            title: title,
            message: message,
            variant: variant
        });

        this.dispatchEvent(evnt);
    }
}