/**
 * Component to display a SLDS modal
 */
import { LightningElement, api } from 'lwc';
import save from '@salesforce/label/c.SaveButton';
import cancel from '@salesforce/label/c.CancelButton';


export default class modalSlds extends LightningElement {

    /********************** API **********************/
    
    
    /**
     * Label to show in the header with a bold style
     * @type string
     * @default ''
     */
    @api headerLabel = '';

    /**
     * Label for cancel button
     * @type string
     * @default CancelButton SF Label
     */
    @api cancelLabel = cancel;

    /**
     * Label for save button
     * @type string
     * @default SaveButton SF Label
     */
    @api saveLabel = save;

    /**
     * Modal size. Applies SLDS based on this attribute
     * Values are 'small', 'medium', 'large'
     * @type String
     * @default 'small'
     */
    @api size = 'small'
    
    /**
     * If footer modal should be hidden
     * @type boolean
     * @default false
     */
    @api hideFooter;

    /**
     * If footer modal buttons should be hidden
     * @type boolean
     * @default false
     */
    @api hideFooterButtons;
    
    /**
     * If cancel button should be hidden
     * @type boolean
     * @default false
     */
    @api hideCancelButton;

    /**
     * If save button should be hidden
     * @type boolean
     * @default false
     */
    @api hideSaveButton;

    /**
     * If cancel button should be disabled
     * @type Boolean
     * @default false
     */
    @api disabledCancelButton

    /**
     * If save button should be disabled
     * @type Boolean
     * @default false
     */
    @api disabledSaveButton

    /**
     * If save button should be disabled
     * @type Boolean
     * @default false
     */
    @api disableCloseOutside = false
    
    /* Others */
    get modalClass() {
        return "slds-modal slds-fade-in-open slds-modal_" + this.size;
    }

    /*************************************************/

    closeIt() {
        this.dispatchEvent(new CustomEvent('close', { detail: 'close' , composed: true, bubbles: false } ));
    }

    closeModalOutside(evt) {
        if(!this.disableCloseOutside && evt && evt.target && evt.target.dataset && evt.target.dataset && evt.target.dataset.where){
            this.closeIt();
        }
    }

    cancelIt() {
        this.dispatchEvent(new CustomEvent('cancel', { detail: 'cancel' , composed: true, bubbles: false } ));
    }

    saveIt() {
        this.dispatchEvent(new CustomEvent('save', { detail: 'save' , composed: true, bubbles: false } ));
    }

}