import { LightningElement, track, wire } from 'lwc';
import getGranteeSubmissions from '@salesforce/apex/GranteeSubmissions_Ctrl.getGranteeSubmissions';



export default class GranteeSubmissionsList extends LightningElement {

    /**
     * Boolean property to control visibility of the modal
     * @type Boolean
     */
    showModal = false;

    // Get Current Grantee user submissions
    @wire(getGranteeSubmissions)
    submissionsList;

    /**
     * Columns of the table of Submissions records
     * @type Array of ojects
     */
    @track submissionsColumns = [
        {label: 'Name', fieldName: 'Name', type: 'text'},
        {label: 'Solicitation', fieldName: 'GS_Solicitation__r.Name', type: 'text'},
        {label: 'Submission Date', fieldName: 'GS_Submission_date__c', type: 'date'},
        {label: 'Applicaition Status', fieldName: 'GS_Status__c', type: 'text'},
        {label: 'Proposed Budget', type: 'button', typeAttributes: {label: 'New Line', title: 'New Line', name: 'newBudgetLine', variant: 'brand', iconName: 'utility:add'}}
    ];
    

    /**
     * Selected submission from the table when the user click the button "New Line"
     * @type GS_Submission__c
     * @default {}
     */
    selectedSubmission = {};

    /*************************************************/

    handleRowAction(event){
        this.selectedSubmission = event.detail.row;
        console.log('submission id: '+this.selectedSubmission.Id);
        // Open modal to show the form of Submission_Budget_Line creation
        this.showModal = true;
    }

    handleCloseModal(event){
        this.showModal = false;
    }
}