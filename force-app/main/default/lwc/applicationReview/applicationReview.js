import { LightningElement } from 'lwc';
import SUBMISSION_SCORE from '@salesforce/schema/GS_Scorecard__c.GS_Score__c';
import SUBMISSION_MAX_SCORE from '@salesforce/schema/GS_Scorecard__c.GS_Max_Score__c';
import SUBMISSION_STATUS from '@salesforce/schema/GS_Scorecard__c.GS_Status__c';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';

export default class ApplicationReview extends LightningElement {
    scoreCardId;
    submissionScore = SUBMISSION_SCORE;
    submissionMaxScore = SUBMISSION_MAX_SCORE;
    submissionStatus = SUBMISSION_STATUS;
    spinning = false;
    
    connectedCallback(){
        const url = new URL(window.location.href).href;
        this.scoreCardId = url.substring(url.lastIndexOf('gs-scorecard')).split("/")[1];
    }

    handleSuccessRecord(event){
        this.showToast("Success", "The review has been submitted successfully.", "success");
        this.spinning = false;
    }

    handleErrorRecord(event){
        this.showToast("Success", "An error occurred while submitting the application .", "error");
        this.spinning = false;
    }

    onSubmitHandler(event){
        this.spinning = true;
        
        event.preventDefault();

        console.log('record id = '+this.scoreCardId);

        // Get form fields:
        const fields = event.detail.fields;
        // Set status field to COMPLETED
        fields.GS_Status__c = 'Completed';

        // submit the form
        this.template.querySelector('lightning-record-edit-form').submit(fields);
    }

    showToast(title, message, variant){
        const evnt = new ShowToastEvent({
            title: title,
            message: message,
            variant: variant
        });

        this.dispatchEvent(evnt);
    }
}