/**
* @File Name:   GS_Payment_Trigger.cls
* @Description:  This is a trigger for  Payment custom object
* @Author:      Halima Dosso, hdosso@deloitte.ca
* @Group:       Trigger
* @Last         Modified by:   Halima Dosso
* @Last         Modified time: 2018-12-17
* @Modification Log :
*-------------------------------------------------------------------------------------
* Ver           Date        Author             Modification
* 1.0       2018-12-17 Halima Dosso   Created the file/*/ 

trigger GS_Payment_Trigger on GS_Payment__c (after update) {
    if (Trigger.isAfter && Trigger.isUpdate) {
        Map<Id,GS_Payment__c> newRecordsMap= new Map<Id,GS_Payment__c> (Trigger.New);
        GS_Payment_Trigger_Handler.afterUpdateFilters(newRecordsMap,Trigger.OldMap );
    }
}