/**
* @File Name:   GS_Submission_Trigger.cls
* @Description:  This is a trigger for  Submission custom object
* @Author:      Halima Dosso, hdosso@deloitte.ca
* @Group:       Trigger
* @Last         Modified by:   Halima Dosso
* @Last         Modified time: 2019-01-21
* @Modification Log :
*-------------------------------------------------------------------------------------
* Ver           Date        Author             Modification
* 1.0       2018-12-18  Halima Dosso   	    Created the file/
* 1.1       2019-01-10  Jean Su             Added the trigger to link submission to budget line
* 1.2       2019-01-17  Kevin Tsai          Added linkApplicationPdf()
* 1.3       2019-01-18  Jean Su             Added the trigger to link submission to objectives
* 1.4       2019-01-21  Halima Dosso        Added call to afterInsertMethod
* 1.5       2019-12-12  Diego Villarroel    Added call to update GS_Status__c in certain conditions
*/

trigger GS_Submission_Trigger on GS_Submission__c (before update,after update, after insert) {
    if (Trigger.isAfter && Trigger.isUpdate) {
        Map<Id,GS_Submission__c> newRecordsMap= new Map<Id,GS_Submission__c> (Trigger.New);
        GS_Submission_Trigger_Handler.afterUpdateFilters(newRecordsMap,Trigger.OldMap );
    }

    if (Trigger.isAfter && Trigger.isInsert) {
        Map<Id,GS_Submission__c> newRecordsMap= new Map<Id,GS_Submission__c> (Trigger.New);
        GS_Submission_Trigger_Handler.afterInsertFilters(newRecordsMap);
        GS_Submission_Trigger_Handler.linkToApplicationBudgetLines(Trigger.new);
        GS_Submission_Trigger_Handler.linkToApplicationObjectives(Trigger.new);
        GS_Submission_Trigger_Handler.linkApplicationPdf(Trigger.new);
    }
    //run this all the time we will filter more in the handeler
    if(!Trigger.isAfter && Trigger.isUpdate){
        GS_Submission_Trigger_Handler.updateNewlyApprovedSubmissionFromTrigger(Trigger.new,Trigger.OldMap ,Trigger.isUpdate, Trigger.isInsert, Trigger.isDelete, Trigger.isAfter);
    }


}