/**
 * @File Name:   GS_ScoreCard_Trigger.cls
 * @Description:  This is a trigger for  Scorecard custom object
 * @Author:      Halima Dosso, hdosso@deloitte.ca
 * @Group:       Trigger
 * @Last         Modified by:   Halima Dosso
 * @Last         Modified time: 2018-12-07
 * @Modification Log :
 *-------------------------------------------------------------------------------------
 * Ver           Date        Author             Modification
 * 1.0		2018-12-07  Halima Dosso   		Created the file
 * 1.1		2019-01-17 	Kevin Tsai	   		Added linkApplicationPdf()
 * 1.2		2019-12-16	Diego Villarroel	Added afterUpdateScorecards and Commented unused codes calls
**/

trigger GS_ScoreCard_Trigger on GS_Scorecard__c (after update, after insert) {
	if (Trigger.isAfter && Trigger.isUpdate) {
		//Map<Id,GS_Scorecard__c> newRecordsMap= new Map<Id,GS_Scorecard__c> (Trigger.New);
		//GS_ScoreCard_Trigger_Handler.afterUpdateFilters(newRecordsMap);
		GS_ScoreCard_Trigger_Handler.afterUpdateScorecard(Trigger.newMap);
	}

	if (Trigger.isAfter && Trigger.isInsert) {
		//GS_ScoreCard_Trigger_Handler.linkApplicationPdf(Trigger.New);
	}
}