/**
* @File Name:   GS_Attachment_Trigger.trigger
* @Description:  This is a trigger for the standard Attachment object
* @Author:      Kevin Tsai, Deloitte
* @Group:       Trigger
* @Last         Modified by:   Kevin Tsai
* @Last         Modified time: 2019-01-19
* @Modification Log :
*-------------------------------------------------------------------------------------
* Ver           Date        Author             Modification
* 1.0        2019-01-19   Kevin Tsai      Created the file/class
*/
trigger GS_Attachment_Trigger on Attachment (after insert) {
	if (Trigger.isAfter && Trigger.isInsert) {
		GS_Attachment_Trigger_Handler.copyAttachmentToFiles(Trigger.new);
	}
}