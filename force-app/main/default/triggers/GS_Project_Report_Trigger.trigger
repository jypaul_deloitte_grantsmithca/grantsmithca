/**
* @File Name:   GS_Project_Report_Trigger.cls
* @Description:  This is a trigger for  Project Report custom object
* @Author:      Halima Dosso, hdosso@deloitte.ca
* @Group:       Trigger
* @Last         Modified by:   Halima Dosso
* @Last         Modified time: 2018-12-17
* @Modification Log :
*-------------------------------------------------------------------------------------
* Ver           Date        Author             Modification
* 1.0       2018-12-17 Halima Dosso   Created the file/*/ 

trigger GS_Project_Report_Trigger on GS_Project_Report__c (after update) {
	if (Trigger.isAfter && Trigger.isUpdate) {
        Map<Id,GS_Project_Report__c> newRecordsMap= new Map<Id,GS_Project_Report__c> (Trigger.New);
        GS_Project_Report_Trigger_Handler.afterUpdateFilters(newRecordsMap,Trigger.OldMap );
    }
}