/**
 * Auto Generated and Deployed by the Declarative Lookup Rollup Summaries Tool package (dlrs)
 **/
trigger dlrs_GS_Project_Report_ExpenseTrigger on GS_Project_Report_Expense__c
    (before delete, before insert, before update, after delete, after insert, after undelete, after update)
{
    dlrs.RollupService.triggerHandler(GS_Project_Report_Expense__c.SObjectType);
}