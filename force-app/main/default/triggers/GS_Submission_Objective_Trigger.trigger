/**
* @File Name:   GS_Submission_Objective_Trigger
* @Description:  This is a trigger for  Submission Objective custom object
* @Author:      Jean Su, jesu@deloitte.ca
* @Group:       Trigger
* @Last         Modified by:   Jean Su
* @Last         Modified time: 2019-01-22
* @Modification Log :
*-------------------------------------------------------------------------------------
* Ver           Date        Author             Modification
* 1.0       2019-01-22  Jean Su   	Created the file/
*/ 
trigger GS_Submission_Objective_Trigger on GS_Application_Objective__c (before insert) {
	if (Trigger.isBefore && Trigger.isInsert) {        
        GS_Submission_Objective_Trigger_Handler.linkToSubmission(Trigger.New);
    }
}