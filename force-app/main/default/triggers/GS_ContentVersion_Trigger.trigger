/**
* @File Name:   GS_ContentVersion_Trigger.cls
* @Description:  This is a trigger for the standard ContentVersion object
* @Author:      Kevin Tsai, Deloitte
* @Group:       Trigger
* @Last         Modified by:   Kevin Tsai
* @Last         Modified time: 2019-01-16
* @Modification Log :
*-------------------------------------------------------------------------------------
* Ver           Date        Author             Modification
* 1.0        2019-01-16   Kevin Tsai      Created the file/class
*/ 
trigger GS_ContentVersion_Trigger on ContentVersion (after insert) {
    if (Trigger.isAfter && Trigger.isInsert) {
        GS_ContentVersion_Trigger_Handler.createContentDistribution(Trigger.new); 
    }
}