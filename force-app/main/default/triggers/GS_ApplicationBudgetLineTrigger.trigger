/**
* @File Name:   GS_ApplicationBudgetLineTrigger.trigger
* @Description:   
* @Author:      Mohamed Zibouli, mzibouli@deloitte.ca
* @Group:       Trigger
* @Modification Log :
* 1.0       2018-12-18  Mohamed Zibouli  
* --------------------------------------------------------------------------------------
**/

trigger GS_ApplicationBudgetLineTrigger on GS_Application_Budget_Line__c (before Insert, after insert, before update, after update, before delete, after delete) {
    
    if(trigger.isBefore){        
        if(trigger.isDelete){}
        if(trigger.isUpdate){
        }
        if(trigger.isInsert){
            //Yen Le edit: Sep 18, 2019
            //Issue: GS_Application_Budget_Line__c doesn't link to GS_Submission__c
            //Cause: GS_Submission__c is created after GS_Application_Budget_Line__c is created, so that afterinsert trigger on GS_Submission__c cannot link GS_Application_Budget_Line__c
			GS_ApplicationBudgetLineTriggerHelper.LinkToRelatedSubmission(trigger.new);            
        }
    }
    
    if(Trigger.isAfter){            
        if(trigger.isDelete){}
        if(trigger.isUpdate){

		}
        if(trigger.isInsert){
            
        }
    }
}