({
    
    navigateToProject : function(component, event, helper) {
        var urlEvent = $A.get("e.force:navigateToURL");
        var projectId = component.get("v.projectWrapper.project.Id");
        urlEvent.setParams({
            "url": "/gs-project/" + projectId
        });
        urlEvent.fire();
    }
})