({
	getAvailablePrograms : function(component, helper) {
		// setup to get the programs from the server
        var action = component.get("c.getPrograms");
        //set the where clause from the builder
        action.setParam("strWhereClause",component.get('v.strWhereClause'));

        // Create a callback that is executed after
        // the server-side action returns
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                // Alert the user with the value returned
                // from the server
                //set the attribute in the CMP
                component.set('v.solicitations',response.getReturnValue());
                //set the number of results returned
                component.set('v.numPrograms',response.getReturnValue().length);

            }
            else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " +
                            errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });

        // $A.enqueueAction adds the server-side action to the queue.
        $A.enqueueAction(action);
	},
    
    getUrlCommunity: function(component, helper){
        var action = component.get("c.getCommunityUrl");
        action.setCallback(this, function(response){
            if(response.getState() === "SUCCESS"){
                component.set("v.communityUrl", response.getReturnValue());
            } else {
                var errors = response.getError();
                if(errors){
                    console.log("Error Message: "+errors[0].message);
                } else {
                    console.log("Unknown error");
                }
                
            }
        });
        
        $A.enqueueAction(action);
    }
})