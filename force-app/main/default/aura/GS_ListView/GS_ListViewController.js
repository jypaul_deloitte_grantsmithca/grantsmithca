({
    doInit: function(component, event, helper) {
        helper.getAvailablePrograms(component, helper);
        // Get the url community from Custom Metadata
        helper.getUrlCommunity(component, helper);
    }, 
    
    toggleDov: function(component, event, helper) {

		//debugger;
        var toggleElm = event.currentTarget,
            //https://developer.mozilla.org/en-US/docs/Web/API/HTMLOrForeignElement/dataset
            //access the dataset to see what the current target to toggle is
        	targetDovId = toggleElm.dataset.dov,
            targetDovElm = document.getElementById(targetDovId),
            targetRightChevr = toggleElm.querySelector(".chevron-right"),
            targetDownChevr = toggleElm.querySelector(".chevron-down");

        //if it's hiding expand it
        if (targetDovElm.classList.contains("slds-hide")) {
            toggleElm.classList.add("lv-toggle--expanded");
            toggleElm.classList.remove("lv-toggle--collapsed");
            targetDovElm.classList.remove("slds-hide");
            targetRightChevr.style.display = "none";
            targetDownChevr.style.display = "block";
        } else {
            //if it's expanded collapse it
            toggleElm.classList.add("lv-toggle--collapsed");
            toggleElm.classList.remove("lv-toggle--expanded");
            targetDovElm.classList.add("slds-hide");
            targetRightChevr.style.display = "block";
            targetDownChevr.style.display = "none";
        }
    }
})