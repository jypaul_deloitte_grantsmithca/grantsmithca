({
    hlpIsUserLoggedIn:function(component,callback){
        /*This method make call to isUserLoggedIn method in IC_LoginServiceCtrl to get info about login user.
        */        
        var action = component.get("c.isUserLoggedIn");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                callback(response.getReturnValue());
            } else if (state === "INCOMPLETE") {
                
            } else if (state === "ERROR") {
                var errors = response.getError();
                console.log('errors :: ' + JSON.stringify(errors, null, 4));
            }
        });
        $A.enqueueAction(action);
    }
})