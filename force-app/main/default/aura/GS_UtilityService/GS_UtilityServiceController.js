({
	init: function(component, event, helper) {
        helper.hlpIsUserLoggedIn(component, function(userWrapper) {
            if(userWrapper != null){
                component.set("v.urlPath",userWrapper.urlPath);
                component.set("v.siteURLPath",userWrapper.siteURLPath);
                if(userWrapper.isGuest){
                    component.set("v.loggedIn", false);
                }else{
                    component.set("v.userId",userWrapper.userId);

                    component.set("v.firstname",userWrapper.firstname);
                    component.set("v.lastname",userWrapper.lastname);
                    component.set("v.initial",userWrapper.initial);
                    component.set("v.loggedIn",true); 
                }
            }
        });
	}
})