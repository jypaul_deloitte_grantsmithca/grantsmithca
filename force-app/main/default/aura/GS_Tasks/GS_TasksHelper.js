({
	getTask : function(component,callback) {
		var action = component.get("c.getTask");
		action.setParams({
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                callback(response.getReturnValue());
            } else if (state === "INCOMPLETE") {
                
            } else if (state === "ERROR") {
                var errors = response.getError();
            }
        });
        
        $A.enqueueAction(action);
	}
})