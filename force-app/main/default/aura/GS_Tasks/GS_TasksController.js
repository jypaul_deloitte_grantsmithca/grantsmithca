({
	doInit : function(component, event, helper) {

	    //https://salesforce.stackexchange.com/questions/239372/can-we-get-community-base-url-without-apex
        var urlString = document.location.href;
        var baseURL = urlString.substring(0, urlString.indexOf("/s"));

        component.set("v.baseURL",baseURL);


        var getTasksService = component.find("tasksService");
        getTasksService.getIncompleteTasks(function(result){
            component.set("v.tasks", result);
            debugger;
        });        
    }
})