({
    doInit : function(component, event, helper) {
        var apexAction = component.get('c.getAllocations');
        apexAction.setParams({  "programId": component.get("v.relatedProgram")});
        apexAction.setCallback(this, function(response) {
            var state = response.getState();
            if(state === "SUCCESS"){
                var result = response.getReturnValue();
                component.set('v.allocationList',result);
            }
        });
        $A.enqueueAction(apexAction);
    },
    handleRadioClick: function(component, event, helper) {
       component.set('v.chosenAllocation',event.target.value);
        console.log(event.target.value);
    }
})