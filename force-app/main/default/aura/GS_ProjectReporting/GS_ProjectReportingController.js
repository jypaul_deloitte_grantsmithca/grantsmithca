/**
 * @File Name:	GS_ProjectReportingController.js
 * @Description: This class used as jS controller for Project reporting lightning component
 * @Author:      Aaron Hallink
 * @Group:   	javascript
 * @Modification Log	:
 *-------------------------------------------------------------------------------------
 * Ver       	Date        Author             Modification
 * 1.0       2019-12-02  	Hallink    		Created the file/class
 */
({
    doInit : function(component, event, helper) {
        //bulk of logic in the helper
        helper.doInit(component, event, helper);
    },
    cancel : function (component,event,helper) {
        //one liner doesn't need to go into the helper
        $A.get("e.force:closeQuickAction").fire();
        window.location.reload();
    },
    next : function (component,event,helper) {
        //bulk of logic in the helper
        helper.next(component,event,helper);
    },
    back : function (component,event,helper) {
        //bulk of logic in the helper
        helper.back(component,event,helper);
    },
    save : function(component,event,helper){
        //bulk of logic in the helper
        helper.save(component,event,helper);
    },
    attachmentSave : function(component,event,helper){
        //bulk of logic in the helper
        helper.attachmentSave(component,event,helper);
    },
    handleBudgetLineRowAction: function (component, event, helper) {
        //bulk of logic in helper
        helper.handleBudgetLineRowAction(component,event,helper);
    },
    handleExpenseLineRowAction: function (component, event, helper) {
        //bulk of logic in helper
        helper.handleExpenseLineRowAction(component,event,helper);
    },
    addRow : function (component, event, helper) {
        //bulk of logic in the helper
        helper.addRow(component, event, helper);
    },
    handleUploadFinished : function(component, event, helper){
        //bulk of logic in the helper
        helper.handleUploadFinished(component,event,helper);
    },




})