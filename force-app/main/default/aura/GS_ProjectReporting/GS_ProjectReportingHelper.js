/**
 * @File Name:	GS_ProjectReportingHelper.js
 * @Description: This class used as a JS helper class for Project reporting lightning component
 * @Author:      Aaron Hallink
 * @Group:   	JavaScript
 * @Modification Log	:
 *-------------------------------------------------------------------------------------
 * Ver       	Date        Author             Modification
 * 1.0       2019-12-02  	Hallink    		Created the file/class
 */
({
    //when the component is opened this method runs first
    doInit : function(component, event, helper) {
        component.set('v.loaded', !component.get('v.loaded'));
        var getProjectaction = component.get("c.getProjectRecord");
        getProjectaction.setParams({ recordId : component.get("v.recordId") });

        // Create a callback that is executed after
        // the server-side action returns
        getProjectaction.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {

                // from the server, in this case put the project object in the component for us to use
                component.set("v.ProjectRecord",response.getReturnValue());

            }
            else  {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " +
                            errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });

        // $A.enqueueAction adds the server-side action to the queue.
        $A.enqueueAction(getProjectaction);

        //create the action for getting the project reporting satusses
        var getStatuses = component.get("c.getProjectReportStatuses");
        // Create a callback that is executed after
        // the server-side action returns
        getStatuses.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                // from the server, in this case put the project object in the component for us to use
                component.set("v.projectReportStatuses",response.getReturnValue());
                //set the value to the first status, default value, if we don't do this it defaults to null
                component.find("reportStatus").set("v.value", response.getReturnValue()[0]);
                //end the spinner
                
                // You would typically fire a event here to trigger
                // client-side notification that the server-side
                // action is complete
            }
            else  {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " +
                            errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });

        // $A.enqueueAction adds the server-side action to the queue.
        $A.enqueueAction(getStatuses);

        //we also want to get the name of the community we are in
        var getProfileNameAction = component.get("c.getCurrentUserProfileName");
        // Create a callback that is executed after
        // the server-side action returns
        getProfileNameAction.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {

                // set the community name we get back from the
                component.set("v.profileName",response.getReturnValue());
                console.log('Profile Name: ' + component.get("v.profileName"));

                //now that we have the profile name we can set the columns
                var profileName = component.get('v.profileName');
                if(profileName == 'Grantee Community User'){
                    component.set('v.expenseLineColumns', [
                        { label: 'Name', fieldName: 'Name', type: 'text' },
                        { label: 'Transaction  Date', fieldName: 'Transaction_Date', type: 'date-local' , editable: true},
                        { label: 'Description', fieldName: 'Short_Description', type: 'text' , editable: true},
                        { label: 'Requested Amount', fieldName: 'Requested_Amount', type: 'currency' , editable: true},
                        { label: 'Ineligible Amount', fieldName: 'Ineligible_Amount', type: 'currency' },
                        { label: 'Approved Amount', fieldName: 'Approved_Amount', type: 'currency' , editable: true},
                        { label: 'Attachments', fieldName: 'relatedFilesCount', type: 'number'},
                        { type: 'action', typeAttributes: { rowActions: actions } }
                    ]);

                } else{
                    component.set('v.expenseLineColumns', [
                        { label: 'Name', fieldName: 'Name', type: 'text' },
                        { label: 'Transaction  Date', fieldName: 'Transaction_Date', type: 'date-local' , editable: true},
                        { label: 'Description', fieldName: 'Short_Description', type: 'text' , editable: true},
                        { label: 'Requested Amount', fieldName: 'Requested_Amount', type: 'currency' , editable: true},
                        { label: 'Ineligible Amount', fieldName: 'Ineligible_Amount', type: 'currency' , editable: true},
                        { label: 'Approved Amount', fieldName: 'Approved_Amount', type: 'currency' , editable: true},
                        { label: 'Attachments', fieldName: 'relatedFilesCount', type: 'number'},
                        { type: 'action', typeAttributes: { rowActions: actions } }
                    ]);
                }

            }
            else  {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " +
                            errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });

        // $A.enqueueAction adds the server-side action to the queue.
        $A.enqueueAction(getProfileNameAction);




        //now set the budget line data table to be ready.
        var actions = [
                { label: 'Add / Delete expenses', name: 'show_lineitems' }
            ];


        component.set('v.budgetLinecolumns', [
            { label: 'Budget Category ', fieldName: 'budgetCategory', type: 'text' },
            { label: 'Match Type', fieldName: 'matchType', type: 'text' },
            { label: 'Description', fieldName: 'description', type: 'text' ,},
            { label: 'Initial Award', fieldName: 'initialAward', type: 'currency' },
            { label: 'Remaining Expenses', fieldName: 'remainingExpenses', type: 'currency' },
            { label: 'Requested Sum', fieldName: 'requestedAmountSum', type: 'currency' },
            { label: 'Ineligible Sum', fieldName: 'ineligibleAmountSum', type: 'currency' },
            { label: 'Approved Sum', fieldName: 'approvedAmountSum', type: 'currency' },
            { label: 'Expenses Count', fieldName: 'expenseCount', type: 'number' },
            { type: 'action', typeAttributes: { rowActions: actions } }
        ]);

        //set the expense table columns
        var actions = [
            { label: 'Add / View Attachments', name: 'attachments' },
            { label: 'Delete expense', name: 'delete' },
        ];
        //what are the columns for the rows, different columns depending on user profile


        //create the attachment table columns
        component.set('v.attachmentLineColumns', [
            { label: 'Record Page', fieldName: 'Id', type: 'url',
                typeAttributes: { tooltip: { fieldName: 'Opens in a new window' }, target:'_blank' } },
            { label: 'Title', fieldName: 'Title', type: 'text',editable: true},
            { label: 'Description', fieldName: 'Description', type: 'text', editable: true },
            { label: 'File Type', fieldName: 'FileType', type: 'text' }
        ]);




    },
    //get the budgetline data for the budgetline data table
    fetchBudgetLineData: function (component) {

        var getProjectBudgetLineItemsWithExpensesaction = component.get("c.getProjectBudgetLineItemsWithExpenses");
        getProjectBudgetLineItemsWithExpensesaction.setParams({ "projectRecordId" : component.get("v.recordId"), "createdReportId" : component.get("v.createdReportId")});

        // Create a callback that is executed after
        // the server-side action returns
        getProjectBudgetLineItemsWithExpensesaction.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {

                // from the server, in this case put the project object in the component for us to use
                //alert(JSON.stringify(response.getReturnValue()));
                component.set("v.budgetLineData",response.getReturnValue());
                component.set('v.loaded', !component.get('v.loaded'));

            }
            else  {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " +
                            errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });

        // $A.enqueueAction adds the server-side action to the queue.
        $A.enqueueAction(getProjectBudgetLineItemsWithExpensesaction);

        component.set('v.loaded', !component.get('v.loaded'));
        

    },
    //based on the parameters inputed lets display a toast message
    showToast : function(component, event, helper,message,type,title) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": title,
            "type" : type,
            "message": message
        });
        toastEvent.fire();
    },
    handleBudgetLineRowAction : function(component, event, helper){
        //depending on the action selected from the table do the method for that action
        var action = event.getParam('action');
        var row = event.getParam('row');
        switch (action.name) {
            case 'show_lineitems':
                //set the expense line table data do the columns first
                //what are the actions for the rows
                //then we set the data
                component.set('v.selectedBudgetLineId',row.budgetLine.Id);
                component.set('v.expenseLineData',row.setRelatedExpensesWrappers);
                component.set('v.stage',3);
                break;
        }
    },
    handleExpenseLineRowAction : function(component, event, helper){
        //depending on the action selected from the table do the method for that action
        var action = event.getParam('action');
        var row = event.getParam('row');
        switch (action.name) {
            case 'delete':
                helper.delete(component,event,helper,row);
                break;
            //method to handle when people want to add attachments to their
            case 'attachments':
                var expenseLineData = component.get("v.expenseLineData");
                for(var i =0; i < expenseLineData.length; i++){
                    //make sure the casing of Id matches the wrapper class in the controller not ID or id, but Id
                    if(expenseLineData[i].Id == null || expenseLineData[i].Id == undefined ){
                        helper.showToast(component,event,helper,"Save before you add attachments to an expense","info","Please Save");
                        //if the user wants to progress they need to save their records first
                        return;
                    }
                }
                //set the attachment data
                component.set('v.attachmentData',row.relatedFiles)
                //set the record we want to attach files to
                component.set('v.selectedExpenseId',row.Id);

                component.set('v.stage',4);

                break;
        }
    },
    //used for adding blank rows to the expense line item page
    addRow : function (component, event, helper) {
        // this fetches the existing data as rendered in datatable
        var myExpenseData = component.get("v.expenseLineData");

        // now push a new empty row in the array retrieved
        myExpenseData.push(
            {
                Name : "",
                Transaction_Date : null,
                Short_Description: "add description",
                Requested_Amount: "0",
                Ineligible_Amount: "0",
                Approved_Amount: "0"
            }
        );

        // now add the new array back to the attribute, so that it reflects on the component
        component.set("v.expenseLineData", myExpenseData);

    },
    //when the user clicks delete on a row
    delete : function(component, event, helper,row){
        //sometimes we will delete an empty row that hasn't been saved
        //remove the first empty row we see
        component.set('v.loaded', !component.get('v.loaded'));
        if(row.Id == undefined || row.Id == null){
            var expenseLineData = component.get('v.expenseLineData');
            for(var i =0; i< expenseLineData.length; i++){
                if(row == expenseLineData[i]){
                    expenseLineData.splice(i,1);
                    component.set('v.expenseLineData',expenseLineData);
                    //wipe out the draft values once we deleted a row
                    component.find("expenseTable").set("v.draftValues",[]);
                    component.set('v.loaded', !component.get('v.loaded'));
                    helper.showToast(component,event,helper,"Delete Complete","success","Delete Complete");
                    return;
                }
            }
        } else{
            //now that the updated values are set we can call the apex controller
            var deleteExpenseAction = component.get("c.deleteExpense");
            deleteExpenseAction.setParams({ expenseId : row.Id });
            // Create a callback that is executed after
            // the server-side action returns
            deleteExpenseAction.setCallback(this, function(response) {
                var state = response.getState();
                if (state === "SUCCESS") {
                    //once it's removed from DB remove from the table
                    var expenseLineData = component.get('v.expenseLineData');
                    for(var i =0; i< expenseLineData.length; i++){
                        if(row == expenseLineData[i]){
                            expenseLineData.splice(i,1);
                            component.set('v.expenseLineData',expenseLineData);
                            //wipe out the draft values once we deleted a row
                            component.find("expenseTable").set("v.draftValues",[]);
                            component.set('v.loaded', !component.get('v.loaded'));
                            helper.showToast(component,event,helper,"Delete Complete","success","Delete Complete");
                            return;
                        }
                    }
                }
                else  {
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " +
                                errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                }
            });

            // $A.enqueueAction adds the server-side action to the queue.
            $A.enqueueAction(deleteExpenseAction);
        }
    },
    //switch statement to do Different work depending on the stage
    //component.get("v.stage") returns an integer
    next : function (component,event,helper) {
        //the value in the stage attribute determines what methods will be run
        switch(component.get("v.stage")) {
            case 1:
                //we don't want this one method to be too busy
                helper.stageOne(component,event,helper);
                break;
            case 2:
                // code block
                break;
            default:
            // code block
        }

    },
    //we may need to go back to the budget line items from the expense table
    back : function (component,event,helper) {
        //the value in the stage attribute determines what methods will be run
        switch(component.get("v.stage")) {
            case 2:
                break;
            case 3:
                //take the current stage minus 1
                //reset the data for the expense line item table
                component.set("v.expenseLineData",null);
                component.set('v.selectedBudgetLineId',null);
                helper.fetchBudgetLineData(component);
                component.set("v.stage",component.get("v.stage") -1);
                break;
            case 4:
                //take the current stage minus 1
                //reset the data for the expense line item table
                component.set('v.loaded', !component.get('v.loaded'));
                var currentBudgetLine = component.get('v.selectedBudgetLineId');
                component.set('v.selectedExpenseId',null);
                var getExpensesAction = component.get("c.customExpensetoExpenseWrappers");
                getExpensesAction.setParams({ selectedBudgetLineId : component.get("v.selectedBudgetLineId"), "createdReportId" : component.get("v.createdReportId")});

                // Create a callback that is executed after
                // the server-side action returns
                getExpensesAction.setCallback(this, function(response) {
                    var state = response.getState();
                    if (state === "SUCCESS") {

                        // from the server, in this case put the project object in the component for us to use
                        component.set("v.expenseLineData",response.getReturnValue());
                        component.set('v.loaded', !component.get('v.loaded'));
                    }
                    else  {
                        var errors = response.getError();
                        if (errors) {
                            if (errors[0] && errors[0].message) {
                                console.log("Error message: " +
                                    errors[0].message);
                            }
                        } else {
                            console.log("Unknown error");
                        }
                    }
                });
                // $A.enqueueAction adds the server-side action to the queue.
                $A.enqueueAction(getExpensesAction);

                component.set("v.stage",component.get("v.stage") -1);
            default:
            // code block
        }

    },
    save : function(component,event,helper){
        //the draft data is an array what was changed and what row
        component.set('v.loaded', !component.get('v.loaded'));
        var expenseLineDraftData  = component.find("expenseTable").get("v.draftValues");
        //the expense line data is what we are going to feed into the apex controller
        var expenseLineData = component.get("v.expenseLineData");

        //only finish this method if there is something to save
        if(expenseLineDraftData == null || expenseLineDraftData == undefined || expenseLineDraftData.length === 0 ){
            helper.showToast(component,event,helper,"In order to save you must change or create a record with values","info","Nothing changed");
            component.set('v.loaded', !component.get('v.loaded'));
            return;
        }

        //copy over the draft values to the expense line data since we are going
        for(var i=0; i < expenseLineDraftData.length; i++){
            //we only want the index of the row that was edited
            var editedRow = expenseLineDraftData[i].id;
            //we remove the row- from the begining of the ID
            var rowIndex = editedRow.substr(4);

            //set the new values in the list of expenses we are going to pass to the apex controller if the value is undefined in the draft lists, it means it wasn't changed so skip it
            if(expenseLineDraftData[i].Transaction_Date != null && expenseLineDraftData[i].Transaction_Date != undefined ){
                expenseLineData[rowIndex].Transaction_Date = expenseLineDraftData[i].Transaction_Date;
            }
            if(expenseLineDraftData[i].Short_Description != null && expenseLineDraftData[i].Short_Description != undefined){
                expenseLineData[rowIndex].Short_Description = expenseLineDraftData[i].Short_Description;
            }
            if(expenseLineDraftData[i].Requested_Amount != null && expenseLineDraftData[i].Requested_Amount != undefined){
                expenseLineData[rowIndex].Requested_Amount = expenseLineDraftData[i].Requested_Amount;
            }
            if(expenseLineDraftData[i].Ineligible_Amount != null && expenseLineDraftData[i].Ineligible_Amount != undefined){
                expenseLineData[rowIndex].Ineligible_Amount = expenseLineDraftData[i].Ineligible_Amount;
            }
            if(expenseLineDraftData[i].Approved_Amount != null && expenseLineDraftData[i].Approved_Amount != undefined){
                expenseLineData[rowIndex].Approved_Amount = expenseLineDraftData[i].Approved_Amount;
            }
        }

        //we must have dates for the transactions, check if any transaction dates are empty
        for(var i=0; i < expenseLineData.length; i++){
            //set the new values in the list of expenses we are going to pass to the apex controller if the value is undefined in the draft lists, it means it wasn't changed so skip it
            if(expenseLineData[i].Transaction_Date == null  || expenseLineData[i].Transaction_Date == undefined || expenseLineData[i].Transaction_Date == "" )
            {
                //if any of the rows we want to add are missing a transaction date throw a info toast
                helper.showToast(component,event,helper,"All Expenses must have Transaction Dates","info","Nothing saved");
                component.set('v.loaded', !component.get('v.loaded'));
                return;
            }
        }

        //now that the updated values are set we can call the apex controller
        var saveExpensesAction = component.get("c.saveExpenses");
        saveExpensesAction.setParams({ "createdReportId" : component.get("v.createdReportId"), "selectedBudgetLineId" : component.get("v.selectedBudgetLineId"),"listExpenses" : expenseLineData});

        // Create a callback that is executed after
        // the server-side action returns
        saveExpensesAction.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {

                // from the server, in this case put the project object in the component for us to use
                component.set("v.expenseLineData",response.getReturnValue());
                //wipe out the draft values once we are saved
                component.find("expenseTable").set("v.draftValues",null);
                component.set('v.loaded', !component.get('v.loaded'));
                helper.showToast(component,event,helper,"Save Complete","success","Save Complete");

            }
            else  {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " +
                            errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });

        // $A.enqueueAction adds the server-side action to the queue.
        $A.enqueueAction(saveExpensesAction);
    },
    attachmentSave : function(component,event,helper){
        //the draft data is an array what was changed and what row
        component.set('v.loaded', !component.get('v.loaded'));
        var attachmentDraftData  = component.find("attachmentTable").get("v.draftValues");
        //the expense line data is what we are going to feed into the apex controller
        var attachmentData = component.get("v.attachmentData");

        //only finish this method if there is something to save
        if(attachmentDraftData == null || attachmentDraftData == undefined || attachmentDraftData.length === 0 ){
            helper.showToast(component,event,helper,"In order to save you must update a record with values","info","Nothing changed");
            component.set('v.loaded', !component.get('v.loaded'));
            return;
        }

        //copy over the draft values to the expense line data since we are going
        for(var i=0; i < attachmentDraftData.length; i++){
            //we only want the index of the row that was edited
            var editedRow = attachmentDraftData[i].id;
            //we remove the row- from the begining of the ID
            var rowIndex = editedRow.substr(4);

            //set the new values in the list of expenses we are going to pass to the apex controller if the value is undefined in the draft lists, it means it wasn't changed so skip it
            if(attachmentDraftData[i].Title != null && attachmentDraftData[i].Title != undefined ){
                attachmentData[rowIndex].Title = attachmentDraftData[i].Title;
            }
            if(attachmentDraftData[i].Description != null && attachmentDraftData[i].Description != undefined){
                attachmentData[rowIndex].Description = attachmentDraftData[i].Description;
            }
        }

        //now that the updated values are set we can call the apex controller
        var saveAttachmentsAction = component.get("c.saveAttachments");
        saveAttachmentsAction.setParams({ "createdReportId" : component.get("v.createdReportId"), "selectedBudgetLineId": component.get("v.selectedBudgetLineId") ,"selectedExpenseId" : component.get("v.selectedExpenseId"),"listAttachments" : attachmentData});

        // Create a callback that is executed after
        // the server-side action returns
        saveAttachmentsAction.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {

                // from the server, in this case put the project object in the component for us to use
                component.set("v.attachmentData",response.getReturnValue());
                //wipe out the draft values once we are saved
                component.find("attachmentTable").set("v.draftValues",null);
                component.set('v.loaded', !component.get('v.loaded'));
                helper.showToast(component,event,helper,"Save Complete","success","Save Complete");

            }
            else  {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " +
                            errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });

        // $A.enqueueAction adds the server-side action to the queue.
        $A.enqueueAction(saveAttachmentsAction);
    },
    //stage one method for the component
    stageOne : function (component,event,helper) {
        var projectStartcomponent = component.find("reportingStart");
        var projectEndcomponent = component.find("reportingEnd");

        //fields must have value to continue
        //if either fields are blank just return
        if((projectEndcomponent.get("v.value") == null || projectEndcomponent.get("v.value") == undefined || projectEndcomponent.get("v.value") =="")
            ||
           (projectStartcomponent.get("v.value") == null || projectStartcomponent.get("v.value") == undefined || projectStartcomponent.get("v.value") =="")) {
            //return they component will take care of letting the user know the field is blank.
            return;
        } else{
            //now we want to make sure the end date is after the start date
            if(projectEndcomponent.get("v.value") <= projectStartcomponent.get("v.value")){
                //let the user know in a toast the dates must be correct
                helper.showToast(component,event,helper,"The end date must be after the start date","error","Dates are incorrect");
                return;
            }
            //now that things have been validated we can create the project report record
            var createProjectReport = component.get("c.createProjectReport");
            createProjectReport.setParams({ "projectId" : component.get("v.recordId"),"startDate" : projectStartcomponent.get("v.value"),"endDate" : projectEndcomponent.get("v.value"),"status" : component.find("reportStatus").get("v.value") });

            // Create a callback that is executed after
            // the server-side action returns
            createProjectReport.setCallback(this, function(response) {
                var state = response.getState();
                if (state === "SUCCESS") {
                    // from the server, in this case put the project object in the component for us to use
                    //alert(response.getReturnValue());
                    component.set("v.createdReportId",response.getReturnValue());
                    component.set("v.stage",2);

                    //query the budget lines now that we have the report ID
                    helper.fetchBudgetLineData(component);
                }
                else  {
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " +
                                errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                }
            });
            // $A.enqueueAction adds the server-side action to the queue.
            $A.enqueueAction(createProjectReport);


        }
    },
    handleUploadFinished : function(component, event, helper){
        //now that a new attachment was just uploaded we want to refresh the list of attachments for that expense
        component.set('v.loaded', !component.get('v.loaded'));
        var getExpensesAction = component.get("c.customExpensetoExpenseWrappers");
        getExpensesAction.setParams({ "selectedBudgetLineId" : component.get("v.selectedBudgetLineId"), "createdReportId" : component.get("v.createdReportId")});

        // Create a callback that is executed after
        // the server-side action returns
        getExpensesAction.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {

                // from the server, in this case put the project object in the component for us to use
                component.set("v.expenseLineData",response.getReturnValue());

                for(var i =0; i < response.getReturnValue().length ; i++ ){
                    if(response.getReturnValue()[i].Id == component.get('v.selectedExpenseId')){
                        component.set('v.attachmentData',response.getReturnValue()[i].relatedFiles);
                    }
                }

                component.set('v.loaded', !component.get('v.loaded'));
            }
            else  {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " +
                            errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        // $A.enqueueAction adds the server-side action to the queue.
        $A.enqueueAction(getExpensesAction);
    }



})