<!--
@File Name:	GS_ProjectReporting.cmp
@Description: Component markup for the GS_ProjectReporting Lightning component
@Author:      Aaron Hallink
@Group:   	Cmp
@Modification Log	:

 Ver       	Date        Author             Modification
1.0       2019-12-02  	Hallink    		Created the file

//force has recordID has no effect on the Community pages, that is why we have the design component
https://salesforce.stackexchange.com/questions/100211/how-to-access-record-id-in-custom-lightning-component-in-a-salesforce-community-->
<aura:component description="GS_ProjectReporting" implements="forceCommunity:availableForAllPageTypes,force:lightningQuickActionWithoutHeader,force:hasRecordId" controller="GS_ProjectReportingController">

    <!--    when the page loads do the initialzation -->
    <aura:handler name="init" value="{!this}" action="{!c.doInit}"/>
    <!--    this will be loaded by the init method with the project record information-->
    <aura:attribute type="Object" name="ProjectRecord"/>
    <!--This will hold the statuses available for the -->
    <aura:attribute type="String[]" name="projectReportStatuses"/>
    <!-- static resource needed for wider modal   https://developer.salesforce.com/forums/?id=9060G000000IDHxQAO -->
    <ltng:require styles="{!$Resource.widenModal}" />

    <!--keeps track of where the user is in the process-->
    <aura:attribute name="stage" type="Integer" description="Used to keep track of what step in the project reporting process" default="1"/>

    <!--if we are in a community we want to know the name of it-->
    <aura:attribute name="profileName" type="String"/>

    <!--the ID of the budget line that was created in stage one of the component, this will be used to associate the expenses with, as well as the created report id, and the selected expense for expense attachments-->
    <aura:attribute type="String" name="createdReportId"/>
    <aura:attribute type="String" name="selectedBudgetLineId"/>
    <!--this determines where uploaded files will get associated with-->
    <aura:attribute type="String" name="selectedExpenseId"/>


<!--    THESE are DATA Table attributes-->
    <aura:attribute name="budgetLineData" type="List"/>
    <aura:attribute name="budgetLinecolumns" type="List"/>

    <aura:attribute name="expenseLineData" type="List"/>
    <aura:attribute name="expenseLineColumns" type="List"/>

    <aura:attribute name="attachmentData" type="List"/>
    <aura:attribute name="attachmentLineColumns" type="List"/>

    <aura:attribute name="expenseErrors" type="Object" default="[]"/>
<!--The data table attributes end here-->
    <aura:attribute name="multiple" type="Boolean" default="true"/>
    <aura:attribute name="disabled" type="Boolean" default="false"/>

<!--    spinner to toggel when some processing is being done.-->
    <aura:attribute name="loaded" type="Boolean" default="false" />
    <aura:if isTrue="{! v.loaded }">
        <aura:set attribute="else">
            <lightning:spinner alternativeText="Loading" />
        </aura:set>
    </aura:if>


    <!--    end ofattributes, handling, spinner component. Below is the component markup / tables-->
    <section role="dialog" tabindex="-1" aria-labelledby="modal-heading-01" aria-modal="true" aria-describedby="modal-content-id-1" class="slds-modal slds-fade-in-open">
        <div class="slds-modal__container">
            <header class="slds-modal__header">
                <h2 id="modal-heading-01" class="slds-modal__title slds-hyphenate">

                    <!--modal title will be based on the stage number-->

                        {!v.ProjectRecord.Name}

                <div class="slds-align_absolute-center" style="height:2rem">
                    <aura:if isTrue="{!v.stage == 1}">
                        <lightning:breadcrumbs>
                            <lightning:breadcrumb style="text-decoration: underline;" title="{!$Label.c.GS_Report_Project_Card_Creation_Help_Text}" label="Report Project Card creation" />
                            <lightning:breadcrumb title="{!$Label.c.GS_Budget_Line_Selection_Help_Text}" label="Budget Line Selection" />
                            <lightning:breadcrumb title="{!$Label.c.GS_Expense_Item_Creation_Help_Text}" label="Expense Item Creation" />
                            <lightning:breadcrumb title="{!$Label.c.GS_Expense_Item_Creation_Help_Text}" label="Expense Item Attachments" />
                        </lightning:breadcrumbs>
                    </aura:if>
                    <aura:if isTrue="{!v.stage == 2}">
                        <lightning:breadcrumbs>
                            <lightning:breadcrumb title="{!$Label.c.GS_Report_Project_Card_Creation_Help_Text}" label="Report Project Card creation" />
                            <lightning:breadcrumb style="text-decoration: underline;" title="{!$Label.c.GS_Budget_Line_Selection_Help_Text}" label="Budget Line Selection" />
                            <lightning:breadcrumb title="{!$Label.c.GS_Expense_Item_Creation_Help_Text}" label="Expense Item Creation" />
                            <lightning:breadcrumb title="{!$Label.c.GS_Expense_Item_Creation_Help_Text}" label="Expense Item Attachments" />
                        </lightning:breadcrumbs>
                    </aura:if>
                    <aura:if isTrue="{!v.stage == 3}">
                        <lightning:breadcrumbs>
                            <lightning:breadcrumb  title="{!$Label.c.GS_Report_Project_Card_Creation_Help_Text}" label="Report Project Card creation" />
                            <lightning:breadcrumb title="{!$Label.c.GS_Budget_Line_Selection_Help_Text}" label="Budget Line Selection" />
                            <lightning:breadcrumb style="text-decoration: underline;"  title="{!$Label.c.GS_Expense_Item_Creation_Help_Text}" label="Expense Item Creation" />
                            <lightning:breadcrumb title="{!$Label.c.GS_Expense_Item_Creation_Help_Text}" label="Expense Item Attachments" />
                        </lightning:breadcrumbs>
                    </aura:if>
                    <aura:if isTrue="{!v.stage == 4}">
                        <lightning:breadcrumbs>
                            <lightning:breadcrumb title="{!$Label.c.GS_Report_Project_Card_Creation_Help_Text}" label="Report Project Card creation" />
                            <lightning:breadcrumb title="{!$Label.c.GS_Budget_Line_Selection_Help_Text}" label="Budget Line Selection" />
                            <lightning:breadcrumb title="{!$Label.c.GS_Expense_Item_Creation_Help_Text}" label="Expense Item Creation" />
                            <lightning:breadcrumb style="text-decoration: underline;"  title="{!$Label.c.GS_Expense_Item_Creation_Help_Text}" label="Expense Item Attachments" />
                        </lightning:breadcrumbs>
                    </aura:if>
                </div>
                    <!-- we are only able to upload files on the expense attachment page-->
                    <aura:if isTrue="{!v.stage == 4}">
                        <lightning:fileUpload  name="fileUploader"
                                               label= "Expense Attachment Upload"
                                               multiple="{!v.multiple}"
                                               disabled="{!v.disabled}"
                                               recordId="{!v.selectedExpenseId}"
                                               onuploadfinished="{! c.handleUploadFinished }"/>
                    </aura:if>





                </h2>
            </header>
            <div class="slds-modal__content slds-p-around_medium" id="modal-content-id-1">
                <!-- documentation for record edit form   https://developer.salesforce.com/docs/component-library/bundle/lightning:recordEditForm/example-->

                    <div class="slds-p-horizontal--small">
                        <!-- Only show the project creation stuff on stage one-->
                        <aura:if isTrue="{!v.stage == 1}">
                            <lightning:input type="date" label="Reporting Period Start Date" aura:id="reportingStart" required="true"/>
                            <lightning:input type="date" label="Reporting Period End Date" aura:id="reportingEnd" required="true"/>
                            <lightning:select disabled="true" aura:id="reportStatus" label="Status" >
                                <aura:iteration var="status" items="{!v.projectReportStatuses}">
                                    <option value="{!status}">{!status}</option>
                                </aura:iteration>
                            </lightning:select>
                        </aura:if >
                        <!--  stage 2 we get to see the data table for the budget lines-->
                        <aura:if isTrue="{!v.stage == 2}">
                            <div style="height: 300px">
                                <lightning:datatable
                                        columns="{! v.budgetLinecolumns }"
                                        data="{! v.budgetLineData }"
                                        keyField="id"
                                        onrowaction="{! c.handleBudgetLineRowAction }"
                                        hideCheckboxColumn="true"/>
                            </div>
                        </aura:if >
                        <!-- table for editing expenses-->
                        <aura:if isTrue="{!v.stage == 3}">
                            <div style="height: 300px">
                                <lightning:datatable
                                        aura:id = "expenseTable"
                                        columns="{! v.expenseLineColumns }"
                                        data="{!v.expenseLineData}"
                                        keyField="id"
                                        onrowaction="{! c.handleExpenseLineRowAction }"
                                        hideCheckboxColumn="true"
                                        errors="{!v.expenseErrors}"
                                        suppressBottomBar="true"/>
                            </div>
                        </aura:if >
                        <!--    table for adding attachments to an expense-->
                        <aura:if isTrue="{!v.stage == 4}">
                            <div style="height: 300px">
                                <lightning:datatable
                                        aura:id = "attachmentTable"
                                        columns="{! v.attachmentLineColumns }"
                                        data="{!v.attachmentData}"
                                        keyField="id"
                                        onrowaction=""
                                        hideCheckboxColumn="true"
                                        suppressBottomBar="true"/>
                            </div>
                        </aura:if >

                    </div>
            </div>
            <!-- modal footer-->
            <footer class="slds-modal__footer">
                <!--These buttons will be in the footer-->
                <lightning:button variant="neutral" label="Cancel" onclick="{!c.cancel}"/>
                <!-- no save button on a non-draft status-->
                <aura:if isTrue="{!v.stage == 3}">
                    <lightning:button variant="neutral"  label="Back" onclick="{!c.back}"/>
                    <lightning:button variant="brand" label="Add Row" title="Add Row" onclick="{! c.addRow }" />
                    <lightning:button variant="brand" label="Save" onclick="{!c.save}"/>
                </aura:if >
                <aura:if isTrue="{!v.stage == 4}">
                    <lightning:button variant="neutral"  label="Back" onclick="{!c.back}"/>
                    <lightning:button variant="brand" label="Save" onclick="{!c.attachmentSave}"/>
                </aura:if >
                <aura:if isTrue="{! v.stage == 1 }">
                    <lightning:button variant="brand" label="Next" onclick="{!c.next}"/>
                </aura:if >

            </footer>
        </div>
    </section>
</aura:component>