({
    getIncompleteTasks : function(component, event) {
        var params = event.getParam('arguments');
        var callback = params.callback;      
		var action = component.get("c.getIncompleteTask");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                callback(response.getReturnValue());
            } else if (state === "INCOMPLETE") {
                
            } else if (state === "ERROR") {
                var errors = response.getError();
            }
        });
        
        $A.enqueueAction(action);
	},
    
    getCompleteTasks : function(component, event) {
        var params = event.getParam('arguments');
        var callback = params.callback; 
		var action = component.get("c.getCompleteTask");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                callback(response.getReturnValue());
            } else if (state === "INCOMPLETE") {
                
            } else if (state === "ERROR") {
                var errors = response.getError();
            }
        });
        
        $A.enqueueAction(action);
	}
})