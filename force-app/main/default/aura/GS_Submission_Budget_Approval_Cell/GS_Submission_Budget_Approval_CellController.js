({
	doInit : function(component, event, helper) {
		var budgetLine = component.get("v.budgetLine");
        var solicitationOblId = component.get("v.solicitationOblId");
        if (budgetLine.GS_Solicitation_Obligation__c == solicitationOblId)
        	component.set("v.amount", budgetLine.GS_Approved_Currency__c);
        
	},
    amountOnChange : function(component, event, helper) {
        var amountComp = component.find("amount");
		var budgetLine = component.get("v.budgetLine");
        var solicitationOblId = component.get("v.solicitationOblId");
        var amount = component.get("v.amount");
        debugger;
        if (amount != null && amount != 0)
        {
            if (amount > budgetLine.GS_Requested_Amount__c)
            {
                amountComp.set("v.errors", [{message:"Enter a number less than requested amount"}]);
            	return;
            }
            else
                amountComp.set("v.errors", null);
            
            budgetLine.GS_Solicitation_Obligation__c = solicitationOblId;
            budgetLine.GS_Approved_Currency__c  = amount;
        	component.set("v.budgetLine", budgetLine);
            var updateEvent = component.getEvent("updateTotalApprovedAmtEvent");
            updateEvent.setParams({"updatedSolicitationOblId" : solicitationOblId,
                                   "submissionId": budgetLine.GS_Submission__c});
            updateEvent.fire();
        }
        
	},
    resetApprovedAmount : function(component, event, helper) {
      
        var budgetLine = component.get("v.budgetLine");
        var solicitationOblId = component.get("v.solicitationOblId");
        if (budgetLine.GS_Solicitation_Obligation__c != solicitationOblId)
    		component.set("v.amount", null);
    },
    checkInputIsValid : function(component, event, helper) {
    	var amountComp = component.find("amount");
        if (amountComp.get("v.errors") != null && amountComp.get("v.errors").length > 0)
            return false;
        return true;
    }
})