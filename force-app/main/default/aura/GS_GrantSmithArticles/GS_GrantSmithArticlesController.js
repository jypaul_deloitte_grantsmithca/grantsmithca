({
    doInit : function(component, event, helper) {
        var action = component.get("c.getKnowledgeArticles");
        action.setCallback(this, function(response){
            console.log(response);
            var state = response.getState();
            
            if (state === "SUCCESS" && response.getReturnValue()!=null) {
                var apexResponse=response.getReturnValue();
                console.log(JSON.stringify(apexResponse));
                component.set('v.articles',apexResponse);
                for(var i=0;i<apexResponse.length;i++){
                    if(apexResponse[i].RecordType.DeveloperName=='GS_Grantee'){
                        component.set('v.articleType','GS_Grantee');
                        break;
                    }else if(apexResponse[i].RecordType.DeveloperName=='GS_Reviewer'){
                        component.set('v.articleType','GS_Reviewer');
                        break;

                    }
                }
            }else{
                console.log('error while doinit ');
                var errors = response.getError();
                console.log(errors[0]);
            }
            
        });
        
        $A.enqueueAction(action);
        
        
    }
})