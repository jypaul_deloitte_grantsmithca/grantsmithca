({
	doInit : function(component, event, helper) {    
        helper.getSolicitation(component, function(solicitation) {
            if(solicitation != null){
                component.set("v.solicitationRelevant", solicitation);
            }
        });
    }
})