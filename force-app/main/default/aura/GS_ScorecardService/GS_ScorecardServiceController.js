({
    getIncompleteScorecards : function(component, event) {
        console.log('GS_ScorecardServiceController.js');
        var params = event.getParam('arguments');
        var callback = params.callback;      
		var action = component.get("c.getIncompleteScorecard");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                callback(response.getReturnValue());
            } else if (state === "INCOMPLETE") {
                
            } else if (state === "ERROR") {
                var errors = response.getError();
            }
        });
        
        $A.enqueueAction(action);
	}
})