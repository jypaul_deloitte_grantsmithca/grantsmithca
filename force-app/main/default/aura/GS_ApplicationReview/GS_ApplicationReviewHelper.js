({
	hlpGetScorecardInfo : function(component,callback) {
		debugger;
        var action = component.get("c.getScorecardById");
        action.setParams({ "scorecardId": this.hlpGetScorecardIdUrl()});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                callback(response.getReturnValue());
            } else if (state === "INCOMPLETE") {
            } else if (state === "ERROR") {
                var errors = response.getError();
            }
        });
        $A.enqueueAction(action);
    },

    hlpGetScorecardIdUrl : function() {
    	var currentUrl = window.location.href;
    	return currentUrl.substring(currentUrl.indexOf('gs-scorecard/')+13, currentUrl.indexOf('gs-scorecard/')+31);
    },

    hlpGetPdfUrl : function(component,callback) {
    	var action = component.get("c.getPdfUrlById");
    	action.setParams({ "scorecardId": this.hlpGetScorecardIdUrl()});
    	action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                callback(response.getReturnValue());
            } else if (state === "INCOMPLETE") {
            } else if (state === "ERROR") {
                var errors = response.getError();
            }
        });
        $A.enqueueAction(action);
    },
})