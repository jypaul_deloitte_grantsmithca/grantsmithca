({
	doInit : function(component, event, helper) {
		debugger;
        helper.hlpGetScorecardInfo(component, function(response) {
        	if(response == null){
        		alert("No Scorecard was found.")
        	}
        	debugger;
        	//console.log('Init...');
        	//console.log(response.GS_Submission__r.GS_Applicant__r.Name);
        	component.set("v.nameOfApplicant", response.GS_Submission__r.GS_Applicant__r.Name);
            component.set("v.scorecard", response);
            component.set("v.loaded", true);
        });
    },

    getPdfUrl : function(component, event, helper) {
    	helper.hlpGetPdfUrl(component, function(response) {
    		if(response == null){
        		alert("No ContentDistribution was found.")
        	}
        	debugger;
            component.set("v.pdfInfo", response);
            window.open(response.DistributionPublicUrl, "_blank", "width=800,height=800");
        });
    },
})