({
   
    handleOnChange : function(component, event, helper) {
        var recordId = event.getParams( "fields" ).value ;
        component.set( "v.selectedRecordId", recordId);
        if( component.get( "v.selectedRecordId") != ''){
            var apexAction = component.get('c.isRecordJustCreated');
//            apexAction.setParams({  "programId": event.getParams( "fields" ).value });/
			var stringifiedId= JSON.stringify(component.get('v.selectedRecordId'));
            apexAction.setParams({  "programId": stringifiedId});
            apexAction.setCallback(this, function(response) {
                var state = response.getState();
               // alert(state);
                if(state === "SUCCESS"){
                    var result = response.getReturnValue();
                    component.set('v.programJustCreated',result);
                   //alert('result'+result);
                }else{
                    var error= response.getError();
                    console.log(error[0]);
                    component.set('v.errorMsg', error[0].message);
                }
            });
            $A.enqueueAction(apexAction);
            
            
        }else{
            component.set('v.programJustCreated','false');
            //alert('existing program is not existing because it is = '+component.get('v.programJustCreated'));
        }
        
        
    }
})