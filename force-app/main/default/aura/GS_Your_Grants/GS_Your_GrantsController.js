({
	doInit : function(component, event, helper) {
        console.log('Do init in your grants');
        helper.getProjects(component, function(projectsReturned) {
            if(projectsReturned != null){
                component.set("v.projects", projectsReturned);
            }
        });
	},
})