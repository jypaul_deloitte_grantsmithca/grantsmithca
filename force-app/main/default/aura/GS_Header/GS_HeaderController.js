({
    
    
    init : function(component, event, helper) {

        // Init header
        var labelName = $A.get("$Label.c.language");
        var locale = $A.get("$Locale");
        var defTop = document.getElementById("def-top");
        var currentUrl = window.location.href;
        var currentPage = currentUrl.substring(0,currentUrl.indexOf("?language="));
        var hrefDestination = currentPage + "?language=" + ((locale.language == "en")?("fr"):("en_US"));

        if(currentUrl.includes("&")){
          var paramsIndex = currentUrl.indexOf("&");
          var otherParams = currentUrl.substring(paramsIndex);
          hrefDestination += otherParams;
        }
        
        defTop.outerHTML = wet.builder.top({
          search: false,
          lngLinks: [{
              lang: ((locale.language == "en")?("fre"):("eng")),
              href: hrefDestination,
              text: labelName
          }],
          showPreContent:  true
          //breadcrumbs:  [{'acronym':'CDN','href':'http://www.google.ca','title':'Home'}]
        });
    },

})