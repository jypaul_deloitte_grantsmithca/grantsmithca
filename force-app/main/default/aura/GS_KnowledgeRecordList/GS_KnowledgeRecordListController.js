/**
* @File Name:	GS_KnowledgeRecordList.js
* @Description:  Helper class for the GS_KnowledgeRecordList component
* @Author:   	Aaron Hallink, ahallink@deloitte.ca
* @Group:   	Apex
* @Modification Log	:
*-------------------------------------------------------------------------------------
* Ver       	Date        Author             Modification
* 1.0       2019-11-07 Aaron Hallink     Test class for GS_KnowledgeBaseCTRL
*
*/
({
    doInit : function(component, event, helper) {


        component.set('v.knowledgeListColumns', [
            {
                label: 'Title',
                fieldName: 'Id',
                type: 'url',
                typeAttributes: {
                    label: {
                        fieldName: 'Title'
                    }
                },
                sortable: true
            },
            { label: 'Description', fieldName: 'GS_Description', type: 'text' },
            { label: 'Body', fieldName: 'GS_Body', type: 'text' },
            {
                label: "Published Date",
                fieldName: "LastPublishedDate",
                type: "date-local",

            }
        ]);


        var action = component.get("c.getAllKnowledgeArticles");
        action.setCallback(this, function(response){
            console.log(response);
            var state = response.getState();

            if (state === "SUCCESS" && response.getReturnValue()!=null) {
                var apexResponse=response.getReturnValue();
                console.log(JSON.stringify(apexResponse));
                component.set('v.knowledgeDataList',apexResponse);

            }else{
                console.log('error while doinit ');
                var errors = response.getError();
                console.log(errors[0]);
            }

        });

        $A.enqueueAction(action);


    }
})