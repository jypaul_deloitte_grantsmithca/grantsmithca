({
	doInit : function(component, event, helper) {
        
		helper.initData(component, event, helper);
        
	},
    handleUpdateTotalApprovedAmtEvent : function(component, event, helper)
    {
        console.log('handleUpdateTotalApprovedAmtEvent');
        helper.dataIsChange(component);
        helper.unlinkBudgetLineWithSolicitationObl(component, event, helper);
        helper.calculateTotalApprovedAmount(component, event, helper);
        helper.calculateAllocationSummaryTable(component, event, helper);
    },
    saveOnClick : function(component, event, helper)
    {
        helper.saveSubmissionBudgetLines(component, event, helper);
    },
    goToSolicitation : function(component, event, helper)
    {
        var dataChanged = component.get("v.isChanged");
        if (dataChanged)
      	{
      		helper.showPopupHelper(component, 'modaldialog', 'slds-fade-in-');
      		helper.showPopupHelper(component,'backdrop','slds-backdrop--'); 
      	}
      	else
      	{
      		var url = window.location.href; 
	        var value = url.substr(0,url.lastIndexOf('/') + 1);
	        window.history.back();
	        return false; 
      	}
    },
    approvalChecked: function(cmp, evt) {
        var listOfApprovedSubmissions = cmp.get("v.approvedSubmissions");
        var targetSubmission = ''+evt.getSource().get("v.name");
        var indOfSubmission = listOfApprovedSubmissions.indexOf(targetSubmission);
        if(indOfSubmission === -1){
            listOfApprovedSubmissions.push(targetSubmission);
        }
        else{
			listOfApprovedSubmissions.splice(indOfSubmission, 1);
        }
        console.log(listOfApprovedSubmissions);
    },
    leavePage : function(component, event, helper)
    {
        var url = window.location.href; 
        var value = url.substr(0,url.lastIndexOf('/') + 1);
        window.history.back();
        return false; 
	   
    },  
    hideModalPopup:function(component, event, helper){
    	helper.hidePopupHelper(component, 'modaldialog', 'slds-fade-in-');
    	helper.hidePopupHelper(component, 'backdrop', 'slds-backdrop--');    	
    },
     showHideEquipSection : function(component, event, helper) {
    	console.log('show hide ');
        //debugger;
        var rowId = event.getSource().get("v.name");
 
        
        helper.helperFun(component,event, rowId);
    },
})