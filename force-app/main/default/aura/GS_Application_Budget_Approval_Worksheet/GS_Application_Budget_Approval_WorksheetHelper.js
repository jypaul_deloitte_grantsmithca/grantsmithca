({
   
    initData : function(component, event, helper) {
        //show spinner
        console.log('INIT DATA~~~@');
        helper.toggleSpinner(component, event, helper);
		var action = component.get("c.initClass");
		action.setParams({
            "solicitationId": component.get("v.recordId")
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var ctrl = response.getReturnValue();
                var submissionList = ctrl.submissionList;
                
                var solicitation = ctrl.currentSolicitation;
                component.set("v.submissionList", submissionList);
                component.set("v.solicitation", solicitation);
                
				this.setupAllocationSummaryTable(component, event, helper, ctrl.solicitationOblList);
                this.setupWorksheet(component, event, helper);
                this.calculateAllocationSummaryTable(component, event, helper);
                
                //hide spinner
        		helper.toggleSpinner(component, event, helper);
                
            } else if (state === "INCOMPLETE") {
                
            } else if (state === "ERROR") {
                var errors = response.getError();
            }
        });
        
        $A.enqueueAction(action);
	},
    refreshData: function(component, event, helper){
		var action = component.get("c.initClass");
		action.setParams({
            "solicitationId": component.get("v.recordId")
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS" ||  1 > 0) {
                
                var ctrl = response.getReturnValue();
                var submissionList = ctrl.submissionList;
                var solicitation = ctrl.currentSolicitation;
                component.set("v.submissionList", submissionList);
                component.set("v.solicitation", solicitation);
                
                try{
                    this.setupAllocationSummaryTable(component, event, helper, ctrl.solicitationOblList);
                	this.setupWorksheet(component, event, helper);
                    this.calculateAllocationSummaryTable(component, event, helper);
                }
                catch(errr){
                    //console.log('ERR');//console.log(errr);
                }
                
            } else if (state === "INCOMPLETE") {
                
            } else if (state === "ERROR") {
                var errors = response.getError();
            }
        });
        
        $A.enqueueAction(action);
    },
    setupAllocationSummaryTable : function(component, event, helper, solicitationOblList) {
	    var solicitationOblTable = [];
        var totalAvailableAmt = 0;
        var totalReservedAmt = 0;
        for(var i = 0; i<solicitationOblList.length; i++)
        {
            console.log('solicitationOblList', i);
            console.log('totalAvailableAmt', totalAvailableAmt);
            totalAvailableAmt += solicitationOblList[i].GS_Available_Amount__c == null ? 0 : solicitationOblList[i].GS_Available_Amount__c;
            totalReservedAmt += solicitationOblList[i].GS_Reserved_Amount__c == null ? 0 : solicitationOblList[i].GS_Reserved_Amount__c;
            var allo = {"id" : solicitationOblList[i].Id,
                        "name": solicitationOblList[i].GS_Allocation__r.Name,
                        "reservedAmt": solicitationOblList[i].GS_Reserved_Amount__c,
                        "availableAmt": solicitationOblList[i].GS_Available_Amount__c,
                        "obligatedAmt": 0,
                        "remainingAmt": 0};
            solicitationOblTable.push(allo);
            console.log('totalAvailableAmt', totalAvailableAmt);
            console.log('-----');
        }
        //add TOTAL line
        solicitationOblTable.push({"id" : null,
                                  "name": "TOTAL",
                                  "reservedAmt": totalReservedAmt,
                                  "availableAmt": totalAvailableAmt,
                                  "obligatedAmt": 0,
                                  "remainingAmt": 0});
        component.set("v.solicitationOblList", solicitationOblTable);
       
           
	},
    setupWorksheet : function(component, event, helper)
    {
        var submissionList = component.get("v.submissionList");
        var solicitationOblList = component.get("v.solicitationOblList");
        var budgetLineMap = []; 
        if (submissionList != null && submissionList.length > 0)
        {
            for(var i = 0; i< submissionList.length; i++)
            {
                if (submissionList[i].Application_Submission_Budget_Lines__r != null 
                    && submissionList[i].Application_Submission_Budget_Lines__r.length > 0)
                {
                    var budgetLines = submissionList[i].Application_Submission_Budget_Lines__r;
                    var totalRequestedAmt = 0;
                    var allocations = [];
                    //console.log('SUB LIST['+i+'] applicant: '+ JSON.stringify(submissionList[i]));
                    for(var j=0; j<budgetLines.length; j++)
                    {
                        totalRequestedAmt += budgetLines[j].GS_Requested_Amount__c == null ? 0 : budgetLines[j].GS_Requested_Amount__c;
                    }
                    var grandTotalApprovedAmt = 0;
                    for(var k=0; k<solicitationOblList.length-1; k++)
                    {
                        var totalApprovedAmt = 0;
                     	for(var j=0; j<budgetLines.length; j++)
                    	{
                    		if (solicitationOblList[k].id != null && budgetLines[j].GS_Solicitation_Obligation__c == solicitationOblList[k].id)
                            {
                                totalApprovedAmt += budgetLines[j].GS_Approved_Currency__c == null ? 0 : budgetLines[j].GS_Approved_Currency__c;    
                            }        
                        }
                        allocations.push({"id": solicitationOblList[k].id,
                                          "totalApprovedAmt": totalApprovedAmt});
                        grandTotalApprovedAmt += totalApprovedAmt;
                    }
                    allocations.push({"id": null,
                                       "totalApprovedAmt": grandTotalApprovedAmt});
                    budgetLineMap.push({"submission": submissionList[i], 
                                        "budgetLines": budgetLines, 
                                        "totalRequestedAmt": totalRequestedAmt,
                                        "allocationList": allocations,
                                        "status": "test..."});
                }
            }
        }
       // debugger;
        console.log(budgetLineMap);
        component.set("v.budgetLineMap", budgetLineMap);
    },
    calculateTotalApprovedAmount : function(component, event, helper)
    {
        
        var updatedSolicitationOblId = event.getParam("updatedSolicitationOblId");
        var submissionId = event.getParam("submissionId");
        var solicitationOblList = component.get("v.solicitationOblList");
        debugger;
        var budgetLineMap = component.get("v.budgetLineMap");
        var obj = $.grep(budgetLineMap, function(obj){return obj.submission.Id === submissionId;})[0];
    	if (obj != undefined)
        {
            var grandTotalApprovedAmt = 0;
            for(var k=0; k<obj.allocationList.length; k++)
            {
                var totalApprovedAmt = 0;
                if (obj.allocationList[k].id != null)
                {
                    for(var j=0; j<obj.budgetLines.length; j++)
                    {
                        if (obj.budgetLines[j].GS_Solicitation_Obligation__c == obj.allocationList[k].id)
                        {
                            totalApprovedAmt += obj.budgetLines[j].GS_Approved_Currency__c == null ? 0 : obj.budgetLines[j].GS_Approved_Currency__c;    
                        }        
                    }
                    
                    obj.allocationList[k].totalApprovedAmt = totalApprovedAmt;
                    grandTotalApprovedAmt += totalApprovedAmt;
                }
                else
                    obj.allocationList[k].totalApprovedAmt = grandTotalApprovedAmt;
                
            }
        }
        component.set("v.budgetLineMap", budgetLineMap);
       
    },
    unlinkBudgetLineWithSolicitationObl : function (component, event, helper)
    {
        var approvedAmtInputs = component.find('approvedAmtInput');
        if (approvedAmtInputs.length != undefined)
        {
            for(var cmp in approvedAmtInputs) {
               approvedAmtInputs[cmp].resetApprovedAmount();
            }
        }
    },
    calculateAllocationSummaryTable : function(component, event, helper)
    {
        var budgetLineMap = component.get("v.budgetLineMap");
        var solicitationOblList = component.get("v.solicitationOblList");
        var grandTotalObligatedAmt = 0;
        var grandTotalRemainingAmt = 0;
        for(var i = 0; i< solicitationOblList.length-1; i++)
        {
            var totalObligatedAmt = 0;
            var totalRemainingAmt = 0;
            for(var sub in budgetLineMap)
            {
                for(var j=0; j<budgetLineMap[sub].budgetLines.length; j++)
                {
                    var bl = budgetLineMap[sub].budgetLines[j];
                    if (bl.GS_Solicitation_Obligation__c == solicitationOblList[i].id)
                    {
                        totalObligatedAmt += bl.GS_Approved_Currency__c;
                    }
                }
            }
            solicitationOblList[i].obligatedAmt = totalObligatedAmt;
            solicitationOblList[i].remainingAmt = solicitationOblList[i].availableAmt - solicitationOblList[i].obligatedAmt;
            grandTotalObligatedAmt += totalObligatedAmt;
            
        }
        var totalAllObj = $.grep(solicitationOblList, function(obj){return obj.name === "TOTAL";})[0];
    	if (totalAllObj != undefined)
        {
            totalAllObj.obligatedAmt = grandTotalObligatedAmt;
            totalAllObj.remainingAmt = totalAllObj.availableAmt - grandTotalObligatedAmt;
        }
        component.set("v.solicitationOblList", solicitationOblList);
    },
    checkDataIsValid : function(component, event, helper)
    {
        var approvedAmtInputs = component.find('approvedAmtInput');
        var isValid = true;
        if (approvedAmtInputs.length != undefined)
        {
            
            for(var cmp in approvedAmtInputs) {
               isValid &= approvedAmtInputs[cmp].isValid();
            }
        }
        return isValid;
    },
    saveSubmissionBudgetLines : function(component, event, helper)
    {
        var budgetLineMap = component.get("v.budgetLineMap");
         debugger;
        var isValid = this.checkDataIsValid(component, event, helper);
        if (!isValid)
            return;
       
        
        var updateBudgetLines = [];
        debugger;
        for(var sub in budgetLineMap)
        {
            for(var j=0; j<budgetLineMap[sub].budgetLines.length; j++)
            {
                var bl = budgetLineMap[sub].budgetLines[j];
                if (bl.GS_Solicitation_Obligation__c != null && bl.GS_Approved_Currency__c != null)
                {
					updateBudgetLines.push(bl);                    
                }
            }
        }
        
        //
        var updateSubmissionIds = component.get("v.approvedSubmissions");
        debugger;
        if (updateBudgetLines.length > 0)
        {
            //show spinner
            helper.toggleSpinner(component, event, helper);
            var action = component.get("c.saveBudgetWorksheet");
            action.setParams({
                "updateBudgetLines": updateBudgetLines,
                "updateSubmissionIds": updateSubmissionIds,
                "solicitationId": component.get("v.recordId")
            });
            action.setCallback(this, function(response) {
                var state = response.getState();
                if (state === "SUCCESS") {
                    this.dataIsNotChange(component);
                    //hide spinner
                    helper.toggleSpinner(component, event, helper);
                    this.showToast(component, "Success", "success", "The worksheet is saved succcessful!");
                     //REfresh componenet here.
                    
                    console.log('initing again....');
                    helper.refreshData(component, event, helper);
                    //this.setupWorksheet(component, event, helper);
                	//this.calculateAllocationSummaryTable(component, event, helper);
                    
                } else if (state === "INCOMPLETE") {
                    
                } else if (state === "ERROR") {
                    //hide spinner
                    helper.toggleSpinner(component, event, helper);
                    var errors = response.getError();
                    if (errors && Array.isArray(errors) && errors.length > 0) {		
                        this.showToast(component, "Error", "error", errors[0].message);
    
                    }
    
                }
            });
            
            $A.enqueueAction(action);
    	}
    },
    refreshSubmissionView: function(component, event, helper){
        console.log('REFRESHING!');
        //$A.get('e.force:refreshView').fire();
        /*var actionToRefresh = component.get("c.initClass");
        actionToRefresh.setCallback(component, 
        function(response){
        	
        });*/
    },
    showToast : function(component, title, msgType, msg) {
    		    
    	var toastEvent = $A.get("e.force:showToast");
    	toastEvent.setParams({
    		"title": title,
    		"type": msgType,
    		"message": msg
    	});
    	toastEvent.fire();    	
    },
    toggleSpinner : function (component, event, helper){
    	console.log('toggleSpinner');
        var spinner = component.find("spinner");
        $A.util.toggleClass(spinner, 'slds-show');  
        $A.util.toggleClass(spinner, 'slds-hide'); 
    },
    // show the modal popup 
    showPopupHelper: function(component, componentId, className){
        var modal = component.find(componentId);
        $A.util.removeClass(modal, className + 'hide');
        $A.util.addClass(modal, className + 'open');
    },
    //hide the modal popup
    hidePopupHelper: function(component, componentId, className){
        var modal = component.find(componentId);
        $A.util.addClass(modal, className+'hide');
        $A.util.removeClass(modal, className+'open');
        component.set("v.body", "");
    },
    dataIsChange : function(component)
    {
        component.set("v.isChanged", true);
    },
    dataIsNotChange : function(component)
    {
        component.set("v.isChanged", false);
    },
    helperFun : function(component, event, rowId) {
	 
        var allBLElements = $('tr.'+rowId);
        for(var x=0; x< allBLElements.length; x++)
        {
             // Below logic to show/hide table body based on user click        
            if (allBLElements[x].style.display === "table-row") {
                allBLElements[x].style.display = "none";
            } else {
                allBLElements[x].style.display = "table-row";
            }  
        }
        var allButtons =    $('div.'+rowId);
        for(var x=0; x< allButtons.length; x++)
        {
             // Below logic to show/hide table body based on user click        
            if (allButtons[x].style.display === "block") {
                allButtons[x].style.display = "none";
            } else {
                allButtons[x].style.display = "block";
            }  
        }
	},
})