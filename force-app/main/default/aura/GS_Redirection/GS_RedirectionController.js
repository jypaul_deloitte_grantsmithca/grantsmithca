({
    doInit: function(component, event, helper) {
        console.log('version'+event);
        var navService = component.find("navService");
        
        
        var pageReference = {
            type: "standard__recordPage",
            attributes: {
                recordId: component.get('v.recordId'),
                objectApiName: component.get('v.objectApiName'),
                actionName: "view"
            }
        };
        console.log(pageReference);
        //event.preventDefault();
        navService.navigate(pageReference);  
    }
})