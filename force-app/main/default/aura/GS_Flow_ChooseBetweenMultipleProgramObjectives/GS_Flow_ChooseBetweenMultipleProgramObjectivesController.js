({
    doInit : function(component, event, helper) {
       //alert('init');
        var apexAction = component.get('c.getProgramObjectives');
        apexAction.setParams({  "programId": component.get("v.relatedProgram")});
        apexAction.setCallback(this, function(response) {
            var state = response.getState();
            //alert('state'+state);
            if(state === "SUCCESS"){
                var result = response.getReturnValue();
                component.set('v.programObjectivesList',result);
                //console.log(result);
                
            }
        });
        $A.enqueueAction(apexAction);
    },
    handleRadioClick: function(component, event, helper) {
        component.set('v.chosenProgramObjective',event.target.value);
        console.log(event.target.value);
    }
})