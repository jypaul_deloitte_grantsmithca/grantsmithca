({
	convertDateToString : function(component, dateToConvert) {
		var dateString = dateToConvert.getMonth() + 1 + "/" + dateToConvert.getDate() + "/" + dateToConvert.getFullYear();
        return dateString;
	}
})