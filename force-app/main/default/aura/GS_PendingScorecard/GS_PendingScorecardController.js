({
	doInit : function(component, event, helper) {
        var getScorecardsService = component.find("ScorecardsService");
        var scorecards;
        var scorecardInfos = [];
        var todayDate = new Date();
        console.log('GS PendingScorecard');
        var userid = $A.get("$ObjectType.CurrentUser.Id");
        console.log('userid', userid);
        getScorecardsService.getIncompleteScorecards(function(result){
            scorecards = result;
            console.log(scorecards.length, ' scorecards long!');

            for (var i = 0; i < scorecards.length; i++)
            {
                var scorecard = scorecards[i];
                var scorecardInfo = {};
                scorecardInfo["Id"] = scorecard.Id;
                scorecardInfo["Applicant"] = scorecard.GS_Submission__r.GS_Applicant__c == undefined ? "anonymous" : scorecard.GS_Submission__r.GS_Applicant__r.Name;
                scorecardInfo["Solicitation"] = scorecard.GS_Submission__r.GS_Solicitation__r.Name;
				var duedate = new Date(scorecard.Due_Date_Time__c);
                debugger;
                var thirtyDays = 30*1000*60*60*24;
                var fourteenDays = 14*1000*60*60*24;
                var sevenDays = 7*1000*60*60*24;
                var oneDay = 1*1000*60*60*24;
                if (duedate - todayDate >= fourteenDays)
                {
                    scorecardInfo["ButtonColors"] = "moreThan30Days";
                    scorecardInfo["DueDate"] = helper.convertDateToString(component, duedate);
                }
                else if (duedate - todayDate > sevenDays && duedate - todayDate < fourteenDays)
                {
                    scorecardInfo["ButtonColors"] = "lessThan30Days";
                    scorecardInfo["DueDate"] = helper.convertDateToString(component, duedate);
                }
                else if (duedate - todayDate < sevenDays && duedate - todayDate > 0)
                {
                    scorecardInfo["ButtonColors"] = "lessThan30Days";
                    scorecardInfo["DueDate"] = "tomorrow";
                }
                else if (duedate - todayDate < 0)
                {
                    scorecardInfo["ButtonColors"] = "overdue";
                    scorecardInfo["DueDate"] = "today";
                }
                scorecardInfos.push(scorecardInfo);
            }

            component.set("v.scorecardInfos", scorecardInfos);
        });
        
        
    },
    
    reviewClick : function(component, event, helper) {

	    //-3 for 's/'

        //https://salesforce.stackexchange.com/questions/239372/can-we-get-community-base-url-without-apex
        var urlString = document.location.href;


        //+3 for /s/
        //first index is the 'page name' in the commuinty, associated with an object, the second index is the record id USUALLY
        //there are cases such as the article pages where it is not exposed
        var URLSuffix = urlString.substring(0, urlString.length-3);
        
        window.open(URLSuffix +'/' + event.target.id);
    }
})