/**
 * @File Name:   GS_BreadcrumbsNavigationHelper.js
 * @Description: helper used to display the breadcrumbs for the portals
 * @Author:     Aaron hallink
 * @Group:       Component
 * @Last Modified by:   Aaron hallink
 * @Last Modified time: 2019-12-18
 * @Modification Log :
 ______________________________________________________________________________________

 * Ver       Date            Author               Modification
 * 1.0    2019-12-18     Aaron Hallink          added controller, apex controller, dynamically build crumb based on page
 */
({
    doInit : function(component, event, helper) {

        //https://salesforce.stackexchange.com/questions/239372/can-we-get-community-base-url-without-apex
        var urlString = document.location.href;
        var baseURL = urlString.substring(0, urlString.indexOf("/s"));

        //this will always bee the home
        component.set("v.baseUrl",baseURL);

        //+3 for /s/
        //first index is the 'page name' in the commuinty, associated with an object, the second index is the record id USUALLY
        //there are cases such as the article pages where it is not exposed
        var URLSuffix = urlString.substring(urlString.indexOf("/s/")+3, urlString.length);
        var URLSuffixArray = URLSuffix.split("/");



        if(URLSuffixArray[0] != undefined ){
            //we want to prettify the name displayed
            switch(URLSuffixArray[0]) {
                case 'gs-submission':
                    URLSuffixArray[0] = 'Submission';
                    helper.setCrumbs(component,event,helper,null,null,URLSuffixArray);
                    break;
                case 'gs-program':
                    URLSuffixArray[0] = 'Program';
                    helper.setCrumbs(component,event,helper,null,null,URLSuffixArray);
                    break;
                case 'gs-project':
                    URLSuffixArray[0] = 'Project';
                    helper.setCrumbs(component,event,helper,null,null,URLSuffixArray);
                    break;
                case 'task':
                    URLSuffixArray[0] = 'Task';
                    helper.setCrumbs(component,event,helper,null,null,URLSuffixArray);
                    break;
                case 'article':
                    URLSuffixArray[0] = 'Article';
                    //there is no Record id so take what's in the second index and put for the suffik
                    URLSuffixArray[1] = 's/article/' + URLSuffixArray[1];
                    //want to have link to our custome articles page
                    helper.setCrumbs(component,event,helper,"s/articles","Articles",URLSuffixArray);
                    break;
                case 'articles':
                    URLSuffixArray[0] = 'Articles';
                    //there is no Record id so take what's in the second index and put for the suffik
                    URLSuffixArray[1] = 's/articles/' + URLSuffixArray[1];
                    helper.setCrumbs(component,event,helper,null,null,URLSuffixArray);
                    //want to have link to our custome articles page
                    break;
                case 'gs-project-budget-line':
                    URLSuffixArray[0] = 'Project Budget Line';
                    //we need to ask the server what is the parent record for this ID, need a callback for the server callback
                    var onSuccessCall = function(parentRecordId){
                        helper.setCrumbs(component,event,helper,parentRecordId,'Project',URLSuffixArray);
                    }
                    helper.getParentRecordId(component, event, helper,URLSuffixArray[1], onSuccessCall);
                    break;
                case 'gs-project-report':
                    URLSuffixArray[0] = 'Project Report';
                    //we need to ask the server what is the parent record for this ID, need a callback for the server callback
                    var onSuccessCall = function(parentRecordId){
                        helper.setCrumbs(component,event,helper,parentRecordId,'Project',URLSuffixArray);
                    }
                    helper.getParentRecordId(component, event, helper,URLSuffixArray[1], onSuccessCall);
                    break
                case 'gs-project-report-expense':
                    URLSuffixArray[0] = 'Project Report Expense';
                    //we need to ask the server what is the parent record for this ID, need a callback for the server callback
                    var onSuccessCall = function(parentRecordId){
                        helper.setCrumbs(component,event,helper,parentRecordId,'Project',URLSuffixArray);
                    }
                    helper.getParentRecordId(component, event, helper,URLSuffixArray[1], onSuccessCall);
                    break;
                case 'gs-project-objective':
                    URLSuffixArray[0] = 'Project Objective';
                    //we need to ask the server what is the parent record for this ID, need a callback for the server callback
                    var onSuccessCall = function(parentRecordId){
                        helper.setCrumbs(component,event,helper,parentRecordId,'Project',URLSuffixArray);
                    }
                    helper.getParentRecordId(component, event, helper,URLSuffixArray[1], onSuccessCall);
                    break;
                case 'gs-project-report-objective':
                    URLSuffixArray[0] = 'Project Report Objective';
                    //we need to ask the server what is the parent record for this ID, need a callback for the server callback
                    var onSuccessCall = function(parentRecordId){
                        helper.setCrumbs(component,event,helper,parentRecordId,'Project',URLSuffixArray);
                    }
                    helper.getParentRecordId(component, event, helper,URLSuffixArray[1], onSuccessCall);
                    break;
                case 'gs-scorecard':
                    URLSuffixArray[0] = 'Scorecard';
                    //we need to ask the server what is the parent record for this ID, need a callback for the server callback
                    var onSuccessCall = function(parentRecordId){
                        helper.setCrumbs(component,event,helper,parentRecordId,'Submission',URLSuffixArray);
                    }
                    helper.getParentRecordId(component, event, helper,URLSuffixArray[1], onSuccessCall);
                    break;
                case 'gs-solicitation':
                    URLSuffixArray[0] = 'Solicitation';
                    //we need to ask the server what is the parent record for this ID, need a callback for the server callback
                    var onSuccessCall = function(parentRecordId){
                        helper.setCrumbs(component,event,helper,parentRecordId,'Program',URLSuffixArray);
                    }
                    helper.getParentRecordId(component, event, helper,URLSuffixArray[1], onSuccessCall);
                    break;
            }
        }




    },
    setCrumbs :function(component, event, helper,parentRecordId,parentObjectName,URLSuffixArray){
        //where we are going to set the bread crumb array
        var breadCrumbArray = [];
        //only add a parent crumb if we have the information to do so
        if(parentRecordId != null || parentRecordId != undefined ){
            breadCrumbArray.push({communityObjectName:parentObjectName, recordId:parentRecordId});
        }

        var breadCrumbObject = {communityObjectName:URLSuffixArray[0], recordId:URLSuffixArray[1]};

        //take the breadcrumb and put it in an array, put the array on the component
        breadCrumbArray.push(breadCrumbObject);
        component.set("v.breadCrumbsList",breadCrumbArray);

    },

        //onSuccess is the call back to call after the serverside callback
    getParentRecordId :function(component, event, helper,recordId,onSuccess){

        var action = component.get("c.getParentID");
        action.setParams({ recordId : recordId });

        // Create a callback that is executed after
        // the server-side action returns
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                // Alert the user with the value returned
                // from the server
                //the parent record ID is fed into the callback function
                onSuccess(response.getReturnValue());
                // You would typically fire a event here to trigger
                // client-side notification that the server-side
                // action is complete
            }
            else if (state === "INCOMPLETE") {
                // do something
            }
            else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " +
                            errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });

        // optionally set storable, abortable, background flag here

        // A client-side action could cause multiple events,
        // which could trigger other events and
        // other server-side action calls.
        // $A.enqueueAction adds the server-side action to the queue.
        $A.enqueueAction(action);
    },
})