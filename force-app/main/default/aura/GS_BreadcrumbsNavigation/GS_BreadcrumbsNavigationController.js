/**
 * @File Name:   GS_BreadcrumbsNavigationController.js
 * @Description: controller used to display the breadcrumbs for the portals
 * @Author:     Aaron hallink
 * @Group:       Component
 * @Last Modified by:   Aaron hallink
 * @Last Modified time: 2019-12-18
 * @Modification Log :
 ______________________________________________________________________________________

 * Ver       Date            Author               Modification
 * 1.0    2019-12-18     Aaron Hallink          added controller, apex controller, dynamically build crumb based on page
 */
({
    doInit : function(component, event, helper) {
        //bulk of logic in the helper
        helper.doInit(component, event, helper);
    }
})