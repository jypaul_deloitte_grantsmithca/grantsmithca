/**
* @File Name:   GS_ScoreCard_Services.cls
* @Description:  This class used as a service class for Scorecard custom object
* @Author:      Halima Dosso, hdosso@deloitte.ca
* @Group:       Apex
* @Last         Modified by:   Halima Dosso
* @Last         Modified time: 2018-12-07
* @Modification Log :
*-------------------------------------------------------------------------------------
* Ver           Date        Author             Modification
* 1.0       2018-12-07  Halima Dosso   Created the file/class and added setSubmissionToReadyForApproval method*/ 


public with sharing class GS_ScoreCard_Services {


   /**
   * @Name          setSubmissionToReadyForApproval
   * @Description   This methods sets a submission status to budget approval 
   *                 once every associated scorecards have a completed status
   * @Author        Halima Dosso, hdosso@deloitte.ca
   * @CreatedDate   2018-12-07
   * @Parameter     Set<Id> submissionIds
   * @Return        
   */
    public static void  setSubmissionToReadyForApproval(Set<Id> submissionIds){
      Map<Id,GS_Submission__c> submissionsToVerifyMap = GS_Submission_Selector.getSubmissionAndRelatedScorecardsById(submissionIds);
    	List<GS_Submission__c> submissionsToVerify = submissionsToVerifyMap.values();
      List<GS_Submission__c> submissionsToUpdate  = new List<GS_Submission__c>();
    	Set<Id> idsOfSubmissionsToUpdate = new Set<Id>();
    	if(!submissionsToVerify.isEmpty()){
    		for(GS_Submission__c submission : submissionsToVerify){
    			//Iterating over all the scorecards related to this submission to verify if they all have a completed status
    			for(GS_Scorecard__c relatedScorecard : submission.Scorecards__r ){
    				if(relatedScorecard.GS_Status__c == GS_Constants.COMPLETED_STATUS){
    					idsOfSubmissionsToUpdate.add(submission.Id);
    				}else{
    					break; // escaping the loop as if only one scorecard does not have a completed status, the submission is not ready for budget approval
    				}
    				
    			}
    		}
        for( Id submissionId : idsOfSubmissionsToUpdate){
          submissionsToUpdate.add(submissionsToVerifyMap.get(submissionId));
        }	/* TODO:	Update Status via specific Criteria.
        	 *			NOTE: this is updating the submissions before even having
        	 *			the criteria of scoring completed
        	 *			Two Workflows were created:
        	 *			UpdStatus_RFR_to_Scoring
        	 *			UpdStatus_Scoring_to_BudgetApproval
        	 *			- dvillarroeld -
        	 */
    		/*if(!idsOfSubmissionsToUpdate.isEmpty() && !submissionsToUpdate.isEmpty()){
    				for(GS_Submission__c submission : submissionsToUpdate){
    					submission.GS_Status__c = GS_Constants.BUDGET_APPROVAL; 
    				}
    				update(submissionsToUpdate);
    			
    		}*/
    	}
    }

    /**
    *  @description service method to get incomplete Scorecard for current user
    *  @author      Jean Su, Deloitte
    *  @date        2018-12-14
    *  @group       Methods
    *  @return      List<GS_Scorecard__c>
    */
	public static List<GS_Scorecard__c> getIncompleteScorecardByOwnerId(Id ownerId)
    {
        return [SELECT Id, OwnerId, Due_Date_Time__c, Name, GS_Status__c, GS_Submission__r.GS_Solicitation__r.Name, GS_Submission__r.GS_Applicant__r.Name
               FROM GS_Scorecard__c WHERE OwnerId = :ownerId AND GS_Status__c = :GS_Constants.NOT_STARTED_STATUS AND GS_Submission__c != null Order by Due_Date_Time__c DESC];
    }

  /**
  *  @description Service method to get Scorecard by ID
  *  @author      Kevin Tsai, Deloitte
  *  @date        2019-01-15
  *  @group       Methods
  *  @return      GS_Scorecard__c
  */
  public static GS_Scorecard__c getScorecardById(String scorecardId){
    return GS_Scorecard_Selector.getScorecardById(scorecardId);
  }
}