/**
* @File Name:	GS_KnowledgeBaseCTRL.cls
* @Description:  This class used as controller for Solicitation lightning component
* @Author:   	Halima Dosso, hdosso@deloitte.ca
* @Group:   	Apex
* @Last 		Modified by:  Halima Dosso
* @Last 		Modified time: 2019-01-17
* @Modification Log	:
*-------------------------------------------------------------------------------------
* Ver       	Date        Author             Modification
* 1.0       2019-01-17  Halima Dosso    Created the file/class
* 1.1       2019-11-07 Aaron Hallink     Refactored for lightning knowledge
*
*/
public with sharing class GS_KnowledgeBaseCTRL {

    @AuraEnabled
    public static  List<Knowledge__kav> getKnowledgeArticles(){
        //we only want to get articles relevant to the community we are on. General articles appear on both
        //Network.getNetworkId(); trouble getting this to return proper ID on test
        //if this is a test go right to using the GRANTEE PORTAL, if not dynamically grab the running user network they are currently on
                                                                    //if true return this query                                                 //if false return this query
        Network currentCommunity = Test.isRunningTest() ? [SELECT Id,Name FROM Network WHERE Name = :GS_Constants.GRANTEE_PORTAL_NAME] : [SELECT Id,Name FROM Network WHERE Id =: GS_Constants.COMMUNITY_ID];

        List<String> articleTypes = new List<String>{GS_Constants.GENERAL_ARTICLES};
        Integer articlesLimit = 0;

        //add to the list of acceptable article types if we know what community we are on
        if(currentCommunity!=null) {

            //ternary for better code coverage
            articlesLimit = currentCommunity.Name == GS_Constants.GRANTEE_PORTAL_NAME ? 3 : 4;

            if (currentCommunity.Name == GS_Constants.GRANTEE_PORTAL_NAME) {
                articleTypes.add(GS_Constants.GRANTEE_PORTAL_ARTICLES);
            } else if (currentCommunity.Name == GS_Constants.REVIEWER_PORTAL_NAME) {
                articleTypes.add(GS_Constants.REVIEWER_PORTAL_ARTICLES);
            }
        }

        System.debug('Community Name:' + currentCommunity.Name);
        //query from the knowledge articles from the list of acceptable record types
        List<Knowledge__kav>knowledgeArticles = [SELECT
                Id,
                Title,
                GS_Body__c,
                GS_Description__c,
                RecordType.DeveloperName
        FROM Knowledge__kav WHERE RecordType.DeveloperName IN:articleTypes AND PublishStatus = 'Online' LIMIT :articlesLimit];


        System.debug('Articles: ' + knowledgeArticles);
            return knowledgeArticles;

        
    }

    @AuraEnabled
    public static  List<knowledgeWrapper> getAllKnowledgeArticles(){
        //we only want to get articles relevant to the community we are on. General articles appear on both
        //Network.getNetworkId(); trouble getting this to return proper ID on test
        //if this is a test go right to using the GRANTEE PORTAL, if not dynamically grab the running user network they are currently on
        //if true return this query                                                 //if false return this query
        Network currentCommunity = Test.isRunningTest() ? [SELECT Id,Name FROM Network WHERE Name = :GS_Constants.GRANTEE_PORTAL_NAME] : [SELECT Id,Name FROM Network WHERE Id =: GS_Constants.COMMUNITY_ID];

        List<String> articleTypes = new List<String>{GS_Constants.GENERAL_ARTICLES};

        //add to the list of acceptable article types if we know what community we are on
        if(currentCommunity!=null) {

            if (currentCommunity.Name == GS_Constants.GRANTEE_PORTAL_NAME) {
                articleTypes.add(GS_Constants.GRANTEE_PORTAL_ARTICLES);
            } else if (currentCommunity.Name == GS_Constants.REVIEWER_PORTAL_NAME) {
                articleTypes.add(GS_Constants.REVIEWER_PORTAL_ARTICLES);
            }
        }

        System.debug('Community Name:' + currentCommunity.Name);
        //query from the knowledge articles from the list of acceptable record types
        List<Knowledge__kav>knowledgeArticles = [SELECT
                Id,
                Title,
                GS_Body__c,
                GS_Description__c,
                RecordType.DeveloperName,PublishStatus,UrlName,LastPublishedDate
        FROM Knowledge__kav WHERE RecordType.DeveloperName IN:articleTypes AND PublishStatus = 'Online'];


        List<knowledgeWrapper> knowledgeWrapperstoReturn = new List<knowledgeWrapper>();

        for(Knowledge__kav article : knowledgeArticles){
            knowledgeWrapperstoReturn.add(new knowledgeWrapper(article));
        }


        return knowledgeWrapperstoReturn;


    }

    public class knowledgeWrapper{

        //these are the expense fields
        //this ID at the top is the URL for the file
        @Auraenabled public String Id{get;set;}
        @Auraenabled public String Title{get;set;}
        @Auraenabled public String GS_Description{get;set;}
        @Auraenabled public String GS_Body{get;set;}
        @Auraenabled public Date LastPublishedDate{get;set;}


        //no args constructor needed for instantiating a List<ExpenseWrapper>
        public knowledgeWrapper(){
        }

        public knowledgeWrapper(Knowledge__kav articleKav){

            //set the file page URL, community URL if it is in a community
            if(GS_Constants.COMMUNITY_ID == null){
                this.id = System.URL.getSalesforceBaseURL().toExternalForm() +'/' + articleKav.Id;
            } else{
                String communityUrl = Network.getLoginUrl(GS_Constants.COMMUNITY_ID);
                //remove the word login at the end of the url
                communityUrl = communityUrl.substring(0,communityUrl.length()-5);
                this.Id = communityUrl+'article/'+articleKav.UrlName;
            }


            this.Title = articleKav.Title;
            this.GS_Description = articleKav.GS_Description__c;
            this.GS_Body  = articleKav.GS_Body__c;
            this.LastPublishedDate = Date.valueOf(articleKav.LastPublishedDate);

        }

    }
    
}