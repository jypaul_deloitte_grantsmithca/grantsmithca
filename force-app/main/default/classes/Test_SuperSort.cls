/**
* @File Name:	Test_SuperSort.cls
* @Description: This class used as a service class for Super Sort
* @Author:   	Benjamin Hagel, bhagel@deloitte.ca
* @Group:   	Apex
* @Last 		Modified by:   Benjamin Hagel
* @Last 		Modified time: 2019-12-05
*/

@IsTest
private class Test_SuperSort {
    @IsTest
    static void testSuperSortFunctionality_Values() {

        List<GS_Solicitation__c> testList = new List<GS_Solicitation__c>();
        //Create data
        for(Integer h = 0;h < 9;h++){
            GS_Solicitation__c newSol = new GS_Solicitation__c(Name='Name'+h);
            insert newSol;
            testList.add(newSol);
        }

        //Sort fields that ar enot references
        System.assert(testList.get(0).Name == 'Name0');
        superSort.sortList(testList, 'Name', 'desc');
        System.assert(testList.get(0).Name == 'Name8');
        superSort.sortList(testList, 'Name', 'asc');
        System.assert(testList.get(0).Name == 'Name0');

    }

    @IsTest
    static void testSuperSortFunctionality_References() {

        List<GS_Solicitation__c> testList = new List<GS_Solicitation__c>();
        //Create data
        for(Integer h = 0;h < 9;h++){
            Integer g = 8 - h;
            GS_Program__c relatedProgramTestObj = new GS_Program__c(Name='p'+g);
            insert relatedProgramTestObj;

            GS_Solicitation__c newSol = new GS_Solicitation__c(Name='Name'+h);
            newSol.GS_Program__c = relatedProgramTestObj.Id;
            insert newSol;
            testList.add(newSol);
        }

        superSort.sortList(testList, 'GS_Program__c', 'desc');
        //Sort fields that are references
        System.debug('What is the name value?: ' + testList.get(0).Name);
        System.assert(testList.get(0).Name == 'Name0');
    }
}