/**
* @File Name:   GS_ApplicationsService.cls
* @Description:  This class used as a service class for VLocity Application object
* @Author:      Mohamed Zibouli, mzibouli@deloitte.ca
* @Group:       Apex
* @Last         Modified by:   Jean Su
* @Last         Modified time: 2019-01-10
* @Modification Log :
*-------------------------------------------------------------------------------------
* Ver           Date        Author             Modification
* 1.0       2018-12-04  Mohamed Zibouli     Created the file/class
* 1.1       2019-01-10  Jean Su             Added the function to get application budget line by application ids
* 1.2       2019-01-18  Jean Su             Added the function to get application objective by application ids
* 1.3       2019-10-15  Aaron hallink       Remove 	GS_vlocity_Application__c references
* 1.4       2019-12-23  Diego Villarroel    Reworked method, new wrapper and returned variables
*/
public with sharing class GS_ApplicationsService {

    /**
    * @Name          getSubmissionsByCreatedById
    * @Description   This method is called get application objective by application ids
    * @Author        unknown
    * @CreatedDate   2019-12-20
    * @Parameter     Set<Id> solIds
    * @Return        List<GS_Submission__c>
    */
    public static List<GS_Submission__c> getSubmissionsByCreatedById(Id ownerId){
        return [SELECT  id,
                        Name,
                        GS_Status__c,
                        GS_Submission_date__c,
                        GS_Solicitation__c,
                        GS_Solicitation__r.Name,
                        GS_Solicitation__r.GS_Awarded_Date__c,
                        GS_Solicitation__r.GS_Application_Due_Date_Time__c,
                        GS_Solicitation__r.GS_Application_Form_Template_URL__c
                  FROM GS_Submission__c
                 WHERE OwnerId =: ownerId];
    }
    
   /**
   * @Name          getApplicationBudgetLine
   * @Description   This method is called get application budget line by application ids
   * @Author        Jean Su, jesu@deloitte.ca
   * @CreatedDate   2019-01-10
   * @Parameter     Set<Id> appIds
   * @Return        List<GS_Application_Budget_Line__c>
   */
    public static List<GS_Application_Budget_Line__c> getApplicationBudgetLine(Set<Id> appIds){
//        return [select id, GS_vlocity_Application__c
//                from GS_Application_Budget_Line__c where GS_vlocity_Application__c in : appIds];
        return [select id
          from GS_Application_Budget_Line__c where Id in : appIds];
    }
    
   /**
   * @Name          getApplicationObjective
   * @Description   This method is called get application objective by application ids
   * @Author        Jean Su, jesu@deloitte.ca
   * @CreatedDate   2019-01-18
   * @Parameter     Set<Id> appIds
   * @Return        List<GS_Application_Objective__c>
   */
    public static List<GS_Application_Objective__c> getApplicationObjective(Set<Id> appIds){
//        return [select id, GS_vlocity_Application__c
//                from GS_Application_Objective__c where GS_vlocity_Application__c in : appIds];

        return [select id from GS_Application_Objective__c where Id in : appIds];
    }
}