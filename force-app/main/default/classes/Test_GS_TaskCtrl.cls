/**
* @File Name:	Test_GS_TaskCtrl.cls
* @Description: This class used as test coverage for the Task Controller
* @Author:   	Benjamin Hagel, bhagel@deloitte.ca
* @Group:   	Apex
* @Last 		Modified by:   Benjamin Hagel
* @Last 		Modified time: 2019-12-04
* @Modification Log	:
*-------------------------------------------------------------------------------------
* Ver       	Date        Author             Modification
* 1.0       2019-12-04  	Benjamin Hagel    	Created the file/class
*/
@IsTest
private class Test_GS_TaskCtrl {
    @IsTest
    static void testBehavior() {
        Task testTask = new Task();
        insert testTask;


        //Grab task
        List<Task> incTasks = GS_TaskCtrl.getIncompleteTask();
        List<Task> comTasks = GS_TaskCtrl.getCompleteTask();

        System.assert(incTasks.size() == 1);
        System.assert(comTasks.size() == 0);

        //Task testTask2 = new Task();
        testTask.Status = GS_Constants.taskCompleted;
        update testTask;

        //Grab task AGAIN
        incTasks = GS_TaskCtrl.getIncompleteTask();
        comTasks = GS_TaskCtrl.getCompleteTask();

        System.assert(incTasks.size() == 0);
        System.assert(comTasks.size() == 1);

    }
}