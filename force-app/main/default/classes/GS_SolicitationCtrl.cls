public class GS_SolicitationCtrl {
	
    @AuraEnabled
    public static List<GS_Solicitation__c> getSolicitationRelevant()
    {
        return [SELECT Id, Name, GS_Program__r.Name, GS_Available_Amount__c FROM GS_Solicitation__c LIMIT 3];
    }
}