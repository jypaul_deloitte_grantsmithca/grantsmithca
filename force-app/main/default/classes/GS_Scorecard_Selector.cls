/**
* @File Name:   GS_Scorecard_Selector.cls
* @Description:  This is a selector for  Scorecard custom object (all the queries on this object are stored here)
* @Author:      Halima Dosso, hdosso@deloitte.ca
* @Group:       Apex class
* @Last         Modified by:   Halima Dosso
* @Last         Modified time: 2018-12-07
* @Modification Log :
*-------------------------------------------------------------------------------------
* Ver           Date        Author             Modification
* 1.0       2018-12-07  Halima Dosso   Created the file and added  getSScorecardsById methods/*/ 

public with sharing class GS_Scorecard_Selector {
	public static Map<Id,GS_Scorecard__c> getScorecardsByIds(Set<String> scorecardIds ) {
        return new Map<Id,GS_Scorecard__c>([SELECT Id, Name,GS_Status__c,GS_Max_Score__c,GS_Score__c
							                FROM GS_Scorecard__c
							                WHERE Id IN :scorecardIds]);
    }

    public static GS_Scorecard__c getScorecardById(String scorecardId) {
    	return [SELECT Id, OwnerId, Due_Date_Time__c, Name, GS_Status__c, 
    				   GS_Submission__r.GS_Solicitation__r.Name, GS_Submission__r.GS_Applicant__r.Name
                FROM GS_Scorecard__c 
                WHERE Id =: scorecardId];
    }
}