/**
* @File Name:   Test_GS_LoginServiceCtrl
* @Description:  This is a test class to cover the GS_LoginServiceController
* @Author:      Aaron Hallink
* @Group:       Apex class
* @Last         Modified by:   Aaron Hallink
* @Last         Modified time: 2019-12-04
* @Modification Log :
*-------------------------------------------------------------------------------------
* Ver           Date        Author             Modification
* 1.0       2019-12-04  Aaron Hallink   Created the file and added test methods/*/

@IsTest
private class Test_GS_LoginServiceCtrl {
    static testMethod void testBehavior() {

        //call the method to get the user wrapper
        GS_LoginServiceCtrl.UserWrapper userWrapper = GS_LoginServiceCtrl.isUserLoggedIn();

        //the same as the user that called the method
        System.assert(userWrapper.userId == Userinfo.getUserId());


        //this returns the currently logged in user
        User u = GS_loginUserProfileCtrl.fetchUserDetail();
        System.assert(u.Id == Userinfo.getUserId());


        //we want to make a contact user (community user to test the other lines of the CTRL class)
        //this user is used to avoid mixed DML
        //https://stackoverflow.com/questions/2387475/how-to-avoid-mixed-dml-operation-error-in-salesforce-tests-that-create-users
        User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];

        //Create portal account owner
        UserRole portalRole = [Select Id,Name From UserRole Where Name = 'SVP Customer Experience' Limit 1];
        Profile profile1 = [Select Id from Profile where name = 'System Administrator'];
        User portalAccountOwner1 = new User(
                UserRoleId = portalRole.Id,
                ProfileId = profile1.Id,
                Username = System.now().millisecond() + 'test2@test123123123123123123123123.com',
                Alias = 'batman',
                Email='bruce.wayne@wayneenterprises.com',
                EmailEncodingKey='UTF-8',
                Firstname='Bruce',
                Lastname='Wayneuuu',
                LanguageLocaleKey='en_US',
                LocaleSidKey='en_US',
                TimeZoneSidKey='America/Chicago'
        );
        insert portalAccountOwner1 ;

        //Create account
        Account portalAccount1 = new Account(
                Name = 'TestAccount',
                OwnerId = portalAccountOwner1.Id
        );
        //https://stackoverflow.com/questions/2387475/how-to-avoid-mixed-dml-operation-error-in-salesforce-tests-that-create-users
        System.runAs(thisUser){
            insert portalAccount1;
        }


//Create contact
        Contact contact1 = new Contact(
                FirstName = 'Test',
                Lastname = 'McTesty',
                AccountId = portalAccount1.Id,
                Email = System.now().millisecond() + 'test@test.com'
        );
        //https://stackoverflow.com/questions/2387475/how-to-avoid-mixed-dml-operation-error-in-salesforce-tests-that-create-users
        System.runAs(thisUser){
            insert contact1;
        }

        //Create user
        Id profile = [select id from profile where name=:GS_Constants.GRANTEE_PORTAL_PROFILE].id;
        User user1 = new User(
                Username = System.now().millisecond() + 'test12345@test.com',
                ContactId = contact1.Id,
                ProfileId = profile,
                Alias = 'test123',
                Email = 'test12345@test.com',
                EmailEncodingKey = 'UTF-8',
                FirstName ='testy',
                LastName = 'McTesty',
                CommunityNickname = 'test12345',
                TimeZoneSidKey = 'America/Los_Angeles',
                LocaleSidKey = 'en_US',
                LanguageLocaleKey = 'en_US'

        );
        //https://stackoverflow.com/questions/2387475/how-to-avoid-mixed-dml-operation-error-in-salesforce-tests-that-create-users
        System.runAs(thisUser){
            insert user1;
        }

        //now that we have done the setup try calling the user wrapper method again
        System.runAs(user1){
            userWrapper = GS_LoginServiceCtrl.isUserLoggedIn();
        }


    }
}