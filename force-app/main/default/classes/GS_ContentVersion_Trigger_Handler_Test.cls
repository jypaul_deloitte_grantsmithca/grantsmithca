/**
* @File Name:   GS_Project_Report_Trigger_Handler_Test.cls
* @Description: This is the TestClass for GS_Project_Report_Trigger_Handler
* @Author:      Diego Villarroel, dvillarroeld@deloitte.com
* @Group:       Test Class
* @Last         Modified by:   Diego Villarroel
* @Last         Modified time: 2019/03/12
* @Modification Log :
*-------------------------------------------------------------------------------------
* Ver           Date        Author              Modification
* 1.0           2019/03/12  Diego Villarroel    Created the file and testCreateContentDistributionBehavior method
**/
@IsTest
private class GS_ContentVersion_Trigger_Handler_Test {
    @testSetup
    static void setup(){

    }

    /**
    * @Name          afterUpdateFiltersBehavior
    * @Description   This method Tests the Behavior/Coverage of testCreateContentDistributionBehavior
    * @Author        Diego Villarroel
    * @CreatedDate   2019-12-03
    * @Parameter
    * @Return
    **/
    @IsTest//(SeeAllData=true)
    static void testCreateContentDistributionBehavior() {
        ContentVersion testContentVersion = new ContentVersion();

        Test.startTest();
        testContentVersion.Title = 'test name';
        testContentVersion.Description = 'test description';
        testContentVersion.PathOnClient = './';
        testContentVersion.VersionData = EncodingUtil.base64Decode('Unit Test Attachment Body');

        insert testContentVersion;
        Test.stopTest();

        testContentVersion = [SELECT Id, Description FROM ContentVersion LIMIT 1];
        System.assert(testContentVersion.Description == 'test description');
    }
}