/**
* @File Name:   GS_Payment_Trigger_Handler.cls
* @Description:  This is a trigger handler for  Payment custom object
* @Author:      Halima Dosso, hdosso@deloitte.ca
* @Group:       Trigger
* @Last         Modified by:   Halima Dosso
* @Last         Modified time: 2018-12-17
* @Modification Log :
*-------------------------------------------------------------------------------------
* Ver           Date        Author             Modification
* 1.0       2018-12-17 Halima Dosso   Created the file and afterUpdateFilters Method/*/ 

public with sharing class GS_Payment_Trigger_Handler {

   /**
   * @Name          afterUpdateFilters
   * @Description   This method is called after an update on payment. It filters and call the appropriate service methods.
   * @Author        Halima Dosso, hdosso@deloitte.ca
   * @CreatedDate   2018-12-07
   * @Parameter     Map<Id,GS_Payment__c> paymentsMap,Map<Id,GS_Payment__c>  oldMap
   * @Return        
   */
	public static void  afterUpdateFilters(Map<Id,GS_Payment__c> paymentsMap,Map<Id,GS_Payment__c>  oldMap){
		Set<Id> paymentIds = new Set<Id>();
   		for(GS_Payment__c payment : paymentsMap.values() ){
   			if(payment.GS_Status__c != oldMap.get(payment.Id).GS_Status__c){
   				paymentIds.add(payment.Id);
   			}
   		}
   		if(!paymentIds.isEmpty()){
   			GS_Payment_Services.calculatePaidAndDisbursedAmount(paymentIds);
   		}
	}
}