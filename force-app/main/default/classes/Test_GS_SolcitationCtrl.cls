/**
* @File Name:   Test_GS_SolictationCtrl
* @Description:  Test_GS_SolictationCtrl
* @Author:     Aaron hallink ahallink@deloitte.ca
* @Group:       apex
* @Last         Modified by:   Aaron Hallink
* @Last         Modified time: 2019-11-19
* @Modification Log :
*-------------------------------------------------------------------------------------
* Ver           Date        Author             Modification
* 1.0       2019-11-19 	Aaron Hallink   	Created the file
*
*/

@IsTest
private class Test_GS_SolcitationCtrl {

    static testMethod void testSolicitationCtrlBehavior() {

        //there are no solicitations currently so this should return an empty list
        List<GS_Solicitation__c> retrievedSolicitations = GS_SolicitationCtrl.getSolicitationRelevant();
        //this sholud be true as it is empty
        System.assert(retrievedSolicitations.isEmpty());
    }

    static testMethod void testSolicitationWizardCtrlBehavior() {

        GS_Program__c testProgram = new GS_Program__c(Name='Test Program');
        insert  testProgram;

        //method is expecting a messy ID
        String bracketedId = '"['+testProgram.Id+']"';

        //this will return true since we just made the record
        System.assert(GS_SolicitationWizardCTRL.isRecordJustCreated(bracketedId));

        //want to wait a second to get a false back
        GS_Program__c testProgram2 = new GS_Program__c(Name='Test Program2');
        insert  testProgram2;

        bracketedId = '"['+testProgram2.Id+']"';

        //wait 10 seconds before checking if the record is new
        wait(10000);
        //this will return false since we waited a second
        System.assert(GS_SolicitationWizardCTRL.isRecordJustCreated(bracketedId) == false);


        //given a fake ID we also get true
        System.assert(GS_SolicitationWizardCTRL.isRecordJustCreated('FakeID'));


        List<GS_Program_Objectives__c> retrievedObjectives = GS_SolicitationWizardCTRL.getProgramObjectives(testProgram.Id);
        //there are no objectives right now so this will be true
        System.assert(retrievedObjectives.isEmpty());

        List<GS_Allocation__c> retrieveAllocations = GS_SolicitationWizardCTRL.getAllocations(testProgram.Id);
        //this will also return empty
        System.assert(retrieveAllocations.isEmpty());



    }
    //we need to delay checking the record that was created to get a false
    public static void wait(Integer millisec) {

        if(millisec == null || millisec < 0) {
            millisec = 0;
        }

        Long startTime = DateTime.now().getTime();
        Long finishTime = DateTime.now().getTime();
        while ((finishTime - startTime) < millisec) {
            //sleep for parameter x millisecs
            finishTime = DateTime.now().getTime();
        }
    }
}