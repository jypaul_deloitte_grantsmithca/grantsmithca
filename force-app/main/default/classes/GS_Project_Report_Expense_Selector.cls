/**
* @File Name:   GS_Project_Report_Expense_Selector.cls
* @Description:  This is a selector for  project report expense custom object (all the queries on this object are stored here)
* @Author:      Halima Dosso, hdosso@deloitte.ca
* @Group:       Apex class
* @Last         Modified by:   Halima Dosso
* @Last         Modified time: 2018-12-18
* @Modification Log :
*-------------------------------------------------------------------------------------
* Ver           Date        Author             Modification
* 1.0       2018-12-18  Halima Dosso   Created the file and added  getProjectReportExpenseById methods/*/ 

public with sharing class GS_Project_Report_Expense_Selector {

	public static Map<Id,GS_Project_Report_Expense__c> getProjectReportExpenseByProjectReportId(Set<Id> projectReportIds ) {
		return new Map<Id,GS_Project_Report_Expense__c>([SELECT Id,Name,Project_Budget_Line__c
			    FROM  GS_Project_Report_Expense__c 
			    WHERE Project_Report__c In: projectReportIds] );
	}
}