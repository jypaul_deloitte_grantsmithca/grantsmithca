/**
* @File Name:   GS_Submission_Trigger_Handler.cls
* @Description:  This is a trigger handler for  submission custom object
* @Author:      Halima Dosso, hdosso@deloitte.ca
* @Group:       Trigger
* @Last         Modified by:   Halima Dosso
* @Last         Modified time: 2019-01-21
* @Modification Log :
*-------------------------------------------------------------------------------------
* Ver           Date        Author             Modification
* 1.0       2018-12-18 	Halima Dosso   	Created the file ,  GS_JSON_ScorecardWrapper class and afterUpdateFilters Method/
* 1.1		    2019-01-10	Jean Su			    Added the trigger function to link submission to budget line
* 1.2       2019-01-17  Kevin Tsai      Added linkApplicationPdf()
* 1.3		    2019-01-18	Jean Su			    Added the trigger function to link submission to objective
* 1.4       2019-01-21  Halima Dosso     Added method afterInsertFilter and added more line to afterUpdateMethod
*
*           2019-10-15 aaron Hallink remove GS_Application_Objective__c,GS_Submission_Selector.cls references
*/ 

public without sharing class GS_Submission_Trigger_Handler {
   
/**
* @Name          afterUpdateFilters
* @Description   This method is called after an update on submission. It filters and call the appropriate service methods.
* @Author        Halima Dosso, hdosso@deloitte.ca
* @CreatedDate   2018-12-07
* @Parameter     Map<Id,GS_Submission__c> submissionsMap,Map<Id,GS_Submission__c> oldMap
* @Return        
*/
    public static void  afterUpdateFilters(Map<Id,GS_Submission__c> submissionsMap,Map<Id,GS_Submission__c> oldMap){
       for(GS_Submission__c submission : submissionsMap.values() ){
//           if(submission.GS_Application_JSON__c == null)
//           {
//               return;
//           }
           GS_Solicitation__c solicitation = [SELECT Id, Name FROM GS_Solicitation__c WHERE ID = :submission.GS_Solicitation__c];
           List<GS_Program_Official__c> programOfficials = new List<GS_Program_Official__c>();
        }

        Map<String,Map<String,String>> scorecardsAndRelatedScores = new Map<String,Map<String,String>> ();
        Map<Id,GS_Submission__c>submissions = new Map<Id,GS_Submission__c> ();
        for(GS_Submission__c submission : submissionsMap.values() ){
            /*if( (submission.GS_Status__c == GS_Constants.SUBMITTED_STATUS && submission.GS_Submission_Type__c==GS_Constants.SUBMISSION_SCORECARD_TYPE) &&
               !(oldMap.get(submission.Id).GS_Status__c == GS_Constants.SUBMITTED_STATUS && oldMap.get(submission.Id).GS_Submission_Type__c==GS_Constants.SUBMISSION_SCORECARD_TYPE)){
                   GS_JSON_ScorecardWrapper  submissionJSON = (GS_JSON_ScorecardWrapper) JSON.deserialize(submission.GS_JSON_Data__c, GS_JSON_ScorecardWrapper.class);
                   Map<String,String> scoresMap = new Map<String,String>();
                   scoresMap.put('Score',submissionJSON.projectTimeline.get('TotalScore'));
                   scoresMap.put('MaxScore',submissionJSON.projectTimeline.get('MaxScore'));
                   scoresMap.put('completedDateTime',String.valueof(submission.GS_Submission_date__c));
                   //ContextId is the scorecard Id related to the omniscript 
                   scorecardsAndRelatedScores.put(submissionJSON.contextId,scoresMap);
               }*/
            if( oldMap.get(submission.Id).GS_Status__c != GS_Constants.APPROVED_STATUS && submission.GS_Status__c == GS_Constants.APPROVED_STATUS ){
                submissions = GS_Submission_Selector.getSubmissionAndrelatedObjectivesAndBudgetLinesById(submissionsMap.keyset());
            }

        }
        /*if(scorecardsAndRelatedScores!=null){
            GS_Submission_Services.copyOverSubmissionToScorecards(scorecardsAndRelatedScores);	
        }*/
        if(submissions!=null){
            GS_Submission_Services.createAssociatedProjectAndRelated(submissions);

        }
    }

    /**
* @Name          afterInsertFilters
* @Description   This method is called after an insert on submission. It filters and call the appropriate service methods.
* @Author        Halima Dosso, hdosso@deloitte.ca
* @CreatedDate   2019-01-21
* @Parameter     Map<Id,GS_Submission__c> submissionsMap
* @Return        
*/
    public static void  afterInsertFilters(Map<Id,GS_Submission__c> submissionsMap){
        for(GS_Submission__c submission : submissionsMap.values() ){
//           if(submission.GS_Application_JSON__c == null)
//           {
//               return;
//           }
        }
        Map<Id,GS_Submission__c>submissions = new Map<Id,GS_Submission__c> ();
        for(GS_Submission__c submission : submissionsMap.values() ){
            if( submission.GS_Status__c == GS_Constants.APPROVED_STATUS){
                submissions = GS_Submission_Selector.getSubmissionAndrelatedObjectivesAndBudgetLinesById(submissionsMap.keyset());
            }
        }
        if(submissions!=null){
            System.debug('Submission Map: ' + submissions);
            GS_Submission_Services.createAssociatedProjectAndRelated(submissions);	
        }
    }
    
    
    /**
* @Name          linkToApplicationBudgetLines
* @Description   This method is called after an insert on submission. It links the submission with the application budget lines
* @Author        Jean Su, jesu@deloitte.ca
* @CreatedDate   2019-01-10
* @Parameter     List<GS_Submission__c> submissionsList
* @Return        void
*/
    public static void linkToApplicationBudgetLines(List<GS_Submission__c> submissionsList){
        for(GS_Submission__c submission : submissionsList)
        {
//           if(submission.GS_Application_JSON__c == null)
//           {
//               return;
//           }
        }
        Map<Id, Id> appIdSubIdMap = new Map<Id, Id>();
//        for (GS_Submission__c submission : submissionsList)
//        {
//            appIdSubIdMap.put(submission.GS_Application_JSON__c, submission.Id);
//        }
        System.debug(appIdSubIdMap);
        List<GS_Application_Budget_Line__c> budgetLines = GS_ApplicationsService.getApplicationBudgetLine(appIdSubIdMap.keySet());
        System.debug(budgetLines);
        for(GS_Application_Budget_Line__c budgetLine : budgetLines)
        {
//            budgetLine.GS_Submission__c = appIdSubIdMap.get(budgetLine.GS_vlocity_Application__c);
//            System.debug(budgetLine);
//            System.debug(appIdSubIdMap.get(budgetLine.GS_vlocity_Application__c));
        }
        update budgetLines;
    }
    
    /**
* @Name          linkToApplicationObjectives
* @Description   This method is called after an insert on submission. It links the submission with the application objectives
* @Author        Jean Su, jesu@deloitte.ca
* @CreatedDate   2019-01-18
* @Parameter     List<GS_Submission__c> submissionsList
* @Return        void
*/
    public static void linkToApplicationObjectives(List<GS_Submission__c> submissionsList){
        for(GS_Submission__c submission : submissionsList)
        {
//           if(submission.GS_Application_JSON__c == null)
//           {
//               return;
//           }
        }
        Map<Id, Id> appIdSubIdMap = new Map<Id, Id>();
//        for (GS_Submission__c submission : submissionsList)
//        {
//            appIdSubIdMap.put(submission.GS_Application_JSON__c, submission.Id);
//        }
        List<GS_Application_Objective__c> objectives = GS_ApplicationsService.getApplicationObjective(appIdSubIdMap.keySet());
        
        for(GS_Application_Objective__c objective : objectives)
        {
//            objective.GS_Submission__c = appIdSubIdMap.get(objective.GS_vlocity_Application__c);
        }
        update objectives;
    }
    
    /**
* @Name          linkApplicationPdf
* @Description   Links the PDF attached to the related vlocity Application of the Submission being inserted.
* @Author        Kevin Tsai, Deloitte
* @CreatedDate   2019-01-17
* @Parameter     List<GS_Submission__c> submissions
* @Return        
*/
    public static void linkApplicationPdf(List<GS_Submission__c> submissions) {
        for(GS_Submission__c submission : submissions)
        {
//           if(submission.GS_Application_JSON__c == null)
//           {
//               return;
//           }
        }
        //Set of Ids used to query the associated vlocity Application record to get the related ContentDocumentId
        Set<Id> vlocityAppIds = new Set<Id>();
        for(GS_Submission__c subm : submissions) {
//            if(subm.GS_Application_JSON__c!=null){
//               vlocityAppIds.add(subm.GS_Application_JSON__c);
//            }
        }
//        //System.debug('here'+vlocityAppIds);
//        if(!vlocityAppIds.isEmpty()) {
//                    System.debug('vlocityAppIds'+vlocityAppIds);
//
//            // Maps vlocity Application Ids to the attached PDF Ids
//            Map<Id,Id> appToDocMap = new Map<Id,Id>();
//            for(ContentDocumentLink cdl : [SELECT ContentDocumentId,LinkedEntityId FROM ContentDocumentLink
//                                           WHERE LinkedEntityId IN: vlocityAppIds]) {
//                                               appToDocMap.put(cdl.LinkedEntityId, cdl.ContentDocumentId);
//                                           }
//
//            if(!appToDocMap.isEmpty()) {
//                List<ContentDocumentLink> cdlsToInsert = new List<ContentDocumentLink>();
//                // Link the PDF to the Submissions
//                for(GS_Submission__c subm : submissions) {
//                    ContentDocumentLink cdl = new ContentDocumentLink();
//                    cdl.LinkedEntityId = subm.Id;
//                    //cdl.ContentDocumentId = appToDocMap.get(subm.GS_Application_JSON__c);
//                    cdl.ShareType = 'V';
//                    cdl.Visibility = 'AllUsers';
//                    cdlsToInsert.add(cdl);
//                }
//                insert cdlsToInsert;
//            }
//        }
    }
    
    
//    //Inner class to hold vlocity json structure in a scorecard ominiscript
//    //https://opfocus.com/json-deserialization-techniques-in-salesforce/
//    public class GS_JSON_ScorecardWrapper{
//        public String applicationId {get;set;}
//        public Map<String,String> reviewer {get;set;}
//        public Map<String,String> projectTimeline {get;set;}
//        public Map<String,String> needForProgramFunding {get;set;}
//        public Map<String,String> organizationHistory {get;set;}
//        public Map<String,String> projectSummary {get;set;}
//        public Map<String,String> conflictOfInterest {get;set;}
//        public Map<String,String> vlcTimeTracking {get;set;}
//        public Map<String,String> persistentComponent {get;set;}
//        public String isdtp {get;set;}
//        public String layout {get;set;}
//        public String omniCancelAction {get;set;}
//        public String omniIframeEmbedded {get;set;}
//        public String sfdcIFrameHost {get;set;}
//        public String sfdcIFrameOrigin {get;set;}
//        public String tour {get;set;}
//        public String userCurrencyCode {get;set;}
//        public String userTimeZone {get;set;}
//        public String userProfile {get;set;}
//        public String userName {get;set;}
//        public String userId {get;set;}
//        public String timeStamp {get;set;}
//        public String contextId {get;set;}
//    }
    public static void updateNewlyApprovedSubmissionFromTrigger(List<GS_Submission__c> newSubs , Map<Id,GS_Submission__c> oldSubMap,boolean isUpdate, boolean isInsert, boolean isDelete, boolean isAfter){
        //this is to get the IDS of the submissions in a set
        Set<Id> submissionIds = new Set<Id>();
            //don't do this for every updated, only when we go from false to true
            for(GS_Submission__c sub: newSubs){
                if(sub.GS_In_Approval__c == true && oldSubMap.get(sub.Id).GS_In_Approval__c != true){
                    submissionIds.add(sub.Id);
                }
            }

        System.debug('Submission Ids; ' + submissionIds);


        if(submissionIds.isEmpty() == false){
            List<User> ownerObjects = new List<User>();

            //Get the In Approval field
            //Get Submission object from ID
            List<GS_Submission__c> submissionToUpdate = [SELECT Id, GS_Status__c, GS_Solicitation__c,GS_In_Approval__c FROM GS_Submission__c WHERE Id IN :submissionIds];

            List<GS_Program_Official__c> programOfficials = 
                    GS_Submission_Trigger_Handler.getProgramOfficialUsersForSubmissionApproval(submissionToUpdate);

            //can only set 1 reviewer
//            for(Integer i = 0;i < programOfficials.size();i++){
//            
            if (programOfficials.size() >0 ){
                User userToUpdate = new User(Id=UserInfo.getUserId());
                //the special on user, only do it for the first record of officals in the list
                userToUpdate.Special_Submission_Approver__c = programOfficials.get(0).GS_User__c;
                //we only want to add a user once
                if(ownerObjects.contains(userToUpdate) == false){
                    ownerObjects.add(userToUpdate);
                }
            }
            //
//            }
//            


            // only do stuff if list is not empty
            if(ownerObjects.isEmpty() == false) {
                update ownerObjects;
            }
        }



    }

    private static List<GS_Program_Official__c> getProgramOfficialUsersForSubmissionApproval(List<GS_Submission__c> submissions){

        Set<Id> relatedSolicitaionIds = new Set<Id>();

        for(GS_Submission__c submission : submissions){
            relatedSolicitaionIds.add(submission.GS_Solicitation__c);
        }

        List<GS_Program_Official__c> listOfProgramOfficials = new List<GS_Program_Official__c>();
        listOfProgramOfficials = [SELECT Id, GS_User__c, GS_Solicitation__c, GS_Program__c,GS_Status__c,Internal_Approver_Role__c FROM GS_Program_Official__c
        WHERE GS_Solicitation__c in :relatedSolicitaionIds
        AND Internal_Approver_Role__c = :GS_Constants.PROGRAM_OFFICIAL_SUBMIT_APPROVER AND GS_Status__c = :GS_Constants.ACTIVE_STATUS];
        return listOfProgramOfficials;
    }
}