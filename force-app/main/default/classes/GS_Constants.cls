public class GS_Constants {
	public static String taskCompleted 		= 'completed';
    public static String NOT_STARTED_STATUS = 'Not Started';
	public static String IN_PROGRESS_STATUS	= 'In Progress';
	public static String RECALLED_STATUS	= 'Recalled';
	public static String ABANDONED_STATUS	= 'Abandoned';
	public static String DENIED_STATUS		= 'Denied';
    public static String OPEN_STATUS 		= 'Open';
	public static String READY_FOR_REVIEW	= 'Ready for Review';
	public static String SCORING_STATUS		= 'Scoring';
	public static String BUDGET_APPROVAL 	= 'Budget Approval';
	public static String COMPLETED_STATUS 	= 'Completed';
	public static String PAID_AMOUNT_ROLLUP_CALCULATION = 'GS_Project_Report_Expense_Paid_Amount';
	public static String DISBURSED_AMOUNT_ROLLUP_CALCULATION = 'GS_Project_Budget_Line_Disbursed_Amount';
	public static String APPROVED_EXPENSES_ROLLUP_CALCULATION = 'GS_Project_Budget_Line_Approved_Amount';
	public static String REIMBURSED_EXPENSES_ROLLUP_CALCULATION = 'GS_Project_Budget_Line_Reimbursed_Amount';
    public static String SUBMISSION_SCORECARD_TYPE = 'Scorecard';
  	public static String SUBMITTED_STATUS 	= 'Submitted';
    public static String LOGO_TITLE			= 'Logo';
	public static string APPROVED_STATUS = 'Approved';
	public static string ACTIVE_STATUS = 'Active';
	public static String PROGRAM_OFFICIAL_SUBMIT_APPROVER = 'Project Administrator';

	//Community and Article Record Types
	public static String COMMUNITY_ID = Network.getNetworkId();
	public static String GRANTEE_PORTAL_NAME = 'GranteePortal';
	public static String REVIEWER_PORTAL_NAME = 'ReviewerPortal';
	public static String GENERAL_ARTICLES = 'GS_General';
	public static String GRANTEE_PORTAL_ARTICLES = 'GS_Grantee';
	public static String REVIEWER_PORTAL_ARTICLES = 'GS_Reviewer';

	//community Profile Name
	public static String GRANTEE_PORTAL_PROFILE = 'Grantee Community User';

	// Custom Metadata Names
	public static String COMMUNITY_URL = 'GS_Communities_Urls';

}