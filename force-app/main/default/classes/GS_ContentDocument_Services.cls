/**
* @File Name:   GS_ContentDocument_Services.cls
* @Description:  This class used as a service class for ContentDocument object
* @Author:      Kevin Tsai, Deloitte
* @Group:       Apex
* @Last         Modified by:   Kevin Tsai
* @Last         Modified time: 2019-01-16
* @Modification Log :
*-------------------------------------------------------------------------------------
* Ver           Date        Author             Modification
* 1.0       2019-01-16  Kevin Tsai   Created the file/class */ 

public with sharing class GS_ContentDocument_Services {
	
	/**
	*  @description Service method to get External Url of a ContentDocument
	*  @author      Kevin Tsai, Deloitte
	*  @date        2019-01-16
	*  @group       Methods
	*  @return      String
	*/
	public static ContentDistribution getPdfContentDist(String scorecardId){
		Set<Id> contentDocIds = new Set<Id>(); 
		for (ContentDocumentLink cdl : [SELECT ContentDocumentId 
										FROM ContentDocumentLink 
										WHERE LinkedEntityId =: scorecardId]) {
			contentDocIds.add(cdl.ContentDocumentId);
		}
		if (contentDocIds.isEmpty() || contentDocIds == null || contentDocIds.size() < 1) {
			return null;
		}

		List<ContentDocument> attachedPdfId = ([SELECT Id, FileType, Title
						   				FROM ContentDocument
						   				WHERE Id IN :contentDocIds
						   				AND FileType = 'PDF'
						   				ORDER BY CreatedDate DESC
						   				LIMIT 1]);
		if (attachedPdfId.isEmpty() || attachedPdfId == null || attachedPdfId.size() < 1) {
			return null;
		}

		List<ContentDistribution> attachedPdfDist = [SELECT ContentDocumentId, ContentVersionId, DistributionPublicUrl, Id, Name, RelatedRecordId 
				FROM ContentDistribution
				WHERE ContentDocumentId =: attachedPdfId[0].Id];
		if (attachedPdfDist.isEmpty() || attachedPdfDist == null || attachedPdfDist.size() < 1) {
			return null;
		}

		return attachedPdfDist[0];
	}
}