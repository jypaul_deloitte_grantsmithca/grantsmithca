/**
* @File Name:	TEST_GS_KnowledgeBaseCTRL.cls
* @Description:  test class for GS_KnowledgeBaseCTRL
* @Author:   	Aaron Hallink, ahallink@deloitte.ca
* @Group:   	Apex
* @Modification Log	:
*-------------------------------------------------------------------------------------
* Ver       	Date        Author             Modification
* 1.0       2019-11-07 Aaron Hallink     Test class for GS_KnowledgeBaseCTRL
*
*/

@IsTest
private class Test_GS_KnowledgeBaseCTRL {

    static testMethod void test_getKnowledgeArticles() {

        //this user is used to avoid mixed DML
        //https://stackoverflow.com/questions/2387475/how-to-avoid-mixed-dml-operation-error-in-salesforce-tests-that-create-users
        User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];

        //Create portal account owner
        UserRole portalRole = [Select Id,Name From UserRole Where Name = 'SVP Customer Experience' Limit 1];
        Profile profile1 = [Select Id from Profile where name = 'System Administrator'];
        User portalAccountOwner1 = new User(
                UserRoleId = portalRole.Id,
                ProfileId = profile1.Id,
                Username = System.now().millisecond() + 'test2@test123123123123123123123123.com',
                Alias = 'batman',
                Email='bruce.wayne@wayneenterprises.com',
                EmailEncodingKey='UTF-8',
                Firstname='Bruce',
                Lastname='Wayneuuu',
                LanguageLocaleKey='en_US',
                LocaleSidKey='en_US',
                TimeZoneSidKey='America/Chicago'
        );
        insert portalAccountOwner1 ;

        //Create account
        Account portalAccount1 = new Account(
                Name = 'TestAccount',
                OwnerId = portalAccountOwner1.Id
        );
        //https://stackoverflow.com/questions/2387475/how-to-avoid-mixed-dml-operation-error-in-salesforce-tests-that-create-users
        System.runAs(thisUser){
            insert portalAccount1;
        }


//Create contact
        Contact contact1 = new Contact(
                FirstName = 'Test',
                Lastname = 'McTesty',
                AccountId = portalAccount1.Id,
                Email = System.now().millisecond() + 'test@test.com'
        );
        //https://stackoverflow.com/questions/2387475/how-to-avoid-mixed-dml-operation-error-in-salesforce-tests-that-create-users
        System.runAs(thisUser){
            insert contact1;
        }

    //Create user
        Id profile = [select id from profile where name=:GS_Constants.GRANTEE_PORTAL_PROFILE].id;
        User user1 = new User(
                Username = System.now().millisecond() + 'test12345@test.com',
                ContactId = contact1.Id,
                ProfileId = profile,
                Alias = 'test123',
                Email = 'test12345@test.com',
                EmailEncodingKey = 'UTF-8',
                firstName='testy',
                LastName = 'McTesty',
                CommunityNickname = 'test12345',
                TimeZoneSidKey = 'America/Los_Angeles',
                LocaleSidKey = 'en_US',
                LanguageLocaleKey = 'en_US'

        );
        //https://stackoverflow.com/questions/2387475/how-to-avoid-mixed-dml-operation-error-in-salesforce-tests-that-create-users
        System.runAs(thisUser){
            insert user1;
            System.debug('Community ID: ' + Network.getNetworkId());
        }


        List<NetworkMember> MemberList = [SELECT Network.Name,Member.Name FROM NetworkMember];

        for(NetworkMember member : MemberList){
            System.debug('Member : ' + member.Member.Name + ' Network: '+ member.Network.Name );
        }

        //lets try to get the URL so we can maybe have the test user have Network.getNetworkId()); return the grantee community
        Network myNetwork = [SELECT Id FROM Network WHERE Name = 'GranteePortal' ];
        //get the URL minus the LOGIN appended at the end
        System.debug('Community URL: ' + Network.getLoginUrl(myNetwork.id).substring(0,Network.getLoginUrl(myNetwork.id).length()-5));
        System.PageReference pageRef = new PageReference(Network.getLoginUrl(myNetwork.id));

        //insert the knowledge record to retrieve in after we run the controller
        Knowledge__kav TestArticle = new Knowledge__kav();
        TestArticle.RecordTypeId = Schema.SObjectType.Knowledge__kav.getRecordTypeInfosByDeveloperName().get(GS_Constants.GRANTEE_PORTAL_ARTICLES).getRecordTypeId();
        TestArticle.Title = 'Test Article';
        TestArticle.UrlName = 'Test-Article';
        TestArticle.IsVisibleInCsp = true;


        //https://stackoverflow.com/questions/2387475/how-to-avoid-mixed-dml-operation-error-in-salesforce-tests-that-create-users
        System.runAs(thisUser){
            insert TestArticle;
            TestArticle = [SELECT Id,Title,KnowledgeArticleId FROM Knowledge__kav WHERE id =: TestArticle.Id];
            KbManagement.PublishingService.publishArticle(TestArticle.KnowledgeArticleId,true);
        }

        //this also does not help getting the Network.getNetworkId() command to return an actual Community ID rather than null
        Test.setCurrentPage(pageRef);


        system.runAs(user1) {
            // statements to be executed by this test user.
            //still always seems to return null despite efforts
            System.debug('Community ID: ' + Network.getNetworkId());

           List<Knowledge__kav> lstArticles = GS_KnowledgeBaseCTRL.getKnowledgeArticles();
            //we should have at least one entry
            System.assert(!lstArticles.isEmpty());
            System.assert(lstArticles.get(0).Title == 'Test Article');

        }



    }
}