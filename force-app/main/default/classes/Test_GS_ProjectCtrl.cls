/**
* @File Name:	Test_GS_ProjectCtrl.cls
* @Description: This class used as test coverage for the controller for the Project lightning component
* @Author:   	Benjamin Hagel, bhagel@deloitte.ca
* @Group:   	Apex
* @Last 		Modified by:   Benjamin Hagel
* @Last 		Modified time: 2019-12-04
* @Modification Log	:
*-------------------------------------------------------------------------------------
* Ver       	Date        Author             Modification
* 1.0       2019-12-04  	Benjamin Hagel    	Created the file/class
*/
@IsTest
private class Test_GS_ProjectCtrl {
    @IsTest
    static void testBehavior() {


        //New Program
        GS_Program__c testProgram = new GS_Program__c(Name='Test Program');
        insert testProgram;

        //New Solicitation
        GS_Solicitation__c testSolicitation = new GS_Solicitation__c(Name='Test Solicitations',GS_Program__c=testProgram.id);
        insert testSolicitation;

        //Init data
        List<GS_Project__c> testProjects = new List<GS_Project__c>();
        GS_Project__c newProject = new GS_Project__c(Name='Test Program', Solicitation__c = testSolicitation.Id);
        //newProject.GS_Program__c = newProgram.Name;
        insert newProject;



        Test.startTest();
        List<GS_Project_Services.GS_ProjectWrapper> openProjects = GS_ProjectCtrl.getOpenProject();
        Test.stopTest();

        GS_ProjectCtrl testProjectControl = new GS_ProjectCtrl();

        System.assert(openProjects.get(0).project.id == newProject.id);


        //do the call again but this time there will be an attachg=ment with the name logo
        //INSERT a contennt document link so it eneters else statmeent
        //need an attachment on the expense to hit the files wrapper lines
        ContentVersion contentVersion = new ContentVersion(
                Title = GS_Constants.LOGO_TITLE,
                PathOnClient = 'logo.jpg',
                VersionData = Blob.valueOf('Test Content'),
                IsMajorVersion = true
        );
        insert contentVersion;
        List<ContentDocument> documents = [SELECT Id, Title, LatestPublishedVersionId FROM ContentDocument];
        //create ContentDocumentLink  record
        ContentDocumentLink cdl = New ContentDocumentLink();
        //we only have one scorecard to choose from.
        cdl.LinkedEntityId = testSolicitation.Id;
        cdl.ContentDocumentId = documents[0].Id;
        cdl.shareType = 'V';
        insert cdl;

        openProjects = GS_ProjectCtrl.getOpenProject();
        System.assert(openProjects.get(0).project.id == newProject.id);
    }
}