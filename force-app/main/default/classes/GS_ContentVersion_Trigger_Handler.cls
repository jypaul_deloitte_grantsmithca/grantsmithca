/**
* @File Name:   GS_ContentVersion_Trigger.cls
* @Description:  This is a trigger handler for the standard ContentVersion object
* @Author:      Kevin Tsai, Deloitte
* @Group:       Trigger Handler
* @Last         Modified by:   Kevin Tsai
* @Last         Modified time: 2019-01-16
* @Modification Log :
*-------------------------------------------------------------------------------------
* Ver           Date        Author             Modification
* 1.0      	 2019-01-16   Kevin Tsai      Created the file/class
*/ 

public with sharing class GS_ContentVersion_Trigger_Handler {

	/**
   * @Name          createContentDistribution
   * @Description   This method is called after an insert on ContentVersion (Notes and Attachements)
   *				It creates a related ContentDistribution record to generate an external link of the file
   * @Author        Kevin Tsai, Deloitte
   * @CreatedDate   2019-01-16
   * @Parameter     List<ContentVersion> contentVersionList
   * @Return        void
   */
   public static void createContentDistribution(List<ContentVersion> contentVersionList){
   	List<ContentDistribution> contentDistributions = new List<ContentDistribution>();
   	for (ContentVersion cv : contentVersionList) {
	   	ContentDistribution cd = new ContentDistribution();
		cd.Name = cv.Title;
		cd.ContentVersionId = cv.Id;
		cd.PreferencesAllowViewInBrowser = true;
		cd.PreferencesLinkLatestVersion = true;
		cd.PreferencesNotifyOnVisit = false;
		cd.PreferencesPasswordRequired = false;
		cd.PreferencesAllowOriginalDownload = true;
		cd.PreferencesAllowPDFDownload = true;
		contentDistributions.add(cd);
	}
	insert contentDistributions;
   }
}