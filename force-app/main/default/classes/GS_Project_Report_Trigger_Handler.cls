/**
* @File Name:   GS_Project_Report_Trigger_Handler.cls
* @Description:  This is a trigger handler for  project report  custom object
* @Author:      Halima Dosso, hdosso@deloitte.ca
* @Group:       Trigger
* @Last         Modified by:   Halima Dosso
* @Last         Modified time: 2018-12-17
* @Modification Log :
*-------------------------------------------------------------------------------------
* Ver           Date        Author             Modification
* 1.0       2018-12-17 Halima Dosso   Created the file and afterUpdateFilters Method/*/ 

public with sharing class GS_Project_Report_Trigger_Handler {


   /**
   * @Name          afterUpdateFilters
   * @Description   This method is called after an update on projet report. It filters and call the appropriate service methods.
   * @Author        Halima Dosso, hdosso@deloitte.ca
   * @CreatedDate   2018-12-07
   * @Parameter     Map<Id,GS_Project_Report__c> projectReportMap,Map<Id , GS_Project_Report__c> oldMap
   * @Return        
   */
	public static void  afterUpdateFilters(Map<Id,GS_Project_Report__c> projectReportMap,Map<Id,GS_Project_Report__c>  oldMap){
		Set<Id> projectReportIds = new Set<Id>();
   		for(GS_Project_Report__c projectReport : projectReportMap.values() ){
   			if(projectReport.Status__c != oldMap.get(projectReport.Id).Status__c){
   				projectReportIds.add(projectReport.Id);
   			}
   		}
   		if(!projectReportIds.isEmpty()){
   			GS_Project_Report_Service.calculateApprovedAndReimbursedExpenses(projectReportIds);
   		}
	}
}