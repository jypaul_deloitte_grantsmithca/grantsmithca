/**
* @File Name:	GS_ApplicationsCtrl.cls
* @Description:  This class used as controller for Solicitation lightning component
* @Author:   	Mohamed Zibouli, mzibouli@deloitte.ca
* @Group:   	Apex
* @Modification Log	:
*-------------------------------------------------------------------------------------
* Ver       	Date        Author             Modification
* 1.0       2018-12-04  Mohamed Zibouli    	Created the file/class
* 1.1		2019-12-20	Diego Villarroel	Reworked Submission
*/
public with sharing class GS_ApplicationsCtrl {

	/**
	 * @description		Method created for Application Table, get solicitations and wrap them into an object
	 * @author 			Diego Villarroel, dvillarroeld@deloitte.com
	 * @return			SubmissionWrapper
	 * @Last			Added new conditions for Wrapper, new Constants and Indentation
	 */
	@AuraEnabled
    public static Map<String, List<SubmissionWrapper>> getSubmissions(){
		Map<String, List<SubmissionWrapper>> submissionsWrap = new Map<String, List<SubmissionWrapper>>();

        User currentUser = GS_Utility.getCurrentUser();
        List<GS_Submission__c> submissions = GS_ApplicationsService.getSubmissionsByCreatedById(currentUser.Id);

		List<SubmissionWrapper> inProgressSubmissions = new List<SubmissionWrapper>();
		List<SubmissionWrapper> inReviewSubmissions = new List<SubmissionWrapper>();
		List<SubmissionWrapper> deniedSubmissions = new List<SubmissionWrapper>();

        for(GS_Submission__c submission : submissions) {
			String dtaString = '';
			String dueString = '';

			try {
				Date dueDate = Date.newInstance(submission.GS_Solicitation__r.GS_Application_Due_Date_Time__c.year(),
												submission.GS_Solicitation__r.GS_Application_Due_Date_Time__c.month(),
												submission.GS_Solicitation__r.GS_Application_Due_Date_Time__c.day());
				Integer dueInteger = Date.Today().daysBetween(dueDate);
				if (dueInteger > 1) dueString = dueInteger.format() + ' Days';
				else if (dueInteger == 1) dueString = dueInteger.format() + ' Day';
				else if (dueInteger == 0) dueString = 'Today';
				else if (dueInteger == -1)  dueString =  (dueInteger*-1) + ' Day of Overdue';
				else if (dueInteger < 0)  dueString =  (dueInteger*-1) + ' Days of Overdue';
			} catch (Exception ex) {
				dueString = 'Not Defined';
			}

			try {
				Integer dtaInteger = Date.Today().daysBetween(submission.GS_Solicitation__r.GS_Awarded_Date__c);
				if (dtaInteger > 1) dtaString = dtaInteger.format() + ' Days';
				else if (dtaInteger == 1) dtaString = dtaInteger.format() + ' Day';
				else if (dtaInteger == 0) dtaString = 'Today';
				else if (dtaInteger == -1) dtaString = (dtaInteger*-1) + ' Day of Overdue';
				else if (dtaInteger < 0) dtaString = (dtaInteger*-1) + ' Days of Overdue';
			} catch(Exception ex) {
				dtaString = 'Not Defined';
			}

			if(submission.GS_Status__c == GS_Constants.IN_PROGRESS_STATUS || submission.GS_Status__c == GS_Constants.RECALLED_STATUS){
				SubmissionWrapper sw = new SubmissionWrapper();
				sw.daysToAward = dtaString;
				sw.deadline = dueString;
				sw.submission = submission;
				inProgressSubmissions.add(sw);
			}
			if(submission.GS_Status__c == GS_Constants.SUBMITTED_STATUS || submission.GS_Status__c == GS_Constants.SCORING_STATUS
					|| submission.GS_Status__c == GS_Constants.READY_FOR_REVIEW || submission.GS_Status__c == GS_Constants.BUDGET_APPROVAL){
				SubmissionWrapper sw = new SubmissionWrapper();
				sw.daysToAward = dtaString;
				sw.deadline = dueString;
				sw.submission = submission;
				inReviewSubmissions.add(sw);
			}
			if(submission.GS_Status__c == GS_Constants.DENIED_STATUS || submission.GS_Status__c == GS_Constants.ABANDONED_STATUS){
				SubmissionWrapper sw = new SubmissionWrapper();
				sw.daysToAward = dtaString;
				sw.deadline = dueString;
				sw.submission = submission;
				deniedSubmissions.add(sw);
			}
        }
		if(!inProgressSubmissions.isEmpty()) submissionsWrap.put('In_Progress', inProgressSubmissions);
		if(!inReviewSubmissions.isEmpty()) submissionsWrap.put('In_Review', inReviewSubmissions);
		if(!deniedSubmissions.isEmpty()) submissionsWrap.put('Denied', deniedSubmissions);

        return submissionsWrap;
    }

	/**
	 * @description 	Internal Class for Solicitations Wrapper
	 * @author 			Diego Villarroel, dvillarroeld@deloitte.com
	 */
    public class SubmissionWrapper {
		@AuraEnabled public String daysToAward;
		@AuraEnabled public String deadline;
		@AuraEnabled public GS_Submission__c submission = new GS_Submission__c();
    }
}