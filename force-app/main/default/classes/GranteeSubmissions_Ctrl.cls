public with sharing class GranteeSubmissions_Ctrl {
    
    @AuraEnabled(cacheable=true)
    public static List<GS_Submission__c> getGranteeSubmissions(){
        User currentUser = GS_Utility.getCurrentUser();

        try {
            List<GS_Submission__c> submissions = new List<GS_Submission__c>([select Id, Name, GS_Submission_date__c, GS_Status__c, GS_Solicitation__r.Name 
                                                    from GS_Submission__c where OwnerId =: currentUser.Id and GS_Status__c != :GS_Constants.APPROVED_STATUS]);
            
            return submissions;
            
        } catch (Exception e) {
            system.debug('*** - GranteeSubmissions_Ctrl -getGranteeSubmissions - exception line: '+e.getLineNumber() + ', message : '+ e.getMessage());
            throw new AuraHandledException(e.getMessage());
        }
    }

    @AuraEnabled
    public static List<GS_Application_Budget_Line__c> getSubmissionBudgetLines(String submissionId){
        try {

            List<GS_Application_Budget_Line__c> submissionLines = new list<GS_Application_Budget_Line__c>([select Id, Name, GS_Submission__c, GS_Budget_Category__c, GS_Requested_Amount__c, GS_Solicitation_Obligation__r.Name, GS_Submission__r.Name
                                                                    from GS_Application_Budget_Line__c where GS_Submission__c = :submissionId]);
            return submissionLines;
            
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }
}