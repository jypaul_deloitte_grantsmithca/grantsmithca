/**
* @File Name:	GS_ScoreCardCtrl.cls
* @Description:  This class used as controller for Scorecard lightning component
* @Author:   	Jean Su, jesu@deloitte.ca
* @Group:   	Apex
* @Last 		Modified by:   Jean Su
* @Last 		Modified time: 2018-12-14
* @Modification Log	:
*-------------------------------------------------------------------------------------
* Ver       	Date        Author             Modification
* 1.0       2018-12-14  	Jean Su    		Created the file/class
*/
public with sharing class GS_ScoreCardCtrl {
	
    /**
    *  @description controller method to get incomplete Scorecard for current user
    *  @author      Jean Su, Deloitte
    *  @date        2018-12-14
    *  @group       Methods
    *  @return      List<GS_Scorecard__c>
    */
    @AuraEnabled
    public static List<GS_Scorecard__c> getIncompleteScorecard()
    {
        User currentUser = GS_Utility.getCurrentUser();
        return GS_ScoreCard_Services.getIncompleteScorecardByOwnerId(currentUser.Id);
    }

    /**
    *  @description  Controller method to get Scorecard by Id
    *  @author      Kevin Tsai, Deloitte
    *  @date        2019-01-15
    *  @group       Methods
    *  @return      GS_Scorecard__c
    */
    @AuraEnabled
    public static GS_Scorecard__c getScorecardById(String scorecardId) {
        return GS_ScoreCard_Services.getScorecardById(scorecardId);
    }

    /**
    *  @description  Controller method to get ContentDistribution record
    *                of the pdf attached to the Scorecard
    *  @author      Kevin Tsai, Deloitte
    *  @date        2019-01-15
    *  @group       Methods
    *  @return      GS_Scorecard__c
    */
    @AuraEnabled
    public static ContentDistribution getPdfUrlById(String scorecardId) {
        return GS_ContentDocument_Services.getPdfContentDist(scorecardId);
    }
}