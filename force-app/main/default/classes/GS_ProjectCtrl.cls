/**
* @File Name:	GS_ProjectCtrl.cls
* @Description: This class used as controller for Project lightning component
* @Author:   	Jean Su, jesu@deloitte.ca
* @Group:   	Apex
* @Last 		Modified by:   Jean Su
* @Last 		Modified time: 2019-01-01
* @Modification Log	:
*-------------------------------------------------------------------------------------
* Ver       	Date        Author             Modification
* 1.0       2019-01-01  	Jean Su    		Created the file/class
*/
public with sharing class GS_ProjectCtrl {
	
    /**
    *  @description controller method to get open Project for current user
    *  @author      Jean Su, Deloitte
    *  @date        2019-01-01
    *  @group       Methods
    *  @return      List<GS_Project__c>
    */
    @AuraEnabled
    public static List<GS_Project_Services.GS_ProjectWrapper> getOpenProject()
    {
        User currentUser = GS_Utility.getCurrentUser();
        return GS_Project_Services.getOpenProjectByOwnerId(currentUser.Id);
    }
}