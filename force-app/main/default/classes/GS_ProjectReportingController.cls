/**
* @File Name:	GS_ProjectReportingController
* @Description: This class used as controller for Project reporting lightning component
* @Author:      Aaron Hallink
* @Group:   	Apex
* @Modification Log	:
*-------------------------------------------------------------------------------------
* Ver       	Date        Author             Modification
* 1.0       2019-12-02  	Hallink    		Created the file/class
*/

public with sharing class GS_ProjectReportingController {

/**
    *  @description controller method to get profile of current user
    *  @author      Aaron Hallink
    *  @date        2019-12-02
    *  @group       Methods
    *  @return      String
    */
    //depending on which profile is looking at the component it will have different behaivours
    @AuraEnabled
    public static String getCurrentUserProfileName(){

        //return the name of the current profile ID;
        Id profileId=userinfo.getProfileId();
        return [Select Id,Name from Profile where Id=:profileId].Name;


    }

    /**
    *  @description controller method to get current project record
    *  @author      Aaron Hallink
    *  @date        2019-12-02
    *  @group       Methods
    *  @return      GS_Project__c
    */
    @AuraEnabled
    public static GS_Project_Report__c getProjectReportRecord(String recordId){

        //this is for the edit page, we need to get the project ID since it can't be fed in
        return [SELECT Id, Due_Date__c, Project__c,Project__r.Name,Project__r.Id, Name, Reporting_Period_End_Date__c, Reporting_Period_Start_Date__c, Status__c FROM GS_Project_Report__c WHERE ID = :recordId];
    }


    /**
    *  @description controller method to get current project record
    *  @author      Aaron Hallink
    *  @date        2019-12-02
    *  @group       Methods
    *  @return      GS_Project__c
    */
    @AuraEnabled
    public static GS_Project__c getProjectRecord(String recordId){

        //return everything from the project record
        return [SELECT Id,
                Account__c,
                GS_Submission__c,
                GS_Budgeted_Cash_Match__c,
                GS_Budgeted_In_Kind_Match__c,
                GS_Budgeted_Obligations__c,
                CreatedById,
                CreatedDate,
                IsDeleted,
                GS_Deobligated_Amount__c,
                GS_Disbursed_Amount__c,
                End_Date__c,
                GS_Expensed_Cash_Match__c,
                GS_Expensed_In_Kind_Match__c,
                GS_Expensed_Obligations__c,
                LastActivityDate,
                LastModifiedById,
                LastModifiedDate,
                LastReferencedDate,
                LastViewedDate,
                OwnerId,
                GS_Paid_Amount__c,
                GS_Program__c,
                Name,
                GS_Refunded_Amount__c,
                GS_Reimbursed_Expenses__c,
                GS_Remaining_Expenses__c,
                GS_Remaining_Payments__c,
                Solicitation__c,
                Spend_Down_Status__c,
                Start_Date__c,
                Status__c,
                Summary_Description__c,
                SystemModstamp,
                GS_Total_Project_Budget__c,
                Account__r.Id,
                Account__r.Description,
                Account__r.Fax,
                Account__r.Name,
                Account__r.AccountNumber,
                Account__r.Phone,
                Account__r.Rating,
                Account__r.Site,
                Account__r.AccountSource,
                Account__r.Type,
                Account__r.AnnualRevenue,
                Account__r.BillingAddress,
                Account__r.BillingCity,
                Account__r.BillingCountry,
                Account__r.BillingGeocodeAccuracy,
                Account__r.BillingLatitude,
                Account__r.BillingLongitude,
                Account__r.BillingState,
                Account__r.BillingStreet,
                Account__r.BillingPostalCode,
                Account__r.CreatedById,
                Account__r.CreatedDate,
                Account__r.Jigsaw,
                Account__r.IsDeleted,
                Account__r.NumberOfEmployees,
                Account__r.Industry,
                Account__r.JigsawCompanyId,
                Account__r.LastActivityDate,
                Account__r.LastModifiedById,
                Account__r.LastModifiedDate,
                Account__r.LastReferencedDate,
                Account__r.LastViewedDate,
                Account__r.MasterRecordId,
                Account__r.OwnerId,
                Account__r.Ownership,
                Account__r.ParentId,
                Account__r.PhotoUrl,
                Account__r.ShippingAddress,
                Account__r.ShippingCity,
                Account__r.ShippingCountry,
                Account__r.ShippingGeocodeAccuracy,
                Account__r.ShippingLatitude,
                Account__r.ShippingLongitude,
                Account__r.ShippingState,
                Account__r.ShippingStreet,
                Account__r.ShippingPostalCode,
                Account__r.Sic,
                Account__r.SicDesc,
                Account__r.SystemModstamp,
                Account__r.TickerSymbol,
                Account__r.Website,
                GS_Program__r.Id,
                GS_Program__r.CreatedById,
                GS_Program__r.CreatedDate,
                GS_Program__r.IsDeleted,
                GS_Program__r.GS_Description_of_the_program__c,
                GS_Program__r.LastActivityDate,
                GS_Program__r.LastModifiedById,
                GS_Program__r.LastModifiedDate,
                GS_Program__r.LastReferencedDate,
                GS_Program__r.LastViewedDate,
                GS_Program__r.OwnerId,
                GS_Program__r.Name,
                GS_Program__r.GS_Status__c,
                GS_Program__r.SystemModstamp,
                GS_Submission__r.Id,
                GS_Submission__r.GS_Applicant__c,
                GS_Submission__r.GS_Average_Score__c,
                GS_Submission__r.CreatedById,
                GS_Submission__r.CreatedDate,
                GS_Submission__r.IsDeleted,
                GS_Submission__r.GS_JSON_Data__c,
                GS_Submission__r.LastActivityDate,
                GS_Submission__r.LastModifiedById,
                GS_Submission__r.LastModifiedDate,
                GS_Submission__r.LastReferencedDate,
                GS_Submission__r.LastViewedDate,
                GS_Submission__r.GS_Number_of_Completed_Scorecards__c,
                GS_Submission__r.OwnerId,
                GS_Submission__r.GS_Reached_Minimum_Reviews_Required__c,
                GS_Submission__r.GS_Solicitation__c,
                GS_Submission__r.GS_Status__c,
                GS_Submission__r.GS_Submission_date__c,
                GS_Submission__r.Name,
                GS_Submission__r.GS_Submission_Type__c,
                GS_Submission__r.SystemModstamp,
                GS_Submission__r.GS_Total_Score__c,
                Solicitation__r.Id,
                Solicitation__r.GS_Application_Due_Date_Time__c,
                Solicitation__r.GS_Application_Form_Template_URL__c,
                Solicitation__r.GS_Available_Amount__c,
                Solicitation__r.GS_Awarded_Date__c,
                Solicitation__r.GS_Close_Date__c,
                Solicitation__r.CreatedById,
                Solicitation__r.CreatedDate,
                Solicitation__r.IsDeleted,
                Solicitation__r.GS_Description__c,
                Solicitation__r.LastActivityDate,
                Solicitation__r.LastModifiedById,
                Solicitation__r.LastModifiedDate,
                Solicitation__r.LastReferencedDate,
                Solicitation__r.LastViewedDate,
                Solicitation__r.Minimum_Reviews_Required__c,
                Solicitation__r.OwnerId, Solicitation__r.GS_Program__c,
                Solicitation__r.GS_Score_Card_Template_URL__c,
                Solicitation__r.Name,
                Solicitation__r.GS_Solicitation_Type__c,
                Solicitation__r.GS_Start_Date__c,
                Solicitation__r.GS_Status__c,
                Solicitation__r.SystemModstamp
        FROM GS_Project__c WHERE Id = :recordID LIMIT 1];
    }

    /**
    *  @description controller method to get all budget line, expenses, and files related to expenses for the specified projectRecordId
    *  @author      Aaron Hallink
    *  @date        2019-12-02
    *  @group       Methods
    *  @return      List<budgetLineWrapper>
    */

    @AuraEnabled
    public static List<budgetLineWrapper> getProjectBudgetLineItemsWithExpenses(String projectRecordId, String createdReportId){

        //return everything from the GS_Project_Budget_Line__c with the fed in project record ID, that have GS_Remaining_Expenses__c > 0
        List<GS_Project_Budget_Line__c> listRelatedBudgetlines = [SELECT Id, GS_Approved_Expenses__c,
                Budget_Category__c,
                GS_Concatenation_for_Search_Layouts__c,
                CreatedById,
                CreatedDate,
                IsDeleted,
                Deobligated_Amount__c,
                Description__c,
                GS_Disbursed_Amount__c,
                Funding_Source__c,
                Initial_Award__c,
                LastActivityDate,
                LastModifiedById,
                LastModifiedDate,
                LastReferencedDate,
                LastViewedDate,
                Match_Type__c,
                Project__c,
                Name,
                GS_Refunded_Amount__c,
                GS_Reimbursed_Expenses__c,
                GS_Remaining_Expenses__c,
                GS_Remaining_Payments__c,
                SystemModstamp,
                GS_Total_Paid_Amount__c
        FROM GS_Project_Budget_Line__c WHERE Project__c = :projectRecordId AND GS_Remaining_Expenses__c > 0 ];

        //one liner for getting the budget line ID's in a set by themselves, makes use of the map contsructor
        List<Id> budgetLineIds = new List<Id>(new Map<Id, GS_Project_Budget_Line__c>(listRelatedBudgetlines).keySet());

        //we will still need to lop through all the project report expenses to relate them to the correct budget line
        List<GS_Project_Report_Expense__c> listRelatedExpenses = [SELECT Id,
                Approved_Amount__c,
                Project_Budget_Line__c,
                CreatedById,
                CreatedDate,
                IsDeleted,
                Ineligible_Amount__c,
                LastActivityDate,
                LastModifiedById,
                LastModifiedDate,
                GS_Paid_Amount__c,
                Project_Report__c,
                Name,
                GS_Reimbursable_Amount__c,
                Requested_Amount__c,
                Short_Description__c,
                SystemModstamp,
                Transaction_Date__c
        FROM GS_Project_Report_Expense__c where Project_Budget_Line__c in :budgetLineIds AND Project_Report__c = :createdReportId];

        System.debug('Fed in parameter reportID: ' + createdReportId );
        System.debug('Fed in parameter ProjectRecordID: ' + projectRecordId );
        System.debug('listRlatedExpenses: ' + listRelatedExpenses);

        //key set of the expenses
        List<Id> expenseIds = new List<Id>(new Map<Id, GS_Project_Report_Expense__c>(listRelatedExpenses).keySet());

        //we want all the related files for the expenses
        //only query for files if we have some expenses, you can't query against contentDocumentlink with blank IDs
        List<ContentDocumentLink> relatedExpenseFiles = new List<ContentDocumentLink>();
        if(!expenseIds.isEmpty()){
            relatedExpenseFiles = [SELECT Id,LinkedEntityId,ContentDocumentId,ContentDocument.Title,ContentDocument.Description,ContentDocument.FileType FROM ContentDocumentLink where LinkedEntityId in :expenseIds ];
        }

        List<expenseWrapper> listofExpenseWrappers = new List<expenseWrapper>();
        //for each expense we need to have the related files for that expense
        for(GS_Project_Report_Expense__c expense : listRelatedExpenses){
            List<ContentDocumentLink> currentExpenseFiles = new List<ContentDocumentLink>();
            for(ContentDocumentLink file : relatedExpenseFiles){
                if(file.LinkedEntityId == expense.Id){
                    currentExpenseFiles.add(file);
                }
            }
            //instantiate the constructor for the expense wrapper
            listofExpenseWrappers.add(new expenseWrapper(expense,currentExpenseFiles));
        }




        List<budgetLineWrapper> listBudgetLineWrappers = new List<budgetLineWrapper>();

        //for each budget line we need to loop through each project report expense
        for(GS_Project_Budget_Line__c budgetLine : listRelatedBudgetlines){
            //new set of related expenses for each budget line, this may end up being empty for some budget lines
            Set<GS_Project_Report_Expense__c> setRelatedExpenses = new Set<GS_Project_Report_Expense__c>();
            //loop through each expense and compare it to the ID of the budget line budgetline.id =
            for(GS_Project_Report_Expense__c expense : listRelatedExpenses){
                if(budgetLine.Id == expense.Project_Budget_Line__c){
                    //add the expense to the set of related expenses
                    setRelatedExpenses.add(expense);
                }
            }
            //
            Set<expenseWrapper> setRelatedExpensesWrapper = new Set<expenseWrapper>();
            for(expenseWrapper expense :  listofExpenseWrappers){
                if(budgetLine.Id == expense.Project_Budget_Line){
                    //add the expense to the set of related expenses
                    setRelatedExpensesWrapper.add(expense);
                }
            }

            //once it has looped through all the expenses for one budget line we can create the wrapper and insert it to our list of wrappers
            listBudgetLineWrappers.add(new budgetLineWrapper(budgetLine,setRelatedExpensesWrapper));
        }



        //return the wrapper class with it's expense sums to the aura component
        return listBudgetLineWrappers;


    }
/**
    *  @description get values for the projectReport Status field
    *  @author      Aaron Hallink
    *  @date        2019-12-02
    *  @group       Methods
    *  @return      List<String>
    */
    @AuraEnabled//we want a string to return for the controller to process
    public static List<String> getProjectReportStatuses(){
        List<String> pickListValuesList= new List<String>();
        Schema.DescribeFieldResult fieldResult = GS_Project_Report__c.Status__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for( Schema.PicklistEntry pickListVal : ple){
            //add each value to the list
            pickListValuesList.add(pickListVal.getLabel());
        }
        //return the values
        return pickListValuesList;
    }

    /**
    *  @description controller to create a GS_Project_Report__c record to associate the expenses with
    *  @author      Aaron Hallink
    *  @date        2019-12-02
    *  @group       Methods
    *  @return      String recordId of the record created
    */

    @AuraEnabled //we want to be able to create the Project_Reporting_Record
    public static String createProjectReport(String projectId,Date startDate,Date endDate, String status){

        GS_Project_Report__c newProjectReport = new GS_Project_Report__c(Project__c = projectId,Reporting_Period_Start_Date__c = startDate,Reporting_Period_End_Date__c = endDate, Status__c = status);

        try {
            // Perform some database operations tha
            //  might cause an exception.
            insert newProjectReport;

        } catch(DmlException e) {
            System.debug('The following exception has occurred: ' + e.getMessage());

        }
        return newProjectReport.id;

    }

    /**
    *  @description deletes the expense record of the ID fed into the method
    *  @author      Aaron Hallink
    *  @date        2019-12-02
    *  @group       Methods
    *  @return      void
    */

    @AuraEnabled //we want to be able to delete expense line items
    public static void deleteExpense(String expenseId){

        GS_Project_Report_Expense__c expenseToDelete = [SELECT ID,Name FROM GS_Project_Report_Expense__c WHERE Id = :expenseId];

        try {
            // Perform some database operations tha
            //  might cause an exception.
            delete expenseToDelete;

        } catch(DmlException e) {
            System.debug('The following exception has occurred: ' + e.getMessage());

        }

    }

    /**
    *  @description update / insert expense records, return updated list of expenes for the selected BudgetLineId
    *  @author      Aaron Hallink
    *  @date        2019-12-02
    *  @group       Methods
    *  @return      List<expenseWrapper>
    */

    @AuraEnabled //we want to be able to create the Project_Reporting_Record
    //there will be records that are updates and records that are inserts
    public static List<expenseWrapper> saveExpenses(String createdReportId,String selectedBudgetLineId,List<expenseWrapper> listExpenses){

        //need to convert back from wrapper to SF custom expense object
        List<GS_Project_Report_Expense__c> listExpensestoParse = new List<GS_Project_Report_Expense__c>();
        for(expenseWrapper expense : listExpenses){

            GS_Project_Report_Expense__c expenseFromWrapper = new GS_Project_Report_Expense__c();
            expenseFromWrapper.Id = expense.Id;
            //cannot update masterdetail  field so we exclude it here
            expenseFromWrapper.Project_Budget_Line__c = selectedBudgetLineId;
            expenseFromWrapper.Short_Description__c = expense.Short_Description;
            expenseFromWrapper.Approved_Amount__c = expense.Approved_Amount;
            expenseFromWrapper.Ineligible_Amount__c = expense.Ineligible_Amount;
            expenseFromWrapper.GS_Paid_Amount__c = expense.GS_Paid_Amount;
            expenseFromWrapper.Requested_Amount__c = expense.Requested_Amount;
            expenseFromWrapper.Transaction_Date__c = expense.Transaction_Date;

            listExpensestoParse.add(expenseFromWrapper);

        }

        //one list for inserts one list for updates
        List<GS_Project_Report_Expense__c> tobeUpdated = new List<GS_Project_Report_Expense__c>();
        List<GS_Project_Report_Expense__c> tobeInserted = new List<GS_Project_Report_Expense__c>();

        //add the needed lookups if this is a new expense we are inserting
        for(GS_Project_Report_Expense__c expense : listExpensestoParse){
            if(expense.id == null){
                System.debug('Date format: ' + expense.Transaction_Date__c);
                expense.Project_Budget_Line__c = selectedBudgetLineId;
                expense.Project_Report__c = createdReportId;
                tobeInserted.add(expense);
            } else {
                tobeUpdated.add(expense);
            }
        }
        try {
            // Perform some database operations tha
            //  might cause an exception.
            //only do the data base action if the lists are not empty
            if(!tobeInserted.isEmpty()){
                insert tobeInserted;
                System.debug('did I get an id?: ' + tobeInserted.get(0));
            }

            if(!tobeUpdated.isEmpty()){
                update tobeUpdated;
            }

        } catch(DmlException e) {
            System.debug('The following exception has occurred: ' + e.getMessage());
        }


        //need te requery and recreate
        return customExpensetoExpenseWrappers(selectedBudgetLineId,createdReportId);


    }

    @AuraEnabled //we want to be able to create the Project_Reporting_Record
    //there will be records that are updates and records that are inserts
    public static List<fileWrapper> saveAttachments(String createdReportId,String selectedBudgetLineId,String selectedExpenseId,List<fileWrapper> listAttachments){

        System.debug('Attachment List: ' + listAttachments);

        //we want the set of content documentIDs from the file wrapper list
        Set<Id> contentDocumentIds = new Set<Id>();
        for(fileWrapper file: listAttachments){
            contentDocumentIds.add(file.ContentDocumentId);
        }


        //need to convert back from wrapper to SF custom expense object
        List<ContentVersion> listContentVersions = [SELECT Id,ContentDocumentId,Description,Title FROM ContentVersion WHERE ContentDocumentId IN :contentDocumentIds];

        //overwrite the titles and descriptions
        for(fileWrapper file: listAttachments){
            for(ContentVersion version : listContentVersions){
                if(file.ContentDocumentId == version.ContentDocumentId){
                    version.Title = file.Title;
                    version.Description = file.Description;
                }
            }

        }


        try {
            // Perform some database operations tha
            //  might cause an exception.
            //only do the data base action if the lists are not empty
            if(!listContentVersions.isEmpty()){
                update listContentVersions;
            }
        } catch(DmlException e) {
            System.debug('The following exception has occurred: ' + e.getMessage());
        }

        //recreate the data again
        List<expenseWrapper> allExpenses = customExpensetoExpenseWrappers(selectedBudgetLineId,createdReportId);

        //this is the file wrapper list we are going to return
        List<fileWrapper> filestoReturn = new List<fileWrapper>();

        for(expenseWrapper expense : allExpenses){
            if(expense.Id == selectedExpenseId){
                filestoReturn = expense.relatedFiles;
            }
        }

        return filestoReturn;


    }

    /**
    *  @description take the specified budgetline ID and return list of related expenses in the wrapper class
    *  @author      Aaron Hallink
    *  @date        2019-12-02
    *  @group       Methods
    *  @return      List<expenseWrapper>
    */
    @AuraEnabled
    public static List<expenseWrapper> customExpensetoExpenseWrappers(String selectedBudgetLineId, String createdReportId){
        //need to convert back to the wrapper class again for the table
        //requery what we just created
        List<GS_Project_Report_Expense__c> listRelatedExpenses = [SELECT Id,
                Approved_Amount__c,
                Project_Budget_Line__c,
                CreatedById,
                CreatedDate,
                IsDeleted,
                Ineligible_Amount__c,
                LastActivityDate,
                LastModifiedById,
                LastModifiedDate,
                GS_Paid_Amount__c,
                Project_Report__c,
                Name,
                GS_Reimbursable_Amount__c,
                Requested_Amount__c,
                Short_Description__c,
                SystemModstamp,
                Transaction_Date__c
        FROM GS_Project_Report_Expense__c where Project_Budget_Line__c = :selectedBudgetLineId AND Project_Report__c = :createdReportId];

        //we want all the related files for the expenses
        //only query for files if we have some expenses, you can't query against contentDocumentlink with blank IDs
        //now that we have inserted / updated the records we want the tabel to update with the new values
        //key set of the expenses
        List<Id> expenseIds = new List<Id>(new Map<Id, GS_Project_Report_Expense__c>(listRelatedExpenses).keySet());
        System.debug('Expense Ids: ' + expenseIds);
        List<ContentDocumentLink> relatedExpenseFiles = new List<ContentDocumentLink>();
        if(!expenseIds.isEmpty()){
            relatedExpenseFiles = [SELECT Id,LinkedEntityId,ContentDocumentId,ContentDocument.Title,ContentDocument.Description,ContentDocument.FileType FROM ContentDocumentLink where LinkedEntityId in :expenseIds ];
            System.debug('Related Expense Files: ' + relatedExpenseFiles);
        }

        List<expenseWrapper> listofExpenseWrappers = new List<expenseWrapper>();
        //for each expense we need to have the related files for that expense
        for(GS_Project_Report_Expense__c expense : listRelatedExpenses){
            List<ContentDocumentLink> currentExpenseFiles = new List<ContentDocumentLink>();
            for(ContentDocumentLink file : relatedExpenseFiles){
                if(file.LinkedEntityId == expense.Id){
                    currentExpenseFiles.add(file);
                    System.debug('File to add: ' + file);
                }
            }
            //instantiate the constructor for the expense wrapper
            listofExpenseWrappers.add(new expenseWrapper(expense,currentExpenseFiles));
        }

        return listofExpenseWrappers;
    }

    /**
    *  @description wrapper class for files, needed for showing data properly on the datatable components
    *  @author      Aaron Hallink
    *  @date        2019-12-02
    *  @group       class

    */

    //need this for displaying information on the data table correctly
    public class fileWrapper{

        //these are the expense fields
        //this ID at the top is the URL for the file
        @Auraenabled public String Id{get;set;}
        @Auraenabled public String Title{get;set;}
        @Auraenabled public String Description{get;set;}
        @Auraenabled public String FileType{get;set;}
        @AuraEnabled public String ContentDocumentId{get;set;}


        //no args constructor needed for instantiating a List<ExpenseWrapper>
        public fileWrapper(){
        }

        public fileWrapper(ContentDocumentLink File){

            //set the file page URL, community URL if it is in a community
            if(GS_Constants.COMMUNITY_ID == null){
                this.id = System.URL.getSalesforceBaseURL().toExternalForm() +'/' + File.ContentDocumentId;
            } else{
                String communityUrl = Network.getLoginUrl(GS_Constants.COMMUNITY_ID);
                //remove the word login at the end of the url
                communityUrl = communityUrl.substring(0,communityUrl.length()-5);
                this.Id = communityUrl+'detail/'+File.ContentDocumentId;
            }



            this.Title = File.ContentDocument.Title;
            this.Description = File.ContentDocument.Description;
            this.FileType = File.ContentDocument.FileType;
            this.ContentDocumentId = File.ContentDocumentId;

        }

    }


    /**
    *  @description wrapper class for expenses, needed for showing data properly on the datatable components
    *  @author      Aaron Hallink
    *  @date        2019-12-02
    *  @group       class

    */

    public class expenseWrapper{

        //these are the related file attributes
        @Auraenabled public List<fileWrapper> relatedFiles{get;set;}
        @Auraenabled public Integer relatedFilesCount{get;set;}

        //these are the expense fields
        @Auraenabled public String Id{get;set;}
        @Auraenabled public String Name{get;set;}
        @Auraenabled public String Project_Budget_Line{get;set;}
        @Auraenabled public String Project_Report{get;set;}
        @Auraenabled public String Short_Description{get;set;}
        @Auraenabled public Decimal Approved_Amount{get;set;}
        @Auraenabled public Decimal Ineligible_Amount{get;set;}
        @Auraenabled public Decimal GS_Paid_Amount{get;set;}
        @Auraenabled public Decimal GS_Reimbursable_Amount{get;set;}
        @Auraenabled public Decimal Requested_Amount{get;set;}
        @Auraenabled public Date Transaction_Date{get;set;}

        //no args constructor needed for instantiating a List<ExpenseWrapper>
        public expenseWrapper(){
        }


        public expenseWrapper(GS_Project_Report_Expense__c expense, List<ContentDocumentLink> relatedFiles){

            //set the values for the wrapper expense attributes
            this.Id = expense.Id;
            this.Name = expense.Name;
            this.Project_Budget_Line = expense.Project_Budget_Line__c;
            this.Project_Report = expense.Project_Report__c;

            this.Short_Description = expense.Short_Description__c;
            this.Approved_Amount = expense.Approved_Amount__c;
            this.Ineligible_Amount = expense.Ineligible_Amount__c;
            this.GS_Paid_Amount = expense.GS_Paid_Amount__c;
            this.GS_Reimbursable_Amount = expense.GS_Reimbursable_Amount__c;
            this.Requested_Amount = expense.Requested_Amount__c;
            this.Transaction_Date = expense.Transaction_Date__c;

            //set the related files attribute
            this.relatedFilesCount = relatedFiles.size();
            System.debug('Related File Count: ' + relatedFilesCount);
            //need to use a wrapper class for the related files
            list<fileWrapper> listfileWrappers = new List<fileWrapper>();
            for(ContentDocumentLink File : relatedFiles){
                listfileWrappers.add(new fileWrapper(File));
            }
            this.relatedFiles = listfileWrappers;

        }

    }

    /**
*  @description wrapper class for budgetLines, needed for showing data properly on the datatable components
*  @author      Aaron Hallink
*  @date        2019-12-02
*  @group       class

*/

    //Define budgetLine Wrapper Class
    public class budgetLineWrapper{

        //these are the budget line attributes
        @Auraenabled public GS_Project_Budget_Line__c budgetLine{get;set;}
        @Auraenabled public String budgetCategory{get;set;}
        @Auraenabled public String matchType{get;set;}
        @Auraenabled public String description{get;set;}
        @Auraenabled public Decimal initialAward{get;set;}
        @Auraenabled public Decimal remainingExpenses{get;set;}


        //these are the expense attributes
        @AuraEnabled public Set<expenseWrapper> setRelatedExpensesWrappers{get;set;}
        @AuraEnabled public Integer expenseCount{get;set;}
        @AuraEnabled public Decimal requestedAmountSum {get;set;}
        @AuraEnabled public Decimal ineligibleAmountSum {get;set;}
        @AuraEnabled public Decimal approvedAmountSum {get;set;}

        public budgetLineWrapper(GS_Project_Budget_Line__c budgetLine,Set<expenseWrapper> setRelatedExpensesWrappers){
            // create a wrapper class object and set the wrapper class @AuraEnabled properties and return it to the lightning component.
            this.budgetLine = budgetLine;
            this.budgetCategory = budgetLine.Budget_Category__c;
            this.matchType = budgetLine.Match_Type__c;
            this.description = budgetLine.Description__c;
            this.initialAward = budgetLine.Initial_Award__c;
            this.remainingExpenses = budgetLine.GS_Remaining_Expenses__c;
            //these are the expense attributes
            this.setRelatedExpensesWrappers = setRelatedExpensesWrappers;
            this.expenseCount = setRelatedExpensesWrappers.size();
            //rollup variables, they start at 0, expense sums
            Decimal requestedAmountSum = 0;
            Decimal ineligibleAmountSum = 0;
            Decimal approvedAmountSum = 0;
            //keep track of these sums
            for(expenseWrapper expense : setRelatedExpensesWrappers){
                requestedAmountSum = requestedAmountSum + expense.Requested_Amount;
                ineligibleAmountSum = ineligibleAmountSum + expense.Ineligible_Amount;
                approvedAmountSum = approvedAmountSum + expense.Approved_Amount;
            }
            //set the values in the wrapper class attributes for the expense summ set the values after we summ them
            this.requestedAmountSum = requestedAmountSum;
            this.ineligibleAmountSum = ineligibleAmountSum;
            this.approvedAmountSum = approvedAmountSum;

        }

    }


}