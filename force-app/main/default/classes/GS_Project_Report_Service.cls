/**
* @File Name:   GS_Project_Report_Service.cls
* @Description:  This class used as a service class for projet report custom object
* @Author:      Halima Dosso, hdosso@deloitte.ca
* @Group:       Apex
* @Last         Modified by:   Halima Dosso
* @Last         Modified time: 2018-12-07
* @Modification Log :
*-------------------------------------------------------------------------------------
* Ver           Date        Author             Modification
* 1.0       2018-12-07  Halima Dosso   Created the file/class and added  calculateApprovedAndReimbursedExpenses method*/ 

public with sharing class GS_Project_Report_Service {

   /**
   * @Name          calculateApprovedAndReimbursedExpenses
   * @Description   This method calls a managed Package (Declarative Lookup Rollup Summary Tool) method to 
   *				trigger the rollup calculations of the approved expenses and reimbursed expenses on a project
   *				budget line
   * @Author        Halima Dosso, hdosso@deloitte.ca
   * @CreatedDate   2018-12-07
   * @Parameter     Set<Id> projectReportIds
   * @Return        
   */
	public static void calculateApprovedAndReimbursedExpenses(Set<Id> projectReportIds){
    	Set<Id> budgetLineIds= New Set<Id>();
     	for(GS_Project_Report_Expense__c reportExpense :  GS_Project_Report_Expense_Selector.getProjectReportExpenseByProjectReportId(projectReportIds).values()){
        	 budgetLineIds.add(reportExpense.Project_Budget_Line__c);
     	}
    	List<dlrs.RollupActionCalculate.RollupToCalculate> listRollupsToCalculate = new List<dlrs.RollupActionCalculate.RollupToCalculate> ();
        for(Id budgetLineId : budgetLineIds){
            dlrs.RollupActionCalculate.RollupToCalculate approvedExpensesRollup = new dlrs.RollupActionCalculate.RollupToCalculate();
            dlrs.RollupActionCalculate.RollupToCalculate reimbursedExpensesRollup = new dlrs.RollupActionCalculate.RollupToCalculate();
            approvedExpensesRollup.ParentId=budgetLineId;
            reimbursedExpensesRollup.ParentId=budgetLineId;
            approvedExpensesRollup.RollupSummaryUniqueName=GS_Constants.APPROVED_EXPENSES_ROLLUP_CALCULATION;
            reimbursedExpensesRollup.RollupSummaryUniqueName=GS_Constants.REIMBURSED_EXPENSES_ROLLUP_CALCULATION;
            listRollupsToCalculate.add(approvedExpensesRollup);
            listRollupsToCalculate.add(reimbursedExpensesRollup);
        }
    	if(!listRollupsToCalculate.isEmpty()){
    		dlrs.RollupActionCalculate.calculate(listRollupsToCalculate);
    	}
	}
}