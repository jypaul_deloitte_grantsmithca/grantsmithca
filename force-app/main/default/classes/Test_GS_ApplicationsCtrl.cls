/**
* @File Name:  Test_GS_ApplicationsCtrl.cls
* @Description:  This is a trigger handler for the standard Attachment object
* @Author:      Aaron Hallink, Deloitte
* @Group:       Trigger Handler
* @Last         Modified by:   Aaron Hallink
* @Last         Modified time: 2019-12-04
* @Modification Log :
*-------------------------------------------------------------------------------------
* Ver           Date        Author             Modification
* 1.0      	 2019-12-04   Aaron Hallink      Created the file/class
*/

@IsTest
private class Test_GS_ApplicationsCtrl {
    static testMethod void testApplicationsCtrl() {

        //these will all return empty lists
        Map<String, List<GS_ApplicationsCtrl.SubmissionWrapper>> retrievedSubmission = GS_ApplicationsCtrl.getSubmissions();
        //List<GS_Submission__c> retrievedSubmissions = GS_ApplicationsService.getSubmission(null);
        List<GS_Application_Objective__c> retrievedApplicationObjectives = GS_ApplicationsService.getApplicationObjective(null);
        List<GS_Application_Budget_Line__c> retrievedApplicationBudgetLines = GS_ApplicationsService.getApplicationBudgetLine(null);

        System.assert(retrievedSubmission.isEmpty());
        //System.assert(retrievedSubmissions.isEmpty());
        System.assert(retrievedApplicationObjectives.isEmpty());
        System.assert(retrievedApplicationBudgetLines.isEmpty());

        //need to make sure the submission can insert correctly
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com',
                EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US',
                LocaleSidKey='en_US', ProfileId = p.Id,
                TimeZoneSidKey='America/Los_Angeles', UserName='standarduser@testorg3124l0982340983249u54rteuoahisntoeuhise.com');

        insert u;
        GS_Solicitation__c testSolicitation = new GS_Solicitation__c(Name='Test Solicitation');
        insert testSolicitation;

       

        GS_Program_Official__c testProgramOfficial =
                new GS_Program_Official__c(GS_Solicitation__c=testSolicitation.Id,
                        GS_Status__c = GS_Constants.ACTIVE_STATUS,
                        Internal_Approver_Role__c=GS_Constants.PROGRAM_OFFICIAL_SUBMIT_APPROVER,
                        GS_User__c=u.Id);
        insert testProgramOfficial;

        GS_Submission__c testSubmission = new GS_Submission__c(Name='Test Submission',GS_Solicitation__c = testSolicitation.Id);
        insert testSubmission;

        //now the wrapper will not be empty
        retrievedSubmission = GS_ApplicationsCtrl.getSubmissions();
        //retrievedSubmissions = GS_ApplicationsService.getSubmission(null);
        retrievedApplicationObjectives = GS_ApplicationsService.getApplicationObjective(null);
        retrievedApplicationBudgetLines = GS_ApplicationsService.getApplicationBudgetLine(null);

        System.assert(retrievedSubmission.isEmpty() == false);

        //add more gsUtility coverage
        User u2 = GS_Utility.getUserById(null);

    }
}