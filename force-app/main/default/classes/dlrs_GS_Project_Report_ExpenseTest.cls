/**
 * Auto Generated and Deployed by the Declarative Lookup Rollup Summaries Tool package (dlrs)
 **/
@IsTest
private class dlrs_GS_Project_Report_ExpenseTest
{
    @IsTest
    private static void testTrigger()
    {
        // Force the dlrs_GS_Project_Report_ExpenseTrigger to be invoked, fails the test if org config or other Apex code prevents this.
        dlrs.RollupService.testHandler(new GS_Project_Report_Expense__c());
    }
}