/**
* @File Name    :   GS_ListViewCtrl
* @Description  :   Controller class for the GS_ListView lightning component
* @Date Created :   2018-12-17
* @Author       :   aaron Hallink
* @group        :   Apex Class
* @Modification Log:
*******************************************************************************
* Ver          Date        Author                      Modification
* 1.0       2019-10-29     Aaron Hallink				     Created the file/class
* 1.1       2019-10-30     Aaron Hallink                    added Design Attribute for where clause on component
**/
@IsTest
private class Test_GS_ListViewCtrl {
    //here we have a test method
    static testMethod void testProgramList() {

        //insert a solicitation so we can query it
        GS_Solicitation__c objTestSolicitation = new GS_Solicitation__c();
        objTestSolicitation.name = 'Test Solicitation';
        insert objTestSolicitation;

        //we put in an empty string because we don't want to filter by anything.
        List<GS_Solicitation__c> lstReturnedSolicitations = GS_ListViewCtrl.getPrograms('');

        //check to see if the item we got back is the one we put in
        System.assert(lstReturnedSolicitations.size() == 1);
        System.assert(lstReturnedSolicitations.get(0).Name == objTestSolicitation.name);



    }
}