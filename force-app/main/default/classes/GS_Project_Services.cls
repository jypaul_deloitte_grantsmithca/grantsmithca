/**
* @File Name:   GS_Project_Services.cls
* @Description: This class used as a service class for GS_Project__c custom object
* @Author:      Jean Su, jesu@deloitte.ca
* @Group:       Apex
* @Last         Modified by:   Jean Su
* @Last         Modified time: 2019-01-01
* @Modification Log :
*-------------------------------------------------------------------------------------
* Ver           Date        Author             Modification
* 1.0       2019-01-01      Jean Su   Created the file/class
**/


global with sharing class GS_Project_Services {
    /**
    *  @description service method to get open project for current user
    *  @author      Jean Su, Deloitte
    *  @date        2019-01-01
    *  @group       Methods
    *  @return      List<GS_ProjectWrapper>
    */
    public static List<GS_ProjectWrapper> getOpenProjectByOwnerId(Id ownerId)
    {
        List<GS_Project__c> projects = [SELECT Id, GS_Total_Project_Budget__c, Start_Date__c, End_Date__c, Status__c, Solicitation__c, Solicitation__r.Name
                FROM GS_Project__c
                WHERE OwnerId = :ownerId AND Status__c = :GS_Constants.OPEN_STATUS limit 3];
        List<Id> solicitationIds = new List<Id>();
        List<GS_ProjectWrapper> projectWrappers = new List<GS_ProjectWrapper>();
        
        for (GS_Project__c project : projects)
        {
            solicitationIds.add(project.Solicitation__c);
        }
        
        //Map<Id, GS_Solicitation__c> solicitationsMap = new Map<Id, GS_Solicitation__c>([SELECT Id, 
        //                                                (SELECT Id FROM Attachments where Description = :GS_Constants.LOGO_TITLE)
        //                                                FROM GS_Solicitation__c
        //                                                WHERE Id in :solicitationIds]);
        Map<Id, GS_Solicitation__c> solicitationsMap = new Map<Id, GS_Solicitation__c>([SELECT Id, 
                                                        (SELECT Id, ContentDocument.LatestPublishedVersionId 
                                                        FROM ContentDocumentLinks where ContentDocument.Title = :GS_Constants.LOGO_TITLE)
                                                        FROM GS_Solicitation__c
                                                        WHERE Id in :solicitationIds]);
        for (GS_Project__c project : projects)
        {
            GS_Solicitation__c solicitation = solicitationsMap.get(project.Solicitation__c);
            GS_ProjectWrapper projectWrapper;
            
            //if (solicitation.Attachments.isEmpty())
            if (solicitation.ContentDocumentLinks.isEmpty())
            {
                //projectWrapper = new GS_ProjectWrapper(project, New Attachment());
                projectWrapper = new GS_ProjectWrapper(project, New ContentDocumentLink());
            }
            else
            {
                //projectWrapper = new GS_ProjectWrapper(project, solicitation.Attachments[0]);
                projectWrapper = new GS_ProjectWrapper(project, solicitation.ContentDocumentLinks[0]);
            }
            
            projectWrappers.add(projectWrapper);
        }
        return projectWrappers;
    }
    
    public class GS_ProjectWrapper {
        @AuraEnabled public GS_Project__c project;
        //@AuraEnabled public Attachment solicitationLogo;
        @AuraEnabled public ContentDocumentLink solicitationLogo;
        
        //public GS_ProjectWrapper(GS_Project__c project, Attachment solicitationLogo)
        public GS_ProjectWrapper(GS_Project__c project, ContentDocumentLink solicitationLogo)
        {
            this.project = project;
            this.solicitationLogo = solicitationLogo;            
        }
    }
}