/**
* @File Name:   GS_Project_Report_Service_Test.cls
* @Description: This is the TestClass for GS_Project_Report_Service
* @Author:      Diego Villarroel, dvillarroeld@deloitte.com
* @Group:       Test Class
* @Last         Modified by:   Diego Villarroel
* @Last         Modified time: 2019/03/12
* @Modification Log :
*-------------------------------------------------------------------------------------
* Ver           Date        Author              Modification
* 1.0           2019/03/12  Diego Villarroel    Created the file and testCalculateApprovedAndReimbursedExpensesBehavior method
**/

@IsTest
private class GS_Project_Report_Service_Test {
    @TestSetup
    static void setup(){
        Integer nReports = 2;

        GS_Project__c testProject = new GS_Project__c();
        List<GS_Project_Report__c> testListReports = new List<GS_Project_Report__c>();
        List<GS_Project_Report_Expense__c> testListReportsExpenses = new List<GS_Project_Report_Expense__c>();

        testProject.Name = 'testProject';
        insert testProject;

        GS_Project_Report__c testReport = new GS_Project_Report__c();
        for(Integer ix = 0; ix < nReports; ix++) {
            testReport = new GS_Project_Report__c();
            testReport.Due_Date__c = System.today() + 1;
            testReport.Reporting_Period_Start_Date__c = System.today();
            testReport.Reporting_Period_End_Date__c = System.today() + 1;
            testReport.Status__c = 'Draft'; /* PickList, Possible States: Draft, Submitted, Approved, Rejected */
            testReport.Project__c = testProject.Id;

            testListReports.add(testReport);
        } insert testListReports;

        GS_Project_Budget_Line__c budgetLine = new GS_Project_Budget_Line__c();
        budgetLine.Project__c = testProject.Id;
        insert budgetLine;

        GS_Project_Report_Expense__c testReportExpenses = new GS_Project_Report_Expense__c();
        for(GS_Project_Report__c currentReport : testListReports) {
            testReportExpenses = new GS_Project_Report_Expense__c();
            testReportExpenses.Project_Report__c = currentReport.Id;
            testReportExpenses.Approved_Amount__c = 0;
            testReportExpenses.Ineligible_Amount__c = 0;
            testReportExpenses.Requested_Amount__c = 0;
            testReportExpenses.Project_Budget_Line__c = budgetLine.Id;
            testListReportsExpenses.add(testReportExpenses);
        } insert testListReportsExpenses;
    }

    /**
    * @Name          testCalculateApprovedAndReimbursedExpensesBehavior
    * @Description   This method Tests the Behavior/Coverage of calculateApprovedAndReimbursedExpenses
    * @Author        Diego Villarroel
    * @CreatedDate   2019/03/12
    * @Parameter
    * @Return
    **/
    @IsTest//(SeeAllData=true)
    static void testCalculateApprovedAndReimbursedExpensesBehavior() {
        Set<Id> reportIds = new Set<Id>();
        List<GS_Project_Report__c> reports = [SELECT Id FROM GS_Project_Report__c];
        for (GS_Project_Report__c report : reports) {
            reportIds.add(report.Id);
        }

        GS_Project_Report_Service.calculateApprovedAndReimbursedExpenses(reportIds);
    }
}