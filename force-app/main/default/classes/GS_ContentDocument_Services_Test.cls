/**
* @File Name:   GS_ContentDocument_Services_Test.cls
* @Description: This is the TestClass for GS_ContentDocument_Services
* @Author:      Diego Villarroel, dvillarroeld@deloitte.com
* @Group:       Test Class
* @Last         Modified by:   Diego Villarroel
* @Last         Modified time: 2019/03/12
* @Modification Log :
*-------------------------------------------------------------------------------------
* Ver           Date        Author              Modification
* 1.0           2019/03/12  Diego Villarroel    Created the file and testGetPdfContentDistBehavior method
**/
@IsTest
private class GS_ContentDocument_Services_Test {
    @testSetup
    static void setup(){

        GS_Solicitation__c testSolicitation = createSolicitation();
        GS_Submission__c testSubmission = createSubmission(testSolicitation);
        createBudgetLine(testSubmission);
        createApplicationObjective(testSubmission);

        createOfficialProgram(testSolicitation);

        createNewAttachment('PDF');
    }

    static GS_Submission__c createSubmission(GS_Solicitation__c solicitation) {
        GS_Submission__c testSubmission = new GS_Submission__c(Name='Test Submission',GS_Solicitation__c=solicitation.Id);
        insert testSubmission;

        return testSubmission;
    }

    static GS_Solicitation__c createSolicitation() {


        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com',
                EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US',
                LocaleSidKey='en_US', ProfileId = p.Id,
                TimeZoneSidKey='America/Los_Angeles', UserName='standarduser@testorg3124l0982340983249u54rteuoahisntoeuhise.com');

        insert u;
        GS_Solicitation__c testSolicitation = new GS_Solicitation__c(Name='Test Solicitation');
        insert testSolicitation;



        GS_Program_Official__c testProgramOfficial =
                new GS_Program_Official__c(GS_Solicitation__c=testSolicitation.Id,
                        GS_Status__c = GS_Constants.ACTIVE_STATUS,
                        Internal_Approver_Role__c=GS_Constants.PROGRAM_OFFICIAL_SUBMIT_APPROVER,
                        GS_User__c=u.Id);
        insert testProgramOfficial;
        return testSolicitation;
    }

    static void createBudgetLine(GS_Submission__c submission) {
        GS_Application_Budget_Line__c budgetLine = new GS_Application_Budget_Line__c(
                GS_Submission__c = submission.Id
        ); insert budgetLine;
    }

    static void createApplicationObjective(GS_Submission__c submission) {
        GS_Application_Objective__c applicationObjective = new GS_Application_Objective__c(
                GS_Submission__c = submission.Id
        ); insert applicationObjective;
    }

    static void createOfficialProgram(GS_Solicitation__c testSolicitation) {
        GS_Program_Official__c officialProgram = new GS_Program_Official__c(
                GS_User__c = UserInfo.getUserId(),
                GS_Solicitation__c = testSolicitation.Id,
                GS_Status__c = 'Active',
                Internal_Approver_Role__c = 'Reviewer'
        ); insert officialProgram;
    }

    static void createNewAttachment(String fileExtension) {
        ContentVersion contentVersion = new ContentVersion(
                Title = 'Test Receipt',
                PathOnClient = 'testReceipt.' + fileExtension,
                VersionData = Blob.valueOf('Test Content'),
                IsMajorVersion = true
        );
        insert contentVersion;
    }


    /**
    * @Name          testGetPdfContentDistBehavior
    * @Description   This method Tests the Behavior/Coverage of getPdfContentDist
    * @Author        Diego Villarroel
    * @CreatedDate   2019-12-03
    * @Parameter
    * @Return
    **/
    @IsTest//(SeeAllData=true)
    static void testGetPdfContentDistBehavior() {
        List<GS_Submission__c> testSubmissions = [SELECT Id FROM GS_Submission__c LIMIT 1];
        List<GS_Solicitation__c> testSolicitations = [SELECT Id FROM GS_Solicitation__c LIMIT 1];

        Test.startTest();
        for(GS_Submission__c testSubmission : testSubmissions) {
            testSubmission.GS_Status__c = 'Ready for Review';
            testSubmission.GS_Solicitation__c = testSolicitations.get(0).Id;
            update  testSubmission;
        }
        Test.stopTest();

        List<GS_Scorecard__c> lstScorecards = [SELECT Id, Name FROM GS_Scorecard__c WHERE GS_Submission__c = :testSubmissions.get(0).Id];
        System.assert(lstScorecards.isEmpty() == false);

        //call method before attachment ***
        ContentDistribution retrievedContent = GS_ContentDocument_Services.getPdfContentDist(lstScorecards.get(0).Id);
        //this will be null since there is no attachment to retrieve yet
        System.assert(retrievedContent == null);

        List<ContentVersion> contentsVersion = [SELECT Id, Title FROM ContentVersion LIMIT 1];
        List<ContentDocument> documents = [SELECT Id, Title, LatestPublishedVersionId FROM ContentDocument LIMIT 1];

        //create ContentDocumentLink  record
        ContentDocumentLink cdl = new ContentDocumentLink();
        cdl.LinkedEntityId = lstScorecards.get(0).Id;
        cdl.ContentDocumentId = documents[0].Id;
        cdl.ShareType = 'V';
        insert cdl;

        //call method after attachment is associated ***
        retrievedContent = GS_ContentDocument_Services.getPdfContentDist(lstScorecards.get(0).Id);

        //the retrieved file should have the same name
        System.assert(retrievedContent.Name == contentsVersion.get(0).Title);
        System.debug('Retrieved Content(Name): ' + retrievedContent.Name + '/ Content Version(Title): ' + contentsVersion.get(0).Title);
    }
}