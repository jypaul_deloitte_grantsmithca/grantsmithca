/**
* @File Name:	GS_SubmissionBudgetWorksheetCtrl.cls
* @Description:  This class used as controller for GS_Application_Budget_Approval_Worksheet lightning component
* @Author:   	Yen Le
* @Group:   	Apex
* @Last 		Modified by:   Yen Le
* @Last 		Modified time: 2019-04-02
* @Modification Log	:
*-------------------------------------------------------------------------------------
* Ver       	Date        Author             Modification
* 1.0       2019-04-02  	Yen Le    Created the file/class
*/
public class GS_SubmissionBudgetWorksheetCtrl {
    @AuraEnabled
    public List<GS_Submission__c> submissionList {get;set;}
    @AuraEnabled
    public GS_Solicitation__c currentSolicitation {get;set;}
    @AuraEnabled
    public List<GS_Solicitation_Obligation__c> solicitationOblList {get;set;}
    
    @AuraEnabled
    public static GS_SubmissionBudgetWorksheetCtrl initClass(String solicitationId)
    {
        GS_SubmissionBudgetWorksheetCtrl ctrl = new GS_SubmissionBudgetWorksheetCtrl();
        ctrl.currentSolicitation = [select Id, Name, GS_Application_Due_Date_Time__c from GS_Solicitation__c where Id =: solicitationId];
        ctrl.submissionList = getSubmissionList(solicitationId);
        ctrl.solicitationOblList = getSolicitationObligationList(solicitationId);
        return ctrl;
    }
	
    private static List<GS_Submission__c > getSubmissionList(String solicitationId)
    {
        system.debug(solicitationId);
        return [SELECT Id, Name, GS_Applicant__c, GS_Applicant__r.Name, GS_Average_Score__c, GS_Status__c,
                	(SELECT Id, GS_Budget_Category__c, GS_Solicitation_Obligation__c, 
                     		GS_Requested_Amount__c, GS_Approved_Currency__c, GS_Submission__c, GS_Match_Type__c
                     FROM Application_Submission_Budget_Lines__r where GS_Budget_Category__c != null order by GS_Budget_Category__c)
                FROM GS_Submission__c  
                WHERE GS_Solicitation__c = :solicitationId 
                		and GS_Status__c =: GS_Constants.BUDGET_APPROVAL
               	order by GS_Average_Score__c desc];
    }
    
    private static List<GS_Solicitation_Obligation__c> getSolicitationObligationList(String solicitationId)
    {
        return [select Id, GS_Allocation__c, GS_Allocation__r.Name, GS_Available_Amount__c, GS_Reserved_Amount__c
               From GS_Solicitation_Obligation__c
               where GS_Solicitation__c = :solicitationId 
               order by GS_Allocation__r.GS_Start_Date__c ];
    }
    
    @AuraEnabled
    public static void saveBudgetWorksheet(List<GS_Application_Budget_Line__c> updateBudgetLines, List<String> updateSubmissionIds, String solicitationId)
    {
        try
        {
            update updateBudgetLines;
        }
        catch (Exception e)
        {
            throw new AuraHandledException(e.getMessage());
        }

        //Update submission ids
        //if(updateSubmissionIds.size() > 0){
        List<GS_Submission__c> submissions = new List<GS_Submission__c>();
        //**Bulkify
        //Loop through all submitted records that were approved
        for(Integer i = 0;i < updateSubmissionIds.size();i++){
            GS_Submission__c submissionToUpdate;
            try{
                submissionToUpdate = 
                    [SELECT GS_Status__c 
                     FROM GS_Submission__c 
                     WHERE Id =: updateSubmissionIds.get(i)];
                
                submissionToUpdate.GS_Status__c ='Approved';
                submissions.add(submissionToUpdate);
            }
            catch(Exception e){
                throw new AuraHandledException(e.getMessage());
            }
        }
        update submissions;
        //}
    }
    
}