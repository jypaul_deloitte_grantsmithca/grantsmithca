/**
* @File Name:   GS_Utility.cls
* @Description: Utility Class
* @Author:      Mohamed Zibouli, mzibouli@deloitte.ca
* @Group:       Apex
* @Last Modified by:   Mohamed Zibouli
* @Last Modified time: 2018-12-04 
* @Modification Log :
*-------------------------------------------------------------------------------------
* Ver       Date            Author                  	Modification
* 1.0       2018-12-04      Mohamed Zibouli          Created the file/class
* 1.0       2021-01-29      Ihssane Taziny           Added Method to query Custom Metadata: GS_Community_URL
*/
public with sharing class GS_Utility {

    private static Map<Id, User> cachedUsers;
    
    static{
        flush();
    }
    
    /**
    *  @description Flush all cached
    *  @author      Mohamed Zibouli, Deloitte
    *  @date        2018-12-04
    */
    public static void flush(){
        cachedUsers = new Map<Id, User>();
    }
    /**
    *  @description Returns the type of logged User 
    *  @author      Mohamed Zibouli, Deloitte
    *  @date        2018-12-04
    *  @group       Methods
    *  @return      String : Type Of User
    */
    public static String getUserType() {
        return UserInfo.getUserType();
    }
    
    /**
    *  @description Returns the User record of the current User.
    *  @author      Mohamed Zibouli, Deloitte
    *  @date        2018-12-04
    *  @group       Methods
    *  @return      Current User record.
    */
    public static User getCurrentUser() {
        Id userId = UserInfo.getUserId();
        if(cachedUsers.containsKey(userId)){
            return cachedUsers.get(userId);
        }
        return getUserById(userId);
    }
    
    /**
    *  @description Returns a User record by Id
    *  @author      Mohamed Zibouli, Deloitte
    *  @date        2018-12-04
    *  @group       Methods
    *  @return      User record.
    */
    public static User getUserById(Id userId) {
        User u;
        
        if (userId != null) {
            if (cachedUsers != null && cachedUsers.containsKey(userId)) {
                u = cachedUsers.get(userId);
            } else {
                List<User> users = [
                    SELECT Id, Name, Email, Contact.FirstName, Contact.LastName, ContactId, AccountId,FirstName,LastName,FederationIdentifier,
                    LanguageLocaleKey, ProfileId, Profile.Name
                    FROM User
                    WHERE Id = : userId
                ];
                
                if (users.size() == 1) {
                    u = users[0];
                    if (cachedUsers == null) {
                        cachedUsers = new Map<Id, User>();
                    }
                    cachedUsers.put(userId, u);
                }
            }
        }
        return u;
    }

    /**
     * @description Method that returns the record for Custom Metadata GS_Community_URL__mdt
     * @param ctmCommunityUrlName: Name of the custom Metadata record to query
     *  @author      Ihssane Taziny, Deloitte
     *  @date        2021-01-29
     *  @group       Methods
     *  @return      Custom Metadata GS_Community_URL__mdt record
     */
    public static GS_Community_URL__mdt getCommunityUrlByName(String ctmCommunityUrlName){
        List<GS_Community_URL__mdt> ctmComunityUrl = new List<GS_Community_URL__mdt>([select Id, GS_Grantee_Portal_Url__c, GS_Program_Directory_Url__c, GS_Reviewer_Portal_Url__c from GS_Community_URL__mdt where DeveloperName = :ctmCommunityUrlName]);

        if(!ctmComunityUrl.isEmpty()){
            return ctmComunityUrl.get(0);
        }

        return null;
    }
}