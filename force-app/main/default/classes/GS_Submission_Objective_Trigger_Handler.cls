/**
* @File Name:   GS_Submission_Objective_Trigger_Handler.cls
* @Description:  This is a trigger handler for  submission objective custom object
* @Author:      Jean Su, jesu@deloitte.ca
* @Group:       Trigger
* @Last         Modified by:   Jean Su
* @Last         Modified time: 2019-01-22
* @Modification Log :
*-------------------------------------------------------------------------------------
* Ver           Date        Author             Modification
* 1.0       2019-01-22 		Jean Su   			Created the file
*           2019-10-15      aaron Hallink       Submission Objective.GS_Application_Objective.GS_vlocity_Application remove references
*/ 

public with sharing class GS_Submission_Objective_Trigger_Handler {

    /**
    * @Name          linkToApplicationObjectives
    * @Description   This method is called after an insert on submission objective. It links the submission objectives with the submission
    * @Author        Jean Su, jesu@deloitte.ca
    * @CreatedDate   2019-01-22
    * @Parameter     List<GS_Application_Objective__c> objectiveList
    * @Return        void
    */
    public static void linkToSubmission(List<GS_Application_Objective__c> objectiveList){
//        Map<Id, Id> appIdObjIdMap = new Map<Id, Id>();
//        Map<Id, Id> appIdSubIdMap = new Map<Id, Id>();
//        for (GS_Application_Objective__c objective : objectiveList)
//        {
//            appIdObjIdMap.put(objective.GS_vlocity_Application__c, objective.Id);
//        }
//        List<GS_Submission__c> submissions = GS_ApplicationsService.getSubmission(appIdObjIdMap.keySet());
//
//        for (GS_Submission__c submission : submissions)
//        {
//            appIdSubIdMap.put(submission.GS_Application_JSON__c, submission.Id);
//        }
//        for(GS_Application_Objective__c objective : objectiveList)
//        {
//            objective.GS_Submission__c = appIdSubIdMap.get(objective.GS_vlocity_Application__c);
//        }
    }    
}