/**
* @File Name:   Test_GS_Attachment_Trigger_Handler.cls
* @Description:  This is a trigger handler for the standard Attachment object
* @Author:      Benjamin Hagel, Deloitte
* @Group:       Trigger Handler
* @Last         Modified by:   Benjamin Hagel
* @Last         Modified time: 2019-12-04
* @Modification Log :
*-------------------------------------------------------------------------------------
* Ver           Date        Author             Modification
* 1.0      	 2019-12-04   Benjamin Hagel      Created the file/class
*/

@IsTest
private class Test_GS_Attachment_Trigger_Handler {

    static testMethod void testAttachmentTrigger() {

        //need any test object to associate attachment with
        Case cs = new Case(Subject = 'Test', Origin = 'Phone', Status = 'New');
        insert cs;

        Test.startTest();
        //inserting attachment will setoff the tirgger
        Attachment attach=new Attachment();
        attach.Name='Unit Test Attachment';
        Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');
        attach.body=bodyBlob;
        attach.parentId=cs.id;
        insert attach;
        Test.stopTest();

        List<ContentVersion> lstContent = [SELECT ID,Title FROM ContentVersion];

        //The only file should have the same title
        System.assert(lstContent.get(0).Title == attach.Name);

    }
}