/**
* @File Name:   GS_Payment_Services.cls
* @Description:  This class used as a service class for payment custom object
* @Author:      Halima Dosso, hdosso@deloitte.ca
* @Group:       Apex
* @Last         Modified by:   Halima Dosso
* @Last         Modified time: 2018-12-07
* @Modification Log :
*-------------------------------------------------------------------------------------
* Ver           Date        Author             Modification
* 1.0       2018-12-07  Halima Dosso   Created the file/class and added  calculatePaidAndDisbursedAmount method*/ 

public with sharing class GS_Payment_Services {

   /**
   * @Name          calculatePaidAndDisbursedAmount
   * @Description   This method calls a managed Package (Declarative Lookup Rollup Summary Tool) method to 
   *				trigger the rollup calculations of the paid amount on a project report expense as well as the
   *				disbursed amount on a project budget line 
   *				budget line
   * @Author        Halima Dosso, hdosso@deloitte.ca
   * @CreatedDate   2018-12-07
   * @Parameter     Set<Id> projectReportIds
   * @Return        
   */
	public static void calculatePaidAndDisbursedAmount(Set<Id> paymentIds){
    	Set<Id>reportExpenseIds= New Set<Id>();
    	Set<Id>budgetLinesIds= New Set<Id>();
     	for(GS_Payment_Line_Item__c lineItem : GS_Payment_Line_Item_Selector.getPaymentLineItemByPaymentIds(paymentIds).values()){
        	reportExpenseIds.add(lineItem.GS_Project_Report_Expense__c);
        	budgetLinesIds.add(lineItem.GS_Project_Budget_Line__c);
     	}
     	//Calling a managed Package (Declarative Lookup Rollup Summary Tool) method to trigger the rollup calculations again
    	List<dlrs.RollupActionCalculate.RollupToCalculate> listRollupsToCalculate = new List<dlrs.RollupActionCalculate.RollupToCalculate> ();
        for(Id reportExpenseId : reportExpenseIds){
            dlrs.RollupActionCalculate.RollupToCalculate paidAmountRollup = new dlrs.RollupActionCalculate.RollupToCalculate();
            paidAmountRollup.ParentId=reportExpenseId;
            paidAmountRollup.RollupSummaryUniqueName=GS_Constants.PAID_AMOUNT_ROLLUP_CALCULATION;

            listRollupsToCalculate.add(paidAmountRollup);
        }
        for(Id budgetLineId : budgetLinesIds){
            dlrs.RollupActionCalculate.RollupToCalculate disbursedAmountRollup = new dlrs.RollupActionCalculate.RollupToCalculate();
            disbursedAmountRollup.ParentId=budgetLineId;
            disbursedAmountRollup.RollupSummaryUniqueName=GS_Constants.DISBURSED_AMOUNT_ROLLUP_CALCULATION;
            listRollupsToCalculate.add(disbursedAmountRollup);
        }
    	if(!listRollupsToCalculate.isEmpty()){
    		dlrs.RollupActionCalculate.calculate(listRollupsToCalculate);
    	}
	}
}