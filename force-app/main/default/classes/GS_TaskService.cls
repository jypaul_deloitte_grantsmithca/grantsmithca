/**
* @File Name:	GS_TaskService.cls
* @Description: This class used as a service class for Task object
* @Author:   	Jean Su, jesu@deloitte.ca
* @Group:   	Apex
* @Last 		Modified by:   Jean Su
* @Last 		Modified time: 2018-12-06
* @Modification Log	:
*-------------------------------------------------------------------------------------
* Ver       	Date        Author             Modification
* 1.0       2018-12-06  	Jean Su    		Created the file/class
* 1.1		2019-01-22		Jean Su			Add some fields to query
*/
public class GS_TaskService {
    
    /**
    *  @description service method to get incomplete tasks for current user
    *  @author      Jean Su, Deloitte
    *  @date        2018-12-06
    *  @group       Methods
    *  @return      List<Task>
    */
	public static List<Task> getIncompleteTasksByOwnerId(Id ownerId)
    {
        return [SELECT Id, Status, Subject, Description, WhatId, What.Name, ActivityDate FROM Task WHERE OwnerId = :ownerId AND Status != :GS_Constants.taskCompleted];
    }
    
    /**
    *  @description service method to get complete tasks for current user
    *  @author      Jean Su, Deloitte
    *  @date        2018-12-07
    *  @group       Methods
    *  @return      List<Task>
    */
    public static List<Task> getCompleteTasksByOwnerId(Id ownerId)
    {
        return [SELECT Id, Status, Subject, Description, WhatId, What.Name, ActivityDate FROM Task WHERE OwnerId = :ownerId AND Status = :GS_Constants.taskCompleted Order by ActivityDate DESC];
    }
}