/**
 * Auto Generated and Deployed by the Declarative Lookup Rollup Summaries Tool package (dlrs)
 **/
@IsTest
private class dlrs_GS_Project_Budget_LineTest
{
    @IsTest
    private static void testTrigger()
    {
        // Force the dlrs_GS_Project_Budget_LineTrigger to be invoked, fails the test if org config or other Apex code prevents this.
        dlrs.RollupService.testHandler(new GS_Project_Budget_Line__c());
    }
}