/**
* @File Name:   GS_Payment_Line_Item_Selector.cls
* @Description:  This is a selector for  project report expense custom object (all the queries on this object are stored here)
* @Author:      Halima Dosso, hdosso@deloitte.ca
* @Group:       Apex class
* @Last         Modified by:   Halima Dosso
* @Last         Modified time: 2018-12-18
* @Modification Log :
*-------------------------------------------------------------------------------------
* Ver           Date        Author             Modification
* 1.0       2018-12-18  Halima Dosso   Created the file and added  getPaymentLineItemById methods/*/ 

public with sharing class GS_Payment_Line_Item_Selector {
	public static Map<Id,GS_Payment_Line_Item__c> getPaymentLineItemByPaymentIds(Set<Id> paymentIds ) {
		return new Map<Id,GS_Payment_Line_Item__c>( [SELECT Id,Name,GS_Payment__c,GS_Amount__c,GS_Project_Report_Expense__c,GS_Project_Budget_Line__c
										             FROM  GS_Payment_Line_Item__c 
								                     WHERE GS_Payment__c In:paymentIds ]);
    }
}