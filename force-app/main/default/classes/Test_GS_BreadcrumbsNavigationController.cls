/**
 * Created by ahallink on 2019-12-17.
 */

@IsTest
private class Test_GS_BreadcrumbsNavigationController {
    static testMethod void testreturnBehavior() {

        GS_Project__c testProject = new GS_Project__c();
        insert testProject;

        GS_Project_Report__c testProjectReport = new GS_Project_Report__c(Project__c=testProject.id);
        insert testProjectReport;

        GS_Project_Objective__c testProjectObjective = new GS_Project_Objective__c(Project__c=testProject.Id);
        insert testProjectObjective;

        GS_Project_Budget_Line__c testProjectBudgetLine = new GS_Project_Budget_Line__c(Project__c = testProject.Id);
        insert testProjectBudgetLine;

        GS_Project_Report_Expense__c testProjectReportExpense = new GS_Project_Report_Expense__c(Project_Report__c=testProjectReport.Id);
        insert testProjectReportExpense;

        GS_Project_Report_Objective__c testProjectReportObjective = new GS_Project_Report_Objective__c(Project_Report__c=testProjectReport.Id);
        insert testProjectReportObjective;


        Account testAccount = new Account(Name='Test Account');
        insert testAccount;
        //project report test

        System.assert(testProject.Id == GS_BreadcrumbsNavigationController.getParentID(testProjectReport.id));


        //project objective test
        System.assert(testProject.Id == GS_BreadcrumbsNavigationController.getParentID(testProjectObjective.id));


        //project budgetLine test
        System.assert(testProject.Id ==  GS_BreadcrumbsNavigationController.getParentID(testProjectBudgetLine.id));


        //project Project Report expense
        System.assert(testProject.Id == GS_BreadcrumbsNavigationController.getParentID(testProjectReportExpense.id));


        //test the project report objective
        System.assert(testProject.Id ==  GS_BreadcrumbsNavigationController.getParentID(testProjectReportObjective.id));

        //using the method with account will return null
        System.assert(null ==  GS_BreadcrumbsNavigationController.getParentID(testAccount.Id));


    }
}