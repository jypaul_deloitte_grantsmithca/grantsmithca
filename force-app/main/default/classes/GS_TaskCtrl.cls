/**
* @File Name:	GS_TaskCtrl.cls
* @Description:  This class used as controller for Task lightning component
* @Author:   	Jean Su, jesu@deloitte.ca
* @Group:   	Apex
* @Last 		Modified by:   Jean Su
* @Last 		Modified time: 2018-12-06
* @Modification Log	:
*-------------------------------------------------------------------------------------
* Ver       	Date        Author             Modification
* 1.0       2018-12-06  	Jean Su    		Created the file/class
*/
public class GS_TaskCtrl {
	
    /**
    *  @description controller method to get incomplete tasks for current user
    *  @author      Jean Su, Deloitte
    *  @date        2018-12-06
    *  @group       Methods
    *  @return      List<Task>
    */
    @AuraEnabled
    public static List<Task> getIncompleteTask()
    {
        User currentUser = GS_Utility.getCurrentUser();
        return GS_TaskService.getIncompleteTasksByOwnerId(currentUser.Id);
    }
    
    /**
    *  @description controller method to get complete tasks for current user
    *  @author      Jean Su, Deloitte
    *  @date        2018-12-07
    *  @group       Methods
    *  @return      List<Task>
    */
    @AuraEnabled
    public static List<Task> getCompleteTask()
    {
        User currentUser = GS_Utility.getCurrentUser();
        return GS_TaskService.getCompleteTasksByOwnerId(currentUser.Id);
    }
}