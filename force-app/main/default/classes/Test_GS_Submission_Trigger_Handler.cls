/**
* @File Name:   GS_Submission_Trigger_Handler.cls
* @Description:  Test class for GS_Submission_Trigger_Handler
* @Author:     Aaron hallink ahallink@deloitte.ca
* @Group:       Trigger
* @Last         Modified by:   Aaron Hallink
* @Last         Modified time: 2019-11-19
* @Modification Log :
*-------------------------------------------------------------------------------------
* Ver           Date        Author             Modification
* 1.0       2019-11-19 	Aaron Hallink   	Created the file ,  GS_JSON_ScorecardWrapper class and afterUpdateFilters Method/
*
*/

@IsTest
private class Test_GS_Submission_Trigger_Handler {

/**
* @Name          testGS_Submission_Trigger_Handler
* @Description   Test method
* @Author        Aaron Hallink
* @CreatedDate   2019-11-19
*/
    static testMethod void testGS_Submission_Trigger_Handler() {


        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com',
                EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US',
                LocaleSidKey='en_US', ProfileId = p.Id,
                TimeZoneSidKey='America/Los_Angeles', UserName='standarduser@testorg3124l0982340983249u54rteuoahisntoeuhise.com');

        insert u;
        GS_Solicitation__c testSolicitation = new GS_Solicitation__c(Name='Test Solicitation');
        insert testSolicitation;



        GS_Program_Official__c testProgramOfficial =
                new GS_Program_Official__c(GS_Solicitation__c=testSolicitation.Id,
                        GS_Status__c = GS_Constants.ACTIVE_STATUS,
                        Internal_Approver_Role__c=GS_Constants.PROGRAM_OFFICIAL_SUBMIT_APPROVER,
                        GS_User__c=u.Id);
        insert testProgramOfficial;

        //lets create the submission record to trigger the trigger
        GS_Submission__c testSubmission = new GS_Submission__c(Name='Test Submission',GS_Solicitation__c = testSolicitation.Id);

        GS_Application_Budget_Line__c testBudget_line = new GS_Application_Budget_Line__c();
        GS_Application_Objective__c testApplicationObjective = new GS_Application_Objective__c();


        //a big chunk of the trigger used to incorporate Vlocity fields and Vlocity record queries.
        //i'm sure the trigger will be updated in the future. This class has been added for test class coverage of old code.
        insert testSubmission;
        //set the submission ID for the Budget_Line and the application objective
        testBudget_line.GS_Submission__c = testSubmission.Id;
        testApplicationObjective.GS_Submission__c = testSubmission.Id;
        insert testBudget_line;
        insert testApplicationObjective;

        User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
        //https://stackoverflow.com/questions/2387475/how-to-avoid-mixed-dml-operation-error-in-salesforce-tests-that-create-users
        System.runAs(thisUser){
            Test.startTest();
            //trigger update by changing status to approved
            testSubmission.GS_Status__c = GS_Constants.APPROVED_STATUS;
            update testSubmission;
            Test.stopTest();
        }




        //lets also insert a new submission with the status to touch the GS_Submission_Selector.getSubmissionAndrelatedObjectivesAndBudgetLinesById method
        //lets create the submission record to trigger the trigger
        GS_Submission__c testSubmission2 = new GS_Submission__c(Name='Test Submission',GS_Status__c = GS_Constants.APPROVED_STATUS);
        insert testSubmission2;



        GS_Submission__c RetrievedSubmission = [SELECT ID,Name FROM GS_Submission__c WHERE ID = :testSubmission.id];

        //very simple class at this point. lets make sure it got inserted with the same name.

        //Test another GS_Submission_Selector
        Set<Id> SubmissionIdSet = new Set<Id>();
        SubmissionIdSet.add(testSubmission.id);
        SubmissionIdSet.add(testSubmission2.id);
        Map<Id,GS_Submission__c> SubmissionMap = GS_Submission_Selector.getSubmissionAndRelatedScorecardsById(SubmissionIdSet);

        //this map should have 2 entries
        System.assert(SubmissionMap.size() == 2);
        System.assert(RetrievedSubmission.Name == testSubmission.Name);


    }
    /**
* @Name          testGS_SubmissionBudgetWorksheetCtrl
* @Description   Test method
* @Author        Aaron Hallink
* @CreatedDate   2019-11-19
* @ModifiedBy    Benjamin Hagel
* @Modified      2019-11-25
*/
    static testMethod void testGS_SubmissionBudgetWorksheetCtrl() {

        //1. Ceate program, budget line
        GS_Program__c testProgram = new GS_Program__c(Name='Test Program');
        insert testProgram;

        GS_Solicitation__c testSolicitation = new GS_Solicitation__c(Name='Test Solicitation');
        testSolicitation.GS_Program__c = testProgram.Id;
        insert testSolicitation;

        //Create allocation to appeease solicitation obligagion
        GS_Allocation__c testAllocation = new GS_Allocation__c(Name='Test Allocation');
        insert testAllocation;

        //Solicitation obligation
        GS_Solicitation_Obligation__c testSolicitationObligation = new GS_Solicitation_Obligation__c();
        testSolicitationObligation.GS_Solicitation__c = testSolicitation.Id;
        testSolicitationObligation.GS_Allocation__c = testAllocation.Id;
        insert testSolicitationObligation;

        GS_Submission__c testSubmission = new GS_Submission__c(Name='Test Submission', GS_Solicitation__c=testSolicitation.Id);
        testSubmission.GS_Status__c = GS_Constants.BUDGET_APPROVAL;//Needs to be BUDGET APPROVAL status to appear on worksheet
        insert testSubmission;

        GS_Application_Budget_Line__c testBudget_line = new GS_Application_Budget_Line__c();
        testBudget_line.GS_Submission__c = testSubmission.Id;
        testBudget_line.GS_Requested_Amount__c = 15000.00;//$
        insert testBudget_line;

//        String idOfSolicitationFromDatabase = '';
//        String idOfSolicitationFromWorksheetControl = '';

        Test.startTest();

            //Get solicitation record from db
            GS_Solicitation__c retrievedSolicitation = [SELECT Id, Name FROM GS_Solicitation__c WHERE Name='Test Solicitation'];
//            idOfSolicitationFromDatabase = ''+retrievedSolicitation.Id;

            GS_SubmissionBudgetWorksheetCtrl testController = GS_SubmissionBudgetWorksheetCtrl.initClass(retrievedSolicitation.Id);
            GS_Submission__c retrievedSubmission = [SELECT Id, Name FROM GS_Submission__c WHERE Name='Test Submission'];


            //Setup lists to save budge worksheet
            List<GS_Application_Budget_Line__c> budgetLinesToChange = new List<GS_Application_Budget_Line__c>();
            budgetLinesToChange.add(testBudget_line);
            List<String> submissionIdsToChange = new List<String>();
            submissionIdsToChange.add(retrievedSubmission.Id);
            //Save the updated values
            GS_SubmissionBudgetWorksheetCtrl.saveBudgetWorksheet(budgetLinesToChange,
                    submissionIdsToChange,
                    retrievedSolicitation.Id);

//        idOfSolicitationFromWorksheetControl = '' + testController.currentSolicitation.Id;
        Test.stopTest();

        //currentSolicitation
        //tryTothroweXception   how to assert:
        //

        //Grab Soliciation value see if it matches the "targetValue"
        //TODO test all  (4?) fields on the budget line
        System.assert(testSolicitation.Id == testController.currentSolicitation.Id);


    }

    /**
    * @Name          testGS_ScorecardCtrl
    * @Description   Test method
    * @Author        Benjamin Hagel
    * @CreatedDate   2019-11-28
    *
    * **/
    static testMethod void testGS_ScorecardCtrl() {

        //Create a solicitation
        GS_Solicitation__c testSolicitation = new GS_Solicitation__c(Name='Test Solicitation');
        insert testSolicitation;

        //Create some submissions
        List<GS_Submission__c> testSubmissions = new List<GS_Submission__c>();
        for(Integer i = 0;i < 10;i++){
            GS_Submission__c toAdd = new GS_Submission__c(Name='Test Submission' + i,
                    GS_Solicitation__c=testSolicitation.Id);
            toAdd.GS_Status__c = GS_Constants.BUDGET_APPROVAL;
            testSubmissions.add(toAdd);
        }
        insert testSubmissions;

        //Create score card objects
        Map<Id, GS_Scorecard__c> testScorecards = new Map<Id, GS_Scorecard__c>();
        for(Integer i = 0;i < 10;i++){
            GS_Scorecard__c newScorecard = new GS_Scorecard__c(GS_Status__c=GS_Constants.COMPLETED_STATUS);
            newScorecard.GS_Submission__c = testSubmissions.get(i).Id;
            //add to map
            testScorecards.put(newScorecard.Id, newScorecard);
            insert newScorecard;
        }

        //Create score card list w no map
        List<GS_Scorecard__c> testScorecardsNoMap = new List<GS_Scorecard__c>();
        for(Integer i = 0;i < 10;i++){
            GS_Scorecard__c newScorecard = new GS_Scorecard__c(GS_Status__c=GS_Constants.COMPLETED_STATUS);
            newScorecard.GS_Submission__c = testSubmissions.get(i).Id;
            testScorecardsNoMap.add(newScorecard);
            insert newScorecard;
        }

        //TODO: vlocity still needed?
        // dvillarroeld@deloitte.com : method invocation commented, code is unused
        //GS_ScoreCard_Trigger_Handler.linkApplicationPdf(testScorecardsNoMap);

        List<GS_Scorecard__c> tocheck_getIncompleteScorecard;
        GS_Scorecard__c tocheck_getScorecardById;


        Test.startTest();
            //Submit submissions
            Set<Id> ids = new Set<Id>();
            for(Integer i = 0;i < testSubmissions.size();i++){
                ids.add(testSubmissions.get(i).Id);
            }

            GS_ScoreCard_Services.setSubmissionToReadyForApproval(ids);

            //Cover all ScoreCardCtrl not handled by trigger
            tocheck_getIncompleteScorecard = GS_ScoreCardCtrl.getIncompleteScorecard();
            tocheck_getScorecardById = GS_ScoreCardCtrl.getScorecardById(testScorecardsNoMap[0].Id);

        Test.stopTest();

        //Re-grab submissions from DB
        List<GS_Submission__c> testSubmissions_fromDB = new List<GS_Submission__c>();
        for(Integer i = 0;i < 10; i++){
            String subName = 'Test Submission'+i;
            testSubmissions_fromDB.add(
                [SELECT Id, Name, GS_Status__c FROM GS_Submission__c WHERE Name=:subName]
            );
        }
        for(Integer i = 0;i < 10; i++){
            System.assert(testSubmissions.get(i).GS_Status__c == testSubmissions_fromDB.get(i).GS_Status__c);
        }

    }
}