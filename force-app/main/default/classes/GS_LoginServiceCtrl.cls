/**
* @File Name    :   GS_LoginServiceCtrl
* @Description  :   Controller class to check if user logged in 
* @Date Created :   2018-12-17
* @Author       :   jesu@deloitte.ca
* @group        :   Apex Class
* @Modification Log:
*******************************************************************************
* Ver          Date        Author                      Modification
* 1.0       2018-12-17     Jean Su				     Created the file/class
**/
public with sharing class GS_LoginServiceCtrl {

    /**
    *  @description check if user logged in and return user info
    *  @author      Jean Su, Deloitte
    *  @date        2018-12-17
    *  @group       Methods
    *  @return      UserWrapper
    */


    @AuraEnabled
    public  static UserWrapper isUserLoggedIn() {
        UserWrapper usrWrapper = null;
        String userType = GS_Utility.getUserType();
        if(!String.isEmpty(userType)){
            if (userType == 'Guest'){
                usrWrapper =  new UserWrapper();
                usrWrapper.isGuest = true;
            }else {

                User usr = GS_Utility.getCurrentUser();
                if(usr.ContactId != null) {
                    usrWrapper = new UserWrapper();
                    usrWrapper.userId = usr.Id;
                    usrWrapper.ContactId = usr.ContactId;
                    usrWrapper.firstname = usr.FirstName;
                    usrWrapper.lastname = usr.LastName;
                    usrWrapper.initial = String.valueOf(usr.FirstName.substring(0,1)) + String.valueOf(usr.LastName.substring(0,1));


                } else {
                    usrWrapper = new UserWrapper();
                    usrWrapper.userId = usr.Id;
                    usrWrapper.firstname = usr.FirstName;
                    usrWrapper.lastname = usr.LastName;
                    usrWrapper.initial = String.valueOf(usr.FirstName.substring(0,1)) + String.valueOf(usr.LastName.substring(0,1));
                }
            }
        }
        return usrWrapper;
    }

    public class UserWrapper {

        @AuraEnabled public Boolean isGuest { get; set; }
        @AuraEnabled public String userId { get; set; }
        @AuraEnabled public String ContactId { get; set; }
        @AuraEnabled public String firstname { get; set; }
        @AuraEnabled public String lastname { get; set; }
        @AuraEnabled public String initial { get; set; }
        @AuraEnabled public String urlPath { get; set; }
        @AuraEnabled public String siteURLPath { get; set; }
        public UserWrapper(){

            Boolean runningInASandbox = [SELECT IsSandbox FROM Organization LIMIT 1].IsSandbox;

            if(!Test.isRunningTest()){
                if(runningInASandbox){
                    this.urlPath = Site.getBaseSecureUrl() + Site.getPathPrefix();
                    //this.siteURLPath = Site.getBaseSecureUrl() + '/' + Site.getMasterLabel() + '/servlet/servlet.FileDownload?file=';
                    this.siteURLPath = Site.getBaseSecureUrl() + '/' + Site.getMasterLabel() + '/sfc/servlet.shepherd/version/renditionDownload?rendition=ORIGINAL_Jpg&versionId=';
                } else {
                    this.siteURLPath = Site.getBaseSecureUrl();

                    this.siteURLPath = this.siteURLPath.substring(0,this.siteURLPath.length()-2);
                    this.siteURLPath = this.siteURLPath + '/sfc/servlet.shepherd/version/renditionDownload?rendition=ORIGINAL_Jpg&versionId=';
                    this.urlPath = Site.getBaseSecureUrl();
                }

                System.debug('urlPath : ' + urlPath);
                System.debug('siteURLPath : ' + siteURLPath);
            }

        }
    }
}