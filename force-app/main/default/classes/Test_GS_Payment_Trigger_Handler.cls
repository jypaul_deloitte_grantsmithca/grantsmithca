/**
* @File Name:   Test_GS_Payment_Trigger_Handler.cls
* @Description:  This is a test class to cover the GS_Payment functionality
* @Author:      Benjamin Hagel, bhagel@deloitte.ca
* @Group:       Apex class
* @Last         Modified by:   Benjamin Hagel
* @Last         Modified time: 2019-12-04
* @Modification Log :
*-------------------------------------------------------------------------------------
* Ver           Date        Author             Modification
* 1.0       2019-12-04  Benjamin Hagel   Created the file and added test methods/*/
@IsTest
private class Test_GS_Payment_Trigger_Handler {
    @IsTest
    static void testPayment_Trigger_Handler() {

        GS_Project_Report__c testProjectReport = new GS_Project_Report__c();
        insert testProjectReport;

        GS_Payment__c testPayment = new GS_Payment__c();
        testPayment.GS_Project_Report__c = testProjectReport.Id;

        insert testPayment;

        //GS_Payment_Line_Item__c testLineItem = new GS_Payment_Line_Item__c();

        System.assert(true);
    }

    @IsTest
    static void testPayment_Services() {

        //Projects
        List<GS_Project__c> testProjects = new List<GS_Project__c>();
        for(Integer h = 0;h < 5;h++){
            GS_Project__c newProj = new GS_Project__c(Name='Test Project'+h);
            insert newProj;
            testProjects.add(newProj);
        }

        //Payments
        List<GS_Payment__c> testPayments = new List<GS_Payment__c>();
        for(Integer i = 0;i < 5;i++){
            GS_Payment__c newPayment = new GS_Payment__c();
            newPayment.GS_Project__c = testProjects.get(i).Id;
            GS_Project_Report__c testProjectReport = new GS_Project_Report__c();
            testProjectReport.Project__c = testProjects.get(i).Id;
            insert testProjectReport;
            newPayment.GS_Project__c = testProjects.get(i).Id;
            newPayment.GS_Project_Report__c = testProjectReport.Id;
            insert newPayment;

            testPayments.add(newPayment);
        }

        //get some trigger code coverage\
        //trigger onlt fires when status updated
        testPayments.get(0).GS_Status__c = 'Draft';
        update testPayments;
        testPayments.get(0).GS_Status__c = 'Pending';
        update testPayments;


        //Payment Line Items
        List<GS_Payment_Line_Item__c> paymentLineItems = new List<GS_Payment_Line_Item__c>();
        for(Integer i = 0;i < 5;i++){


            //Project bduget Line objcet
            GS_Project_Budget_Line__c newProjectBudgetLine = new GS_Project_Budget_Line__c();
            newProjectBudgetLine.Project__c = testProjects.get(i).Id;
            //Project Report expense
            GS_Project_Report_Expense__c newProjectReportExpense = new GS_Project_Report_Expense__c(Project_Report__c = testPayments.get(i).GS_Project_Report__c);
            insert newProjectReportExpense;
            insert newProjectBudgetLine;


            GS_Payment_Line_Item__c newPaymentLineItem = new GS_Payment_Line_Item__c();
            newPaymentLineItem.GS_Payment__c = testPayments.get(i).Id;
            newPaymentLineItem.GS_Project_Budget_Line__c = newProjectBudgetLine.Id;
            newPaymentLineItem.GS_Project_Report_Expense__c = newProjectReportExpense.id;
            insert newPaymentLineItem;



            paymentLineItems.add(newPaymentLineItem);
        }


        Set<Id> paymentIDS = new Set<Id>();
        for(Integer g = 0;g < testPayments.size();g++){paymentIDS.add(testPayments.get(g).Id);}
        //PAYMENT SERVICES
        //GS_Payment_Services testPaymentServices = new GS_Payment_Services();
        Test.startTest();
        GS_Payment_Services.calculatePaidAndDisbursedAmount(paymentIDS);
        Test.stopTest();
        //this doesnt return anything so lets not assert
    }

    @IsTest
    static void testPayment_Line_Item_Selector() {
        //Projects
        List<GS_Project__c> testProjects = new List<GS_Project__c>();
        for(Integer h = 0;h < 5;h++){
            GS_Project__c newProj = new GS_Project__c(Name='Test Project'+h);
            insert newProj;
            testProjects.add(newProj);
        }

        //Payments
        List<GS_Payment__c> testPayments = new List<GS_Payment__c>();
        for(Integer i = 0;i < 5;i++){
            GS_Payment__c newPayment = new GS_Payment__c();
            newPayment.GS_Project__c = testProjects.get(i).Id;
            GS_Project_Report__c testProjectReport = new GS_Project_Report__c();
            testProjectReport.Project__c = testProjects.get(i).Id;
            insert testProjectReport;
            newPayment.GS_Project__c = testProjects.get(i).Id;
            newPayment.GS_Project_Report__c = testProjectReport.Id;

            insert newPayment;
            testPayments.add(newPayment);
        }


        //Payment Line Items
        List<GS_Payment_Line_Item__c> paymentLineItems = new List<GS_Payment_Line_Item__c>();
        for(Integer i = 0;i < 5;i++){


            //Project bduget Line objcet
            GS_Project_Budget_Line__c newProjectBudgetLine = new GS_Project_Budget_Line__c();
            newProjectBudgetLine.Project__c = testProjects.get(i).Id;
            //Project Report expense
            GS_Project_Report_Expense__c newProjectReportExpense = new GS_Project_Report_Expense__c(Project_Report__c = testPayments.get(i).GS_Project_Report__c);
            insert newProjectReportExpense;
            insert newProjectBudgetLine;


            GS_Payment_Line_Item__c newPaymentLineItem = new GS_Payment_Line_Item__c();
            newPaymentLineItem.GS_Payment__c = testPayments.get(i).Id;
            newPaymentLineItem.GS_Project_Budget_Line__c = newProjectBudgetLine.Id;
            newPaymentLineItem.GS_Project_Report_Expense__c = newProjectReportExpense.id;
            insert newPaymentLineItem;



            paymentLineItems.add(newPaymentLineItem);
        }

        //take the list and make a list of just IDS
        Set<Id> testPaymentIds = new Set<Id>(new Map<Id, GS_Payment__c>(testPayments).keySet());

        Test.startTest();
        Map<Id,GS_Payment_Line_Item__c> testLineItems = GS_Payment_Line_Item_Selector.getPaymentLineItemByPaymentIds(testPaymentIds);
        Test.stopTest();


        //the keyset should contain these ids
        for(GS_Payment_Line_Item__c paymentLI : paymentLineItems){
            System.assert(testLineItems.containsKey(paymentLI.Id));
        }

    }
}