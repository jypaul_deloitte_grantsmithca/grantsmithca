public class GS_SolicitationWizardCTRL {
    
    public static final String ACTIVE = 'Active';
    //this method looks for all program's related program objectives
    //it is used as part of the solicitation wizard
    @AuraEnabled
    public static List<GS_Program_Objectives__c> getProgramObjectives (String programId) {
        List<GS_Program_Objectives__c> objectives = [ SELECT Id, Name, GS_Description__c,GS_Status__c
                                                      FROM GS_Program_Objectives__c 
                                                      WHERE  GS_Status__c=:ACTIVE AND GS_Program__c =:programId];
        return objectives; 
    }
    
    //this method looks for all program's related allocations
    //it is used as part of the solicitation wizard
    @AuraEnabled
    public static List<GS_Allocation__c> getAllocations (String programId) {
        List<GS_Allocation__c> allocations =  [ SELECT Id, Name, GS_Program__c
                                               FROM GS_Allocation__c 
                                               WHERE  GS_Program__c =:programId];
        return allocations;
    }
    
    
    //This method verify if a program was just created, if so, the user will need to create
    //// program related records ( allocation and program objectives) before creating a solicitation
    //on the solicitation wizard 
    //
    @AuraEnabled
    public static boolean isRecordJustCreated (String programId) {
        String programIdFinal = programId.substring(2,programId.length()-2);
        //For some reason the string sent to the server starts by "[ and ends by"].
        // We want to get rid of those extra characters so we have a proper ID format
        System.debug('programs'+programId);
        System.debug('program split'+programIdFinal );
        
        
        List<GS_Program__c> programs= [SELECT Id,CreatedDate  FROM  GS_Program__c WHERE Id =: programIdFinal ];
        System.debug('programs'+programs);
        if(!programs.isEmpty()){
            System.debug('program exists');
            Long differenceInTime = DateTime.now().getTime() - programs[0].CreatedDate.getTime();
            Long milliseconds = differenceInTime/1000;
            Long seconds = milliseconds/60;
            if(milliseconds<10){
                return true ;
            }else{
                
                
                return false ;
            }
            
        }
        System.debug('here in else' );
        return true;
        
    }
    
    
    
}