/**
* @File Name:   GS_Submission_Services.cls
* @Description:  This class used as a service class for Submission custom object
* @Author:      Halima Dosso, hdosso@deloitte.ca
* @Group:       Apex
* @Last         Modified by:   Halima Dosso
* @Last         Modified time: 2018-12-18
* @Last         Modified time: 2019-01-21
* @Modification Log :
*-------------------------------------------------------------------------------------
* Ver           Date        Author             Modification
* 1.0       2018-12-18  Halima Dosso   Created the file/class and added copyOverScoresToScorecards method
* 1.1       2019-01-21  Halima Dosso   Added createAssociatedProjectAndRelated method
*/ 



public with sharing class GS_Submission_Services {

   /**
   * @Name          copyOverScoresToScorecards
   * @Description   This methods copy over the score and max score from a submited submission of type scorecard
   *				to the related scorecard. It also sets the scorecard status to completed
   * @Author        Halima Dosso, hdosso@deloitte.ca
   * @CreatedDate   2018-12-
   * @Parameter     Map<String,Map<String,String>> scorecardsAndRelatedScores
   * @Return        
   */
    public static void  copyOverSubmissionToScorecards(Map<String,Map<String,String>> scorecardsAndRelatedScores ){
		Map<Id,GS_Scorecard__c> scorecards = GS_Scorecard_Selector.getScorecardsByIds(scorecardsAndRelatedScores.keySet());
    	for (Id scorecardId : scorecards.keySet()) {
    		String truncatedId = String.valueOf(scorecardId).substring(0, 15);
    		scorecards.get(scorecardId).GS_Max_Score__c = decimal.valueOf(scorecardsAndRelatedScores.get(truncatedId).get('MaxScore'));
    		scorecards.get(scorecardId).GS_Score__c =  decimal.valueOf(scorecardsAndRelatedScores.get(truncatedId).get('Score'));
    		scorecards.get(scorecardId).GS_Status__c = GS_Constants.COMPLETED_STATUS;
    		scorecards.get(scorecardId).GS_Completed_Time__c = Datetime.valueOf(scorecardsAndRelatedScores.get(truncatedId).get('completedDateTime')); 
    	}
    	update scorecards.values();
    }
    
    /**
   * @Name          createAssociatedProjectAndRelated
   * @Description   This methods copy over the submission fields to a project record field.
   *                It also copies over the submission budget lines to the project budget lines as well 
   *                as the submission objectives to the project objectives
   * @Author        Halima Dosso, hdosso@deloitte.ca
   * @CreatedDate   2019-01-21
   * @Parameter     Map<String,Map<String,String>> scorecardsAndRelatedScores
   * @Return        
   */
    public static void  createAssociatedProjectAndRelated(Map<Id,GS_Submission__c> submissionMap){
        List<GS_Project__c> projectsToAdd = new List<GS_Project__c>();
        List<GS_Project_Budget_Line__c> projectBudgetLinesToAdd = new List<GS_Project_Budget_Line__c>();
        List<GS_Project_Objective__c> projectObjectivesToAdd = new List<GS_Project_Objective__c>();
        for (GS_Submission__c submission : submissionMap.values() ){
            GS_Project__c projectToAdd = new GS_Project__c();
            projectToAdd.Account__c = submission.GS_Applicant__c;
            projectToAdd.Solicitation__c = submission.GS_Solicitation__c;
            projectToAdd.Name = submission.Name;
            projectToAdd.GS_Submission__c = submission.Id;
            projectsToAdd.add(projectToAdd);
        }
        if(!projectsToAdd.isEmpty()){
            insert projectsToAdd;
            for (GS_Project__c project : projectsToAdd){
               List<GS_Application_Budget_Line__c> submissionBudgetLines = submissionMap.get(project.GS_Submission__c).Application_Submission_Budget_Lines__r;
                System.debug('submissionBudgetLines: ' + submissionBudgetLines);
           	   List<GS_Application_Objective__c> submissionObjectives = submissionMap.get(project.GS_Submission__c).Application_Submission_Objectives__r;
               for (GS_Application_Budget_Line__c submissionBudgetLine:submissionBudgetLines){
                   GS_Project_Budget_Line__c projectBudgetLineToAdd = new GS_Project_Budget_Line__c();
                   projectBudgetLineToAdd.Budget_Category__c = submissionBudgetLine.GS_Budget_Category__c;
                   projectBudgetLineToAdd.Match_Type__c = submissionBudgetLine.GS_Match_Type__c;
                   projectBudgetLineToAdd.Funding_Source__c = submissionBudgetLine.GS_Solicitation_Obligation__c;
                   projectBudgetLineToAdd.Initial_Award__c = submissionBudgetLine.GS_Approved_Currency__c;
                   projectBudgetLineToAdd.Project__c = project.Id;
                   projectBudgetLinesToAdd.add(projectBudgetLineToAdd);
               }
               for (GS_Application_Objective__c submissionObjective:submissionObjectives){
                   GS_Project_Objective__c projectObjectiveToAdd = new GS_Project_Objective__c ();
                   projectObjectiveToAdd.City__c = submissionObjective.City__c;
                   projectObjectiveToAdd.Country__c = submissionObjective.Country__c;
                   projectObjectiveToAdd.State_Province__c = submissionObjective.State_Province__c;
                   projectObjectiveToAdd.Description__c = submissionObjective.GS_Description__c;
                   projectObjectiveToAdd.Objective__c = submissionObjective.GS_Solicitation_Objective__c;
                   projectObjectiveToAdd.Target__c = submissionObjective.Target__c;
                   projectObjectiveToAdd.Project__c = project.Id;
                   projectObjectivesToAdd.add(projectObjectiveToAdd);
               }
            }
            if(!projectBudgetLinesToAdd.isEmpty()){
                insert projectBudgetLinesToAdd;
            }
            if(!projectObjectivesToAdd.isEmpty()){
                insert projectObjectivesToAdd;
            }
        }
    }
    
}