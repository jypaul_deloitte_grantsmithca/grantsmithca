/**
 * @File Name:   GS_ScoreCard_Trigger_Handler.cls
 * @Description:  This class used as a trigger handler  class for Scorecard custom object
 * @Author:      Halima Dosso, hdosso@deloitte.ca
 * @Group:       Apex
 * @Last         Modified by:   Halima Dosso
 * @Last         Modified time: 2018-12-07
 * @Modification Log :
 *-------------------------------------------------------------------------------------
 * Ver           Date        Author             Modification
 * 1.0		2018-12-07	Halima Dosso		Created the file/class and added afterUpdateFilters method
 * 1.1		2019-01-17	Kevin Tsai			Added linkApplicationPdf()
 * 			2019-10-15	Aaron hallink		remove GS_Application_JSON__c references
 * 1.3		2019-12-16	Diego Villarroel	Commented unused code and created afterUpdateScorecard
**/

public with sharing class GS_ScoreCard_Trigger_Handler {
	/**
     * @Name			afterUpdateSubmissionScores
     * @Description		This method is called after update a Scorecard, and updates the average score and the total score of the related submission.
     * @Author			Diego Villarroel, dvillarroeld@deloitte.com
     * @CreatedDate		2019-12-16
     * @Parameter		Map<Id, GS_Scorecard__c> newMap, Map<Id, GS_Scorecard__c> oldMap
     * @Return			void
    **/
	public static void afterUpdateScorecard(Map<Id,GS_Scorecard__c> newMap) {
		/* DONE: Re-Calculate the Average and Total Score after a Scorecard is completed */
		List<GS_Scorecard__c> newScorecards = newMap.values();
		List<GS_Scorecard__c> relatedScorecards = [SELECT Id, Name, GS_Score__c FROM GS_Scorecard__c WHERE GS_Submission__c = :newScorecards.get(0).GS_Submission__c AND GS_Status__c = :GS_Constants.COMPLETED_STATUS AND GS_Score__c != NULL];

		if(!relatedScorecards.isEmpty()) {
			Decimal totalScore = 0.0;
			GS_Submission__c submission = [SELECT Id, Name, GS_Average_Score__c, GS_Total_Score__c FROM GS_Submission__c WHERE Id = :newScorecards.get(0).GS_Submission__c];
			//System.debug('- Submission: '+submission.Name);
			//System.debug('- Related Scorecards:');
			for (GS_Scorecard__c scorecard : relatedScorecards) {
				//System.debug('-- '+scorecard.name + ' (Score): '+scorecard.GS_Score__c);
				totalScore += scorecard.GS_Score__c;
			}

			submission.GS_Total_Score__c = totalScore;
			//System.debug('- Total Score: '+submission.GS_Total_Score__c);
			if(relatedScorecards.size() > 1) submission.GS_Average_Score__c = totalScore / relatedScorecards.size();
			//System.debug('- Average Score: '+submission.GS_Average_Score__c);

			update(submission);
		}
	}

	/**
	 * @Name          afterUpdateFilters
	 * @Description   This method is called after an update on scorecards. It filters and call the appropriate service methods.
	 * @Author        Halima Dosso, hdosso@deloitte.ca
	 * @CreatedDate   2018-12-07
	 * @Parameter     List<GS_Scorecard__c> scorecards, Map<Id, GS_Scorecard__c> oldMap
	 * @Return
	**/
	/*
	public static void afterUpdateFilters(Map<Id,GS_Scorecard__c>  scorecardsMap){
		// Set of Ids used to query the associated submission records that need to have to be set to ready for budget approval
		List<GS_Scorecard__c> scorecards = scorecardsMap.values();
		Set<Id> submissionIds = new Set<Id>(); 
		for(GS_Scorecard__c scorecard :scorecards ){
			if(scorecard.GS_Status__c == GS_Constants.COMPLETED_STATUS){
				submissionIds.add(scorecard.GS_Submission__c);
			}
		}
		if(!submissionIds.isEmpty()){
			GS_ScoreCard_Services.setSubmissionToReadyForApproval(submissionIds);
		}
	}
	*/

	/**
	 * @Name          linkApplicationPdf
	 * @Description   Links the PDF attached to the related vlocity Application of the scorecard being inserted.
	 * @Author        Kevin Tsai, Deloitte
	 * @CreatedDate   2019-01-17
	 * @Parameter     List<GS_Scorecard__c> scorecards
	 * @Return
    **/
	/*
	public static void linkApplicationPdf(List<GS_Scorecard__c> scorecards) {
		// Set of Ids used to query the associated Submission records to get the related vlocity Application Id
		Set<Id> parentSubmissionIds = new Set<Id>();
		for(GS_Scorecard__c sc : scorecards) {
			if(sc.GS_Submission__c != null){
				parentSubmissionIds.add(sc.GS_Submission__c);
			}
		}

		if(!parentSubmissionIds.isEmpty()) {
			// Submission records that contain the associated vlocity Application Ids (record that the PDF is attached to)
			Map<Id, GS_Submission__c> parentSubmissions = new Map<Id, GS_Submission__c>([SELECT Id
																			   	   FROM GS_Submission__c 
																			       WHERE Id IN : parentSubmissionIds]);
			// Maps Scorecard Ids to their "grandparent" vlocity Application Ids
			Map<Id,Id> scToAppMap = new Map<Id,Id>();
			for(GS_Scorecard__c sc : scorecards) {
				//scToAppMap.put(sc.Id, parentSubmissions.get(sc.GS_Submission__c).GS_Application_JSON__c);
			}

			if(!scToAppMap.isEmpty()) {
				// Maps vlocity Application Ids to the attached PDF Ids
				Map<Id,Id> appToDocMap = new Map<Id,Id>();
				//Stored vlocity Application Ids in a Set due to query implementation restriction on ContentDocumentLink
				Set<Id> vlocityAppIds = new Set<Id>(scToAppMap.values());
				for(ContentDocumentLink cdl : [SELECT ContentDocumentId,LinkedEntityId FROM ContentDocumentLink 
											   WHERE LinkedEntityId IN: vlocityAppIds]) {
					appToDocMap.put(cdl.LinkedEntityId, cdl.ContentDocumentId);
				}

				if(!appToDocMap.isEmpty()) {
					List<ContentDocumentLink> cdlsToInsert = new List<ContentDocumentLink>();
					// Link the PDF to the Scorecards
					for(GS_Scorecard__c sc : scorecards) {
						ContentDocumentLink cdl = new ContentDocumentLink();
						cdl.LinkedEntityId = sc.Id;
						cdl.ContentDocumentId = appToDocMap.get(scToAppMap.get(sc.Id));
						cdl.ShareType = 'V';
    					cdl.Visibility = 'AllUsers';
						cdlsToInsert.add(cdl);
					}
					insert cdlsToInsert;
				}
			}
		}
	}
	*/
}