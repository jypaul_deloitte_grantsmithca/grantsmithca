/**
* @File Name:   GS_ApplicationBudgetLineTriggerHelper
* @Description:   
* @Author:      Mohamed Zibouli, mzibouli@deloitte.ca
* @Group:       Class
* @Modification Log :
* 1.0       2018-12-18  Mohamed Zibouli
*           2019-10-15 aaron hallink remove GS_vlocity_Application__c  references
* -------------------------------------------------------------------------------------
**/

public with sharing class GS_ApplicationBudgetLineTriggerHelper {
    
    /**
    * @Name          LinkToRelatedSubmission
    * @Description   Method to link Related Submission to Application Budget Lines
    * @Author        mzibouli@deloitte.ca
    * @CreatedDate   2018-12-18
    * @Params		 List<GS_Application_Budget_Line__c> newTrg
    * @Return        void
    */
    public static void LinkToRelatedSubmission(List<GS_Application_Budget_Line__c> newTrg ){
//        Map<Id, List<GS_Application_Budget_Line__c>> appIdToABLinesMap = new Map<Id, List<GS_Application_Budget_Line__c>>();
//        for(GS_Application_Budget_Line__c abl : newTrg ){
//            if(abl.GS_vlocity_Application__c != null){
//                if(abl.GS_vlocity_Application__c != null){
//                List<GS_Application_Budget_Line__c> ablines= appIdToABLinesMap.get(abl.GS_vlocity_Application__c);
//                if(ablines == null ){
//                    ablines = new List<GS_Application_Budget_Line__c>();
//                    appIdToABLinesMap.put(abl.GS_vlocity_Application__c, ablines);
//                }
//                ablines.add(abl);
//            }
//        }
//        if(appIdToABLinesMap != null && !appIdToABLinesMap.isEmpty()){
//            Map<Id,GS_Submission__c> submissionsMap = GS_Submission_Selector.getSubmissionsByAppIds(appIdToABLinesMap.keySet());
//            for(GS_Submission__c submission : submissionsMap.values()){
//                for(GS_Application_Budget_Line__c abl :appIdToABLinesMap.get(submission.GS_Application_JSON__c) ){
//                    abl.GS_Submission__c = submission.Id;
//                }
//            }
//        }
    }
}