/**
* @File Name:   GS_Submission_Selector.cls
* @Description:  This is a selector for  Submission custom object (all the queries on this object are stored here)
* @Author:      Halima Dosso, hdosso@deloitte.ca
* @Group:       Apex class
* @Last         Modified by:   Halima Dosso
* @Last         Modified time: 2019-01-21
* @Modification Log :
*-------------------------------------------------------------------------------------
* Ver           Date        Author             	Modification
* 1.0       2018-12-07  Halima Dosso   		Created the file and added  getSubmissionAndRelatedScorecardsById methods
* 1.1       2018-12-18  Mohamed Zibouli   	add getSubmissionsByAppIds
* 1.2 		2019-01-21 Halima Dosso         added getSubmissionAndrelatedObjectivesAndBudgetLinesById
*           2019-10-15 aaron hallink        remove GS_Application_JSON__c references
*/ 


public with sharing class GS_Submission_Selector {

    /**
	* @Name          getSubmissionAndRelatedScorecardsById
	* @Description   It will return list of submissions for given application ids as well as the submission 
	*				 related application budget lines and application objectives
	* @Author        hdosso@deloitte.ca
	* @CreatedDate   2018-12-07
	* @Params		 Set<Id> submissionIds
	* @Return        Map<Id,GS_Submission__c>
	*/
	public static Map<Id,GS_Submission__c> getSubmissionAndRelatedScorecardsById(Set<Id> submissionIds ) {
        return new Map<Id,GS_Submission__c>([SELECT Id, Name,GS_Status__c,
                                             (SELECT Id,Name,GS_Status__c FROM Scorecards__r)
                							 FROM GS_Submission__c
                							 WHERE Id IN :submissionIds]);
    }

    /**
	* @Name          getSubmissionsByAppIds
	* @Description   It will return list of submission for given application ids
	* @Author        mzibouli@deloitte.ca
	* @CreatedDate   2018-12-18
	* @Params		 Set<Id> appIds
	* @Return        Map<Id,GS_Submission__c>
	*/
    
//	public static Map<Id,GS_Submission__c> getSubmissionsByAppIds(Set<Id> appIds ) {
////        return new Map<Id,GS_Submission__c>([SELECT Id, Name,GS_Status__c,GS_Application_JSON__c FROM GS_Submission__c WHERE GS_Application_JSON__c IN :appIds]);
//        return new Map<Id,GS_Submission__c>([SELECT Id, Name,GS_Status__c FROM GS_Submission__c WHERE Id IN :appIds]);
//    }
    
    /**
	* @Name          getSubmissionAndrelatedObjectivesAndBudgetLinesById
	* @Description   It will return list of submissions for given application ids as well as the submission 
	*				 related  budget lines and  objectives
	* @Author        hdosso@deloitte.ca
	* @CreatedDate   2019-01-21
	* @Params		 Set<Id> submissionIds
	* @Return        Map<Id,GS_Submission__c>
	*/
    
	public static Map<Id,GS_Submission__c> getSubmissionAndrelatedObjectivesAndBudgetLinesById(Set<Id> submissionIds ) {
        return new Map<Id,GS_Submission__c>([SELECT Id, Name,GS_Status__c,GS_Applicant__c,GS_Solicitation__c,
                                                 (SELECT Id,Name,GS_Budget_Category__c,GS_Submission__c,GS_Match_Type__c,GS_Solicitation_Obligation__c,GS_Approved_Currency__c
                                                  FROM Application_Submission_Budget_Lines__r),
                                                 (SELECT Id,Name,City__c,Country__c,State_Province__c,GS_Description__c,GS_Submission__c,GS_Solicitation_Objective__c,Target__c
                                                  FROM Application_Submission_Objectives__r)
                                             FROM GS_Submission__c 
                                             WHERE Id IN :submissionIds]);
    }  
    
    
    
    
}