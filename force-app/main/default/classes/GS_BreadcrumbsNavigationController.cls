/**
 * @File Name:   GS_BreadcrumbsNavigationController
 * @Description: controller used to display the breadcrumbs for the portals
 * @Author:     Aaron hallink
 * @Group:       Component
 * @Last Modified by:   Aaron hallink
 * @Last Modified time: 2019-12-18
 * @Modification Log :
 ______________________________________________________________________________________

 * Ver       Date            Author               Modification
 * 1.0    2019-12-18     Aaron Hallink          added controller, apex controller, dynamically build crumb based on page
 */

public with sharing class GS_BreadcrumbsNavigationController {


    //based on the object type of the ID we will return the 'parent' object
    @AuraEnabled
    public static String getParentID(String recordId) {

        //we want to use the standard ID method to get the Sobject name
        Id childId = Id.valueOf(recordId);
        String objectAPIName = childId.getSobjectType().getDescribe().getName();

        //we don't know what object to search until we get the ID

        switch on objectAPIName {
            when 'GS_Project_Budget_Line__c' {
                //return the Parent ID in this case it is the project ID
                return [SELECT Id,Project__c FROM GS_Project_Budget_Line__c WHERE Id = :recordId].Project__c;
            }
            when 'GS_Project_Report__c' {
                //return the Parent ID in this case it is the project ID
                return [SELECT Id,Project__c FROM GS_Project_Report__c WHERE Id = :recordId].Project__c;
            }
            when 'GS_Project_Report_Expense__c' {
                //return the Parent ID in this case it is the project ID
                return [SELECT Id,Project_Report__r.Project__c FROM GS_Project_Report_Expense__c WHERE Id = :recordId].Project_Report__r.Project__c;
            }
            when 'GS_Project_Objective__c' {
                //return the Parent ID in this case it is the project ID
                return [SELECT Id, Project__c FROM GS_Project_Objective__c WHERE Id = :recordId].Project__c;
            }
            when 'GS_Project_Report_Objective__c' {
                //return the Parent ID in this case it is the project ID
                return [SELECT Id, Project_Report__r.Project__c FROM GS_Project_Report_Objective__c WHERE Id = :recordId].Project_Report__r.Project__c;
            }
            when 'GS_Scorecard__c' {
                //return the Parent ID in this case it is the project ID
                return [SELECT Id, GS_Submission__r.Id FROM GS_Scorecard__c WHERE Id = :recordId].GS_Submission__r.Id;
            }
            when 'GS_Solicitation__c' {
                //return the Parent ID in this case it is the project ID
                return [SELECT Id, GS_Program__r.Id FROM GS_Solicitation__c WHERE Id = :recordId].GS_Program__r.Id;
            }

        }

        //if we hit none of the cases return null
        return null;

    }


}