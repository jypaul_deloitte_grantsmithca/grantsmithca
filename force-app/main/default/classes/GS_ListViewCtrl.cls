/**
* @File Name    :   GS_ListViewCtrl
* @Description  :   Controller class for the GS_ListView lightning component
* @Date Created :   2018-12-17
* @Author       :   aaron Hallink
* @group        :   Apex Class
* @Modification Log:
*******************************************************************************
* Ver          Date        Author                      Modification
* 1.0       2019-10-29     Aaron Hallink				     Created the file/class
* 1.1       2019-10-30     Aaron Hallink                    added Design Attribute for where clause on component
**/
public without sharing class GS_ListViewCtrl {

    @AuraEnabled
    public static List<GS_Solicitation__c> getPrograms(String strWhereClause){
        //just return the records we want

        String strProgramQuery = 'SELECT Id,GS_Application_Due_Date_Time__c,GS_Application_Form_Template_URL__c,GS_Available_Amount__c,GS_Awarded_Date__c,GS_Close_Date__c,CreatedById,CreatedDate,IsDeleted,GS_Description__c,LastActivityDate,LastModifiedById,LastModifiedDate,LastReferencedDate,LastViewedDate,Minimum_Reviews_Required__c,OwnerId,GS_Program__c,GS_Program__r.Name,GS_Program__r.GS_Description_of_the_program__c,GS_Score_Card_Template_URL__c,Name,GS_Solicitation_Type__c,GS_Start_Date__c,GS_Status__c,SystemModstamp FROM GS_Solicitation__c ';
        //add the where clause from the builder
        return Database.Query(strProgramQuery + strWhereClause);

    }
	
    @AuraEnabled
    public static GS_Community_URL__mdt getCommunityUrl(){
        try {
            
            GS_Community_URL__mdt cmtCommunityUrl = GS_Utility.getCommunityUrlByName(GS_Constants.COMMUNITY_URL);
            return cmtCommunityUrl;

        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }
}