/**
* @File Name:   GS_Project_Report_Trigger_Handler_Test.cls
* @Description: This is the TestClass for GS_Project_Report_Trigger_Handler
* @Author:      Diego Villarroel, dvillarroeld@deloitte.com
* @Group:       Test Class
* @Last         Modified by:   Diego Villarroel
* @Last         Modified time: 2019/02/12
* @Modification Log :
*-------------------------------------------------------------------------------------
* Ver           Date        Author              Modification
* 1.0           2019/02/12  Diego Villarroel    Created the file and afterUpdateFiltersBehavior method
**/
@IsTest
private class GS_Project_Report_Trigger_Handler_Test {
    @testSetup
    static void setup(){
        GS_Project_Report__c testReport = new GS_Project_Report__c();
        testReport.Due_Date__c = System.today()+1;
        testReport.Reporting_Period_Start_Date__c = System.today();
        testReport.Reporting_Period_End_Date__c = System.today()+1;
        testReport.Status__c = 'Draft'; /* PickList, Possible States: Draft, Submitted, Approved, Rejected */

        insert testReport;
    }

    /**
   * @Name          afterUpdateFiltersBehavior
   * @Description   This method Tests the Behavior/Coverage of afterUpdateFilters
   * @Author        Diego Villarroel
   * @CreatedDate   2019-12-02
   * @Parameter
   * @Return
   **/
    @IsTest/*(SeeAllData=true)*/
    static void testAfterUpdateReport() {
        List<GS_Project_Report__c> report = [SELECT id, Status__c FROM GS_Project_Report__c];

        Test.startTest();
        report[0].Status__c = 'Approved';
        update report[0];
        Test.stopTest();

        System.assertEquals(report[0].Status__c, 'Approved');
    }
}