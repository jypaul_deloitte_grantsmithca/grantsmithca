/**
* @File Name:   GS_Attachment_Trigger_Handler.cls
* @Description:  This is a trigger handler for the standard Attachment object
* @Author:      Kevin Tsai, Deloitte
* @Group:       Trigger Handler
* @Last         Modified by:   Kevin Tsai
* @Last         Modified time: 2019-01-19
* @Modification Log :
*-------------------------------------------------------------------------------------
* Ver           Date        Author             Modification
* 1.0      	 2019-01-19   Kevin Tsai      Created the file/class
*/ 
public with sharing class GS_Attachment_Trigger_Handler {

	/**
   * @Name          copyAttachmentToFiles
   * @Description   This method is called after an insert on Attachment
   *				It creates a copy of the attachment in Files (ContentVersion)
   * @Author        Kevin Tsai, Deloitte
   * @CreatedDate   2019-01-16
   * @Parameter     List<Attachment> attachmentList
   * @Return        void
   */
	public static void copyAttachmentToFiles(List<Attachment> attachmentList){
		List<ContentVersion> copiesToInsert = new List<ContentVersion>();
		for(Attachment attachment : attachmentList){
			ContentVersion attmCopy = new ContentVersion();
			attmCopy.Title = attachment.Name;
			attmCopy.ContentLocation = 'S';
			attmCopy.FirstPublishLocationId = attachment.ParentId;
			attmCopy.VersionData = attachment.Body;
			attmCopy.PathOnClient = attachment.Name;
			copiesToInsert.add(attmCopy);
		}
		insert copiesToInsert;
	}
}